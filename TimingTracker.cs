//#define USING_TIMING_TRACKER

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Nancy;

namespace FIORest
{
    public class RequestTimingData
    {
        public string Endpoint { get; set; }
        public long MinMS { get; set; } = long.MaxValue;
        public long MaxMS { get; set; } = long.MinValue;
        public long TotalMS { get; set; } = 0;
        public double AvgMS { get; set; } = 0.0;
        public long NumReqs { get; set; } = 0;
    }

    public static class TimingTracker
    {
        public static bool IsEnabled =
#if USING_TIMING_TRACKER
            true;
#else
            false;
#endif // USING_TIMING_TRACKER

        public static Dictionary<string, RequestTimingData> TimingData = new Dictionary<string, RequestTimingData>();
        public static Dictionary<Request, Stopwatch> PendingTimings = new Dictionary<Request, Stopwatch>();
        public static int PendingTimingIndex = 0;

        public static Func<NancyContext, Response> BeforeRequest()
        {
            return context =>
            {
                var sw = new Stopwatch();
                var PendingTimingIdx = PendingTimingIndex++;
                PendingTimings.Add(context.Request, sw);
                sw.Start();
                return null;
            };
        }

        public static Action<NancyContext> AfterRequest()
        {
            return context =>
            {
                if (PendingTimings.ContainsKey(context.Request))
                {
                    var sw = PendingTimings[context.Request];
                    PendingTimings.Remove(context.Request);
                    sw.Stop();

                    var endpoint = $"{context.Request.Method}-{context.Request.Path}";
                    if (!TimingData.ContainsKey(endpoint))
                    {
                        var rtd = new RequestTimingData();
                        rtd.Endpoint = endpoint;
                        rtd.MinMS = sw.ElapsedMilliseconds;
                        rtd.MaxMS = sw.ElapsedMilliseconds;
                        rtd.TotalMS = sw.ElapsedMilliseconds;
                        rtd.AvgMS = sw.ElapsedMilliseconds;
                        rtd.NumReqs = 1;

                        TimingData.TryAdd(endpoint, rtd);
                    }
                    else
                    {
                        TimingData[endpoint].MinMS = Math.Min(TimingData[endpoint].MinMS, sw.ElapsedMilliseconds);
                        TimingData[endpoint].MaxMS = Math.Max(TimingData[endpoint].MaxMS, sw.ElapsedMilliseconds);
                        TimingData[endpoint].TotalMS += sw.ElapsedMilliseconds;
                        TimingData[endpoint].NumReqs++;
                        TimingData[endpoint].AvgMS = TimingData[endpoint].TotalMS / TimingData[endpoint].NumReqs;
                    }
                }
            };
        }

        public static Func<NancyContext, Exception, dynamic> OnError()
        {
            return (ctx, ex) =>
            {
                PendingTimings.Remove(ctx.Request);
                return null;
            };
        }

        public static List<RequestTimingData> GetTimingData()
        {
            return TimingData.Values.ToList();
        }
    }
}