﻿using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class ShipModule : NancyModule
    {
        public ShipModule() : base("/ship")
        {

            Post("/ships", _ =>
            {
                this.EnforceWriteAuth();
                return PostShips();
            });

            Get("/ships/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetShips(parameters.user_name);
            });

            Get("/ships/fuel/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetShipsFuel(parameters.user_name);
            });

            Post("/flights", _ =>
            {
                this.EnforceWriteAuth();
                return PostFlights();
            });

            Get("/flights/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetFlights(parameters.user_name);
            });
        }

        private Response PostShips()
        {
            using (var req = new FIORequest<JSONRepresentations.ShipShips.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                SHIPSModel model = req.DB.SHIPSModels.Where(m => m.UserNameSubmitted.ToUpper() == req.UserName).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new SHIPSModel();
                }
                else
                {
                    model.Ships.Clear();
                }
                
                foreach (var shipObj in rootObj.payload.message.payload.ships)
                {
                    SHIPSShip ship = new SHIPSShip();
                    ship.ShipId = shipObj.id;
                    ship.StoreId = shipObj.idShipStore;
                    ship.StlFuelStoreId = shipObj.idStlFuelStore;
                    ship.FtlFuelStoreId = shipObj.idFtlFuelStore;
                    ship.Registration = shipObj.registration;
                    ship.Name = shipObj.name;
                    ship.CommissioningTimeEpochMs = shipObj.commissioningTime.timestamp;
                    ship.BlueprintNaturalId = shipObj.blueprintNaturalId;
                    ship.FlightId = shipObj.flightId;
                    ship.Acceleration = shipObj.acceleration;
                    ship.Thrust = shipObj.thrust;
                    ship.Mass = shipObj.mass;
                    ship.OperatingEmptyMass = shipObj.operatingEmptyMass;
                    ship.ReactorPower = shipObj.reactorPower;
                    ship.EmitterPower = shipObj.emitterPower;
                    ship.Volume = shipObj.volume;

                    ship.Condition = shipObj.condition;
                    ship.LastRepairEpochMs = shipObj.lastRepair?.millis;

                    foreach(var repairMat in shipObj.repairMaterials)
                    {
                        SHIPSRepairMaterial rm = new SHIPSRepairMaterial();

                        rm.MaterialName = repairMat.material.name;
                        rm.MaterialId = repairMat.material.id;
                        rm.MaterialTicker = repairMat.material.ticker;
                        rm.Amount = repairMat.amount;

                        ship.RepairMaterials.Add(rm);
                    }

                    List<string> addressLines = new List<string>();
                    if (shipObj.address != null)
                    {
                        foreach (var lines in shipObj.address.lines)
                        {
                            if (lines.type == "SYSTEM" || lines.type == "PLANET")
                            {
                                // Look up planet name
                                if ( lines.entity.naturalId != lines.entity.name)
                                {
                                    addressLines.Add($"{lines.entity.name} ({lines.entity.naturalId})");
                                }
                                else
                                {
                                    addressLines.Add(lines.entity.naturalId);
                                }
                            }
                            else
                            {
                                addressLines.Add(lines.type);
                            }
                        }
                    }
                    ship.Location = string.Join(" - ", addressLines);

                    ship.StlFuelFlowRate = shipObj.stlFuelFlowRate;

                    model.Ships.Add(ship);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetShips(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if ( Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Flight))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.SHIPSModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetShipsFuel(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequestUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequestUserName, UserName, Auth.PrivacyType.Flight))
            {
                using(var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.Type == "STL_FUEL_STORE" || s.Type == "FTL_FUEL_STORE")).ToList();
                    return JsonConvert.SerializeObject(res);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostFlights()
        {
            using (var req = new FIORequest<JSONRepresentations.ShipFlightFlights.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                FLIGHTSModel model = req.DB.FLIGHTSModels.Where(m => m.UserNameSubmitted.ToUpper() == req.UserName).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new FLIGHTSModel();
                }
                else
                {
                    model.Flights.Clear();
                }

                var data = rootObj.payload.message.payload;

                foreach (var flightObj in data.flights)
                {
                    FLIGHTSFlight flight = new FLIGHTSFlight();
                    flight.FlightId = flightObj.id;
                    flight.ShipId = flightObj.shipId;

                    var originLines = new List<string>();
                    foreach (var lines in flightObj.origin.lines)
                    {
                        if (lines.type == "SYSTEM" || lines.type == "PLANET")
                        {
                            if ( lines.entity.name != lines.entity.naturalId)
                            {
                                originLines.Add($"{lines.entity.name} ({lines.entity.naturalId})");
                            }
                            else
                            {
                                originLines.Add(lines.entity.naturalId);
                            }
                        }
                        else
                        {
                            originLines.Add(lines.type);
                        }
                    }
                    flight.Origin = string.Join(" - ", originLines);


                    var destLines = new List<string>();
                    foreach (var lines in flightObj.destination.lines)
                    {
                        if (lines.type == "SYSTEM" || lines.type == "PLANET")
                        {
                            if (lines.entity.name != lines.entity.naturalId)
                            {
                                destLines.Add($"{lines.entity.name} ({lines.entity.naturalId})");
                            }
                            else
                            {
                                destLines.Add(lines.entity.naturalId);
                            }
                        }
                        else
                        {
                            destLines.Add(lines.type);
                        }
                    }
                    flight.Destination = string.Join(" - ", destLines);

                    flight.DepartureTimeEpochMs = flightObj.departure.timestamp;
                    flight.ArrivalTimeEpochMs = flightObj.arrival.timestamp;

                    foreach (var segment in flightObj.segments)
                    {
                        FLIGHTSFlightSegment flightSegment = new FLIGHTSFlightSegment();

                        flightSegment.Type = segment.type;
                        flightSegment.DepartureTimeEpochMs = segment.departure.timestamp;
                        flightSegment.ArrivalTimeEpochMs = segment.arrival.timestamp;

                        flightSegment.StlDistance = segment.stlDistance;
                        flightSegment.StlFuelConsumption = segment.stlFuelConsumption;
                        flightSegment.FtlDistance = segment.ftlDistance;
                        flightSegment.FtlFuelConsumption = segment.ftlFuelConsumption;

                        if (segment.origin != null)
                        {
                            flightSegment.Origin = "";
                            foreach ( var segLine in segment.origin.lines)
                            {
                                if (segLine.orbit != null)
                                {
                                    flightSegment.Origin += " Orbit -";
                                }
                                else
                                {
                                    string entityName = (segLine.entity.name == segLine.entity.naturalId) ? segLine.entity.naturalId : $"{segLine.entity.name} ({segLine.entity.naturalId})";
                                    flightSegment.Origin += $" {entityName} -";
                                }

                                if(segLine.entity != null)
                                {
                                    OriginLine line = new OriginLine();
                                    line.Type = segLine.entity._type;
                                    line.LineId = segLine.entity.id;
                                    line.LineName = segLine.entity.name;
                                    line.LineNaturalId = segLine.entity.naturalId;
                                    flightSegment.OriginLines.Add(line);
                                }
                            }
                        }
                        flightSegment.Origin = flightSegment.Origin.Substring(0, flightSegment.Origin.Length - 2).Trim();

                        if (segment.destination != null)
                        {
                            flightSegment.Destination = "";
                            foreach (var segLine in segment.destination.lines)
                            {
                                if (segLine.orbit != null)
                                {
                                    flightSegment.Destination += " Orbit -";
                                }
                                else
                                {
                                    string entityName = (segLine.entity.name == segLine.entity.naturalId) ? segLine.entity.naturalId : $"{segLine.entity.name} ({segLine.entity.naturalId})";
                                    flightSegment.Destination += $" {entityName} -";
                                }

                                if (segLine.entity != null )
                                {
                                    DestinationLine line = new DestinationLine();
                                    line.Type = segLine.entity._type;
                                    line.LineId = segLine.entity.id;
                                    line.LineName = segLine.entity.name;
                                    line.LineNaturalId = segLine.entity.naturalId;
                                    flightSegment.DestinationLines.Add(line);
                                }
                            }
                        }
                        flightSegment.Destination = flightSegment.Destination.Substring(0, flightSegment.Destination.Length - 2).Trim();

                        flight.Segments.Add(flightSegment);
                    }

                    flight.CurrentSegmentIndex = flightObj.currentSegmentIndex;

                    flight.StlDistance = flightObj.stlDistance;
                    flight.FtlDistance = flightObj.ftlDistance;
                    flight.IsAborted = flightObj.aborted;

                    model.Flights.Add(flight);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.FLIGHTSModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetFlights(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Flight))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.FLIGHTSModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
