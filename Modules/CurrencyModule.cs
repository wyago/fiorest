using System;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class CurrencyModule : NancyModule
    {
        public CurrencyModule() : base("/currency")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostCurrencies();
            });

            Get("/{currency_pair_ticker}", parameters =>
            {
                return GetCurrency(parameters.currency_pair_ticker);
            });

            Get("/all", _ =>
            {
                return GetAll();
            });
        }


        private Response PostCurrencies()
        {
            using (var req = new FIORequest<JSONRepresentations.ForexCurrencyPairs.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload;
                foreach (var p in payload.pairs)
                {
                    var m = req.DB.FXDataModels.FirstOrDefault(x => x.BrokerId == p.brokerId);
                    bool isAdd = (m == null);

                    if (isAdd)
                        m = new FXDataModel();

                    m.BrokerId = p.brokerId;
                    m.BaseCurrencyCode = p.pair.@base.code;
                    m.BaseCurrencyName = p.pair.@base.name;
                    m.BaseCurrencyNumericCode = p.pair.@base.numericCode;
                    m.QuoteCurrencyCode = p.pair.@quote.code;
                    m.QuoteCurrencyName = p.pair.@quote.name;
                    m.QuoteCurrencyNumericCode = p.pair.@quote.numericCode;
                    m.High = (decimal)p.price.high.rate;
                    m.Low = (decimal)p.price.low.rate;
                    m.Open = (decimal)p.price.open.rate;
                    m.Previous = (decimal)p.price.previous.rate;
                    m.PriceUpdateEpochMs = p.price.time.timestamp;
                    m.Traded = (decimal)p.price.traded.amount;
                    m.Volume = (decimal)p.price.volume.amount;

                    m.UserNameSubmitted = req.UserName;
                    m.Timestamp = req.Now;

                    if (isAdd)
                        req.DB.FXDataModels.Add(m);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetCurrency(string CurrencyTicker)
        {
            string[] parts = CurrencyTicker.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 2)
            {
                string baseCode = parts[0].ToUpper();
                string quoteCode = parts[1].ToUpper();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.FXDataModels.Where(c => c.BaseCurrencyCode == baseCode && c.QuoteCurrencyCode == quoteCode).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }
            else
            {
                Response badRequest = "Invalid ticker format.";
                badRequest.StatusCode = HttpStatusCode.BadRequest;
                return badRequest;
            }
        }

        private Response GetAll()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.FXDataModels.ToList());
            }
        }
    }
}
