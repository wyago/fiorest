﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class SitesModule : NancyModule
    {
        public SitesModule() : base("/sites")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostSites();
            });

            Post("/built", _ =>
            {
                this.EnforceWriteAuth();
                return PostSiteBuilt();
            });

            Post("/demolish", _ =>
            {
                this.EnforceWriteAuth();
                return PostSiteDemolish();
            });

            Post("/warehouses", _ =>
            {
                this.EnforceWriteAuth();
                return PostWarehouses();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetSites(parameters.username);
            });

            Get("/{username}/{planet}", parameters =>
            {
                this.EnforceReadAuth();
                return GetSite(parameters.username, parameters.planet);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetPlanetSites(parameters.username);
            });

            Get("/warehouses/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWarehouses(parameters.username);
            });
        }

        class DegradationTracker
        {
            private readonly PRUNDataContext DB;
            private List<BuildingDegradationModel> TrackedModels;

            const double Threshold = 0.0001;
        
            public DegradationTracker(PRUNDataContext DB)
			{
                this.DB = DB;
                TrackedModels = new List<BuildingDegradationModel>();
			}
        
            public void Track(SITESBuilding building)
			{
                bool bRepaired = (building.BuildingLastRepair != null);
                long buildingAgeMs = Utils.GetCurrentEpochMs() - (bRepaired ? (long)building.BuildingLastRepair : building.BuildingCreated);
                TimeSpan buildingAgeTimeSpan = TimeSpan.FromMilliseconds(buildingAgeMs);
                double buildingAgeDays = buildingAgeTimeSpan.TotalDays.RoundToDecimalPlaces(3);
        
                // Skip buildings more than 200 days old
                if (buildingAgeDays > 200.0)
                    return;
        
                // First check TrackedModels to see if we've already added this entry
                if ( !TrackedModels.Any(d =>
                        d.HasBeenRepaired == bRepaired &&
                        d.BuildingTicker.ToUpper() == building.BuildingTicker.ToUpper() &&
                        (d.DaysSinceLastRepair - buildingAgeDays < Threshold && -(d.DaysSinceLastRepair - buildingAgeDays) < Threshold)))
				{
                    var bdm = new BuildingDegradationModel(building);
                    TrackedModels.Add(bdm);
				}
            }

            public void Apply()
			{
                foreach (var TrackedModel in TrackedModels)
				{
                    DB.BuildingDegradationModels.Upsert(TrackedModel)
                        .On(bd => new { bd.BuildingTicker, bd.DaysSinceLastRepair, bd.HasBeenRepaired })
                        .Run();

                    var dbm_id = DB.BuildingDegradationModels
                        .Where(bd =>
                            bd.BuildingTicker == TrackedModel.BuildingTicker &&
                            bd.DaysSinceLastRepair == TrackedModel.DaysSinceLastRepair &&
                            bd.HasBeenRepaired == TrackedModel.HasBeenRepaired
                        )
                        .Select(bd => bd.BuildingDegradationModelId).First();

                    List<BuildingDegradationReclaimableMaterialModel> reclaimList = new List<BuildingDegradationReclaimableMaterialModel>();
                    foreach (var reclaimableMaterial in TrackedModel.ReclaimableMaterials)
                    {
                        var bdrmm = new BuildingDegradationReclaimableMaterialModel();
                        bdrmm.MaterialTicker = reclaimableMaterial.MaterialTicker;
                        bdrmm.MaterialCount = reclaimableMaterial.MaterialCount;
                        bdrmm.BuildingDegradationModelId = dbm_id;
                        reclaimList.Add(bdrmm);
                    }
                    DB.BuildingDegradationReclaimableMaterialModels.UpsertRange(reclaimList)
                        .On(rmm => new { rmm.BuildingDegradationModelId, rmm.MaterialTicker })
                        .Run();

                    List<BuildingDegradationRepairMaterialModel> repairList = new List<BuildingDegradationRepairMaterialModel>();
                    foreach (var repairMaterial in TrackedModel.RepairMaterials)
                    {
                        var bdrmm = new BuildingDegradationRepairMaterialModel();
                        bdrmm.MaterialTicker = repairMaterial.MaterialTicker;
                        bdrmm.MaterialCount = repairMaterial.MaterialCount;
                        bdrmm.BuildingDegradationModelId = dbm_id;
                        repairList.Add(bdrmm);
                    }
                    DB.BuildingDegradationRepairMaterialModels.UpsertRange(repairList)
                        .On(rmm => new { rmm.BuildingDegradationModelId, rmm.MaterialTicker })
                        .Run();
                }
            }
        }

        private Response PostSites()
        {
            using (var req = new FIORequest<JSONRepresentations.SiteSites.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                SITESModel model = req.DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == req.UserName).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new SITESModel();
                }
                else
                {
                    model.Sites.Clear();
                }

                var data = rootObj.payload.message.payload;

                var degradationTracker = new DegradationTracker(req.DB);
                foreach (var site in data.sites)
                {
                    SITESSite sitesSite = new SITESSite();

                    sitesSite.SiteId = site.siteId;
                    foreach (var line in site.address.lines)
                    {
                        if (line.type == "PLANET")
                        {
                            sitesSite.PlanetId = line.entity.id;
                            sitesSite.PlanetIdentifier = line.entity.naturalId;
                            sitesSite.PlanetName = line.entity.name;
                            break;
                        }
                    }
                    sitesSite.PlanetFoundedEpochMs = site.founded.timestamp;

                    foreach (var platform in site.platforms)
                    {
                        SITESBuilding sitesBuilding = new SITESBuilding();

                        sitesBuilding.BuildingCreated = platform.creationTime.timestamp;
                        sitesBuilding.BuildingId = platform.module.id;
                        sitesBuilding.BuildingName = platform.module.reactorName;
                        sitesBuilding.BuildingTicker = platform.module.reactorTicker;
                        sitesBuilding.BuildingLastRepair = platform.lastRepair?.timestamp;
                        sitesBuilding.Condition = platform.condition;

                        foreach (var reclaimable in platform.reclaimableMaterials)
                        {
                            SITESReclaimableMaterial sitesReclaimable = new SITESReclaimableMaterial();

                            sitesReclaimable.MaterialId = reclaimable.material.id;
                            sitesReclaimable.MaterialTicker = reclaimable.material.ticker;
                            sitesReclaimable.MaterialAmount = reclaimable.amount;
                            sitesReclaimable.MaterialName = reclaimable.material.name;

                            sitesBuilding.ReclaimableMaterials.Add(sitesReclaimable);
                        }

                        foreach (var repairable in platform.repairMaterials)
                        {
                            SITESRepairMaterial sitesRepairable = new SITESRepairMaterial();

                            sitesRepairable.MaterialId = repairable.material.id;
                            sitesRepairable.MaterialTicker = repairable.material.ticker;
                            sitesRepairable.MaterialAmount = repairable.amount;
                            sitesRepairable.MaterialName = repairable.material.name;

                            sitesBuilding.RepairMaterials.Add(sitesRepairable);
                        }

                        sitesSite.Buildings.Add(sitesBuilding);
                        degradationTracker.Track(sitesBuilding);
                    }

                    model.Sites.Add(sitesSite);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.SITESModels.Add(model);
                }

                req.DB.SaveChanges();
                req.Commit();

                // Apply degradation
                req.CreateNewTransaction();
                degradationTracker.Apply();
                req.DB.SaveChanges();
                req.Commit();

                return HttpStatusCode.OK;
            }
        }

        private Response PostSiteBuilt()
        {
            using (var req = new FIORequest<JSONRepresentations.SitePlatformBuilt.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var site = req.DB.SITESSites.Where(ss => ss.SiteId == payload.siteId).FirstOrDefault();
                if (site != null)
                {
                    var newBuilding = new SITESBuilding();

                    newBuilding.BuildingCreated = payload.creationTime.timestamp;
                    newBuilding.BuildingId = payload.module.reactorId;
                    newBuilding.BuildingName = payload.module.reactorName;
                    newBuilding.BuildingTicker = payload.module.reactorTicker;
                    newBuilding.Condition = payload.condition;

                    foreach(var payloadReclaimable in payload.reclaimableMaterials)
                    {
                        var newReclaimable = new SITESReclaimableMaterial();

                        newReclaimable.MaterialId = payloadReclaimable.material.id;
                        newReclaimable.MaterialName = payloadReclaimable.material.name;
                        newReclaimable.MaterialTicker = payloadReclaimable.material.ticker;
                        newReclaimable.MaterialAmount = payloadReclaimable.amount;

                        newBuilding.ReclaimableMaterials.Add(newReclaimable);
                    }

                    // repairMaterials is empty on a new build...

                    site.Buildings.Add(newBuilding);
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostSiteDemolish()
        {
            using (var req = new FIORequest<JSONRepresentations.SiteDemolish.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var site = req.DB.SITESSites.Where(ss => ss.SiteId == payload.siteId).FirstOrDefault();
                if (site != null)
                {
                    var building = site.Buildings.Where(b => b.BuildingId == payload.platformId).FirstOrDefault();
                    if (building != null)
                    {
                        site.Buildings.Remove(building);
                        req.DB.SaveChanges();
                    }
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostWarehouses()
        {
            using (var req = new FIORequest<JSONRepresentations.WarehouseStorages.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var storages = req.JsonPayload.payload.message.payload.storages;
                foreach(var warehouse in storages)
                {
                    var model = req.DB.WarehouseModels.Where(wm => wm.WarehouseId == warehouse.warehouseId && wm.StoreId == warehouse.storeId).FirstOrDefault();
                    bool bIsAdd = (model == null);
                    if (bIsAdd)
                    {
                        model = new WarehouseModel();
                    }

                    model.WarehouseId = warehouse.warehouseId;
                    model.StoreId = warehouse.storeId;
                    model.Units = warehouse.units;
                    model.WeightCapacity = warehouse.weightCapacity;
                    model.VolumeCapacity = warehouse.volumeCapacity;
                    if ( warehouse.nextPayment != null )
                    {
                        model.NextPaymentTimestampEpochMs = warehouse.nextPayment.timestamp;
                    }
                    
                    if ( warehouse.fee != null )
                    {
                        model.FeeAmount = warehouse.fee.amount;
                        model.FeeCurrency = warehouse.fee.currency;
                    }

                    if ( warehouse.feeCollector != null )
                    {
                        model.FeeCollectorId = warehouse.feeCollector.id;
                        model.FeeCollectorName = warehouse.feeCollector.name;
                        model.FeeCollectorCode = warehouse.feeCollector.code;
                    }

                    model.LocationName = warehouse.address.lines[warehouse.address.lines.Length - 1].entity.name;
                    model.LocationNaturalId = warehouse.address.lines[warehouse.address.lines.Length - 1].entity.naturalId;

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    if (bIsAdd)
                    {
                        req.DB.WarehouseModels.Add(model);
                    }
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetSite(string UserName, string Planet)
        {
            UserName = UserName.ToUpper();
            Planet = Planet.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var site = sites.Sites.Where(s => s.PlanetId.ToUpper() == Planet || s.PlanetIdentifier.ToUpper() == Planet || s.PlanetName.ToUpper() == Planet).FirstOrDefault();
                        if ( site != null)
                        {
                            return JsonConvert.SerializeObject(site);
                        }
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetPlanetSites(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var planets = sites.Sites.Select(s => s.PlanetId).ToList();
                        return JsonConvert.SerializeObject(planets);
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWarehouses(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if ( Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using(var DB = PRUNDataContext.GetNewContext())
                {
                    var warehouses = DB.WarehouseModels.Where(w => w.UserNameSubmitted.ToUpper() == UserName).ToList();
                    return JsonConvert.SerializeObject(warehouses);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
