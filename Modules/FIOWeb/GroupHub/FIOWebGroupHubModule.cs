﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules.FIOWeb.GroupHub
{
    public class FIOWebGroupHubModule : NancyModule
    {
        public FIOWebGroupHubModule() : base("/fioweb/grouphub")
        {
            this.EnforceReadAuth();

            Get("/{group_name}", parameters =>
            {
                return GetGroupHubData(parameters.group_name);
            });

            Post("/", _ =>
            {
                return PostGetGroupHubData();
            });
        }

        private Response GetGroupHubData(string GroupName)
        {
            // @TODO: This should replace PostGetGroupHubData
            return null;
        }

        private Response PostGetGroupHubData()
        {
            using (var req = new FIORequest<List<string>>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var materials = req.DB.MATModels.ToList();
                var buildings = req.DB.BUIModels.ToList();
                if (materials.Count == 0 || buildings.Count == 0 )
                {
                    Response noMatOrBuiDataResponse = (Response)"Unable to find MAT/BUI data";
                    noMatOrBuiDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noMatOrBuiDataResponse.ContentType = "text/plain";
                    return noMatOrBuiDataResponse;
                }

                List<string> UserNames = req.JsonPayload;
                string RequesterUserName = Request.GetUserName();

                var sfVolume = req.DB.MATModels.Where(mm => mm.Ticker == "SF").Select(mm => mm.Volume).FirstOrDefault();
                var ffVolume = req.DB.MATModels.Where(mm => mm.Ticker == "FF").Select(mm => mm.Volume).FirstOrDefault();
                if (sfVolume == default(double) || ffVolume == default(double))
                {
                    Response noVolumeDataResponse = (Response)"Unable to find SF/FF Volume Data";
                    noVolumeDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noVolumeDataResponse.ContentType = "text/plain";
                    return noVolumeDataResponse;
                }

                var cxLocations = req.DB.ComexExchanges.ToList();
                if (cxLocations.Count == 0)
                {
                    Response noCxDataResponse = (Response)"Unable to find CX location data";
                    noCxDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noCxDataResponse.ContentType = "text/plain";
                    return noCxDataResponse;
                }

                GroupHubModel result = new GroupHubModel();
                foreach (var UserName in UserNames)
                {
                    StringBuilder errorSb = new StringBuilder();

                    bool HasStoragePermissions = Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage);
                    bool HasBuildingPermissions = Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building);
                    bool HasFlightPermissions = Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Flight);
                    bool HasContractPermissions = Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Contracts);
                    bool HasProductionPermissions = Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production);
                    if (!HasStoragePermissions || !HasBuildingPermissions || !HasFlightPermissions || !HasContractPermissions || !HasProductionPermissions)
                    {
                        errorSb.Append($"Do not have the following permissions for '{UserName}': ");
                        if (!HasStoragePermissions)
                        {
                            errorSb.Append("Storage,");
                        }
                        if (!HasBuildingPermissions)
                        {
                            errorSb.Append("Building,");
                        }
                        if (!HasFlightPermissions)
                        {
                            errorSb.Append("Flight,");
                        }
                        if (!HasContractPermissions)
                        {
                            errorSb.Append("Contract,");
                        }
                        if (!HasProductionPermissions)
                        {
                            errorSb.Append("Production,");
                        }

                        // Remove last comma
                        errorSb.Remove(errorSb.Length - 1, 1);
                        result.Failures.Add(errorSb.ToString());
                    }

                    var warehouses = HasStoragePermissions ? req.DB.WarehouseModels.Where(war => war.UserNameSubmitted.ToUpper() == UserName.ToUpper()).ToList() : null;
                    var sites = HasBuildingPermissions ? req.DB.SITESModels.Where(sites => sites.UserNameSubmitted.ToUpper() == UserName.ToUpper()).FirstOrDefault() : null;
                    var storages = HasStoragePermissions ? req.DB.StorageModels.Where(store => store.UserNameSubmitted.ToUpper() == UserName.ToUpper()).Include(s => s.StorageItems).ToList() : null;
                    var ships = HasFlightPermissions ? req.DB.SHIPSModels.Where(ships => ships.UserNameSubmitted.ToUpper() == UserName.ToUpper()).Include(s => s.Ships).FirstOrDefault() : null;
                    var flights = HasFlightPermissions ? req.DB.FLIGHTSModels.Where(fm => fm.UserNameSubmitted.ToUpper() == UserName.ToUpper()).Include(f => f.Flights).FirstOrDefault() : null;
                    var companydata = HasContractPermissions ? req.DB.CompanyDataModels.Where(cdm => cdm.UserNameSubmitted.ToUpper() == UserName.ToUpper()).Include(c => c.Balances).FirstOrDefault() : null;

                    PRODLinesModel production = null;
                    if (HasProductionPermissions)
                    {
                        production = req.DB.PRODLinesModels.Where(plm => plm.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .Include(p => p.ProductionLines)
                                .ThenInclude(pl => pl.Orders)
                                    .ThenInclude(plo => plo.Inputs)
                            .Include(p => p.ProductionLines)
                                .ThenInclude(pl => pl.Orders)
                                    .ThenInclude(plo => plo.Outputs).FirstOrDefault();
                    }

                    // CXWarehouses
                    if (warehouses != null && storages != null)
                    {
                        foreach(var warehouse in warehouses)
                        {
                            if (!cxLocations.Any(cxl => cxl.LocationNaturalId == warehouse.LocationNaturalId))
                            {
                                // This isn't a CX
                                continue;
                            }

                            var existingWarehouse = result.CXWarehouses.Where(cxw => cxw.WarehouseLocationNaturalId == warehouse.LocationNaturalId).FirstOrDefault();
                            bool bAddWarehouse = (existingWarehouse == null);
                            if (bAddWarehouse)
                            {
                                existingWarehouse = new GroupCXWarehouse();
                                existingWarehouse.WarehouseLocationName = warehouse.LocationName;
                                existingWarehouse.WarehouseLocationNaturalId = warehouse.LocationNaturalId;
                            }

                            var warehouseStore = storages.Where(store => store.AddressableId == warehouse.WarehouseId).FirstOrDefault();
                            if (warehouseStore != null)
                            {
                                var playerStorage = new PlayerStorage();

                                playerStorage.PlayerName = UserName;
                                playerStorage.StorageType = warehouseStore.Type;

                                foreach(var storageItem in warehouseStore.StorageItems)
                                {
                                    var groupHubItem = new GroupHubInventoryItem();

                                    groupHubItem.MaterialName = storageItem.MaterialName;
                                    groupHubItem.MaterialTicker = storageItem.MaterialTicker;
                                    groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == storageItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                    groupHubItem.Units = storageItem.MaterialAmount;

                                    playerStorage.Items.Add(groupHubItem);
                                }

                                playerStorage.LastUpdated = warehouseStore.Timestamp;
                                existingWarehouse.PlayerCXWarehouses.Add(playerStorage);
                            }

                            if (bAddWarehouse)
                            {
                                result.CXWarehouses.Add(existingWarehouse);
                            }
                        }
                    }

                    // PlayerShipsInFlight
                    if (ships != null && flights != null && storages != null)
                    {
                        foreach( var ship in ships.Ships)
                        {
                            var playerShip = new PlayerShip();

                            playerShip.PlayerName = UserName;
                            playerShip.ShipName = ship.Name;
                            playerShip.ShipRegistration = ship.Registration;

                            var flight = flights.Flights.Where(f => f.ShipId == ship.ShipId).FirstOrDefault();
                            if ( flight != null)
                            {
                                // In flight
                                playerShip.Location = $"En route to {flight.Destination}";
                                playerShip.Destination = flight.Destination;
                                playerShip.LocationETA = Utils.FromUnixTime(flight.ArrivalTimeEpochMs);
                            }
                            else
                            {
                                continue;
                            }

                            var stlFuelStore = storages.Where(s => s.StorageId == ship.StlFuelStoreId).FirstOrDefault();
                            if (stlFuelStore != null)
                            {
                                var sfAmount = stlFuelStore.StorageItems.Where(si => si.MaterialTicker == "SF").Select(si => si.MaterialAmount).FirstOrDefault();
                                playerShip.Fuel.CurrentSF = sfAmount;
                                playerShip.Fuel.MaxSF = (int)(stlFuelStore.VolumeCapacity / sfVolume);
                            }

                            var ftlFuelStore = storages.Where(s => s.StorageId == ship.FtlFuelStoreId).FirstOrDefault();
                            if (ftlFuelStore != null)
                            {
                                var ffAmount = stlFuelStore.StorageItems.Where(si => si.MaterialTicker == "FF").Select(si => si.MaterialAmount).FirstOrDefault();
                                playerShip.Fuel.CurrentFF = ffAmount;
                                playerShip.Fuel.MaxFF = (int)(ftlFuelStore.VolumeCapacity / ffVolume);
                            }

                            var shipStore = storages.Where(s => ship.ShipId == s.AddressableId && s.Type == "SHIP_STORE").FirstOrDefault();
                            if (shipStore != null)
                            {
                                playerShip.Cargo = new PlayerStorage();
                                playerShip.Cargo.PlayerName = UserName;
                                playerShip.Cargo.StorageType = shipStore.Type;

                                foreach(var shipStoreItem in shipStore.StorageItems)
                                {
                                    var groupHubItem = new GroupHubInventoryItem();

                                    groupHubItem.MaterialName = shipStoreItem.MaterialName;
                                    groupHubItem.MaterialTicker = shipStoreItem.MaterialTicker;
                                    groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == shipStoreItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                    groupHubItem.Units = shipStoreItem.MaterialAmount;

                                    playerShip.Cargo.Items.Add(groupHubItem);
                                }

                                playerShip.Cargo.LastUpdated = shipStore.Timestamp;
                            }

                            playerShip.LastUpdated = ships.Timestamp;

                            result.PlayerShipsInFlight.Add(playerShip);
                        }
                    }

                    // PlayerModels
                    {
                        var playerModel = new PlayerModel();
                        playerModel.UserName = UserName;

                        if (companydata != null)
                        {
                            foreach (var balance in companydata.Balances)
                            {
                                if (balance.Balance > 0.0)
                                {
                                    var playerCurrency = new PlayerCurrency();

                                    playerCurrency.CurrencyName = balance.Currency;
                                    playerCurrency.Amount = balance.Balance;
                                    playerCurrency.LastUpdated = companydata.Timestamp;

                                    playerModel.Currencies.Add(playerCurrency);
                                }
                            }
                        }

                        if (sites != null)
                        {
                            foreach (var site in sites.Sites)
                            {
                                var playerLocation = new PlayerLocation();

                                playerLocation.LocationIdentifier = site.PlanetIdentifier;
                                playerLocation.LocationName = site.PlanetName;

                                // ProductionLines
                                if (production != null)
                                {
                                    var prodLines = production.ProductionLines.Where(pl => pl.PlanetNaturalId == site.PlanetIdentifier);
                                    foreach(var prodLine in prodLines)
                                    {
                                        var playerProductionLine = new PlayerProductionLine();

                                        playerProductionLine.BuildingName = prodLine.Type;
                                        playerProductionLine.BuildingTicker = buildings.Where(b => b.Name == prodLine.Type).Select(b => b.Ticker).FirstOrDefault();
                                        playerProductionLine.Capacity = prodLine.Capacity;
                                        playerProductionLine.Efficiency = prodLine.Efficiency;
                                        playerProductionLine.Condition = prodLine.Condition;

                                        foreach ( var prodOrder in prodLine.Orders )
                                        {
                                            var playerProductionOrder = new PlayerProductionOrder();

                                            playerProductionOrder.Started = (prodOrder.CompletionEpochMs != null);
                                            playerProductionOrder.Halted = prodOrder.IsHalted;
                                            playerProductionOrder.Recurring = prodOrder.Recurring;

                                            playerProductionOrder.OrderDuration = prodOrder.DurationMs != null ? TimeSpan.FromMilliseconds((double)prodOrder.DurationMs) : TimeSpan.FromMilliseconds(0.0);

                                            if ( prodOrder.CompletionEpochMs != null)
                                            {
                                                playerProductionOrder.TimeCompletion = Utils.FromUnixTime((long)prodOrder.CompletionEpochMs);
                                            }

                                            foreach(var prodInput in prodOrder.Inputs)
                                            {
                                                var groupHubItem = new GroupHubInventoryItem();

                                                groupHubItem.MaterialName = prodInput.MaterialName;
                                                groupHubItem.MaterialTicker = prodInput.MaterialTicker;
                                                groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == prodInput.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                                groupHubItem.Units = prodInput.MaterialAmount;

                                                playerProductionOrder.Inputs.Add(groupHubItem);
                                            }

                                            foreach(var prodOutput in prodOrder.Outputs)
                                            {
                                                var groupHubItem = new GroupHubInventoryItem();

                                                groupHubItem.MaterialName = prodOutput.MaterialName;
                                                groupHubItem.MaterialTicker = prodOutput.MaterialTicker;
                                                groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == prodOutput.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                                groupHubItem.Units = prodOutput.MaterialAmount;

                                                playerProductionOrder.Outputs.Add(groupHubItem);
                                            }

                                            playerProductionOrder.LastUpdated = production.Timestamp;

                                            playerProductionLine.ProductionOrders.Add(playerProductionOrder);
                                        }


                                        playerLocation.ProductionLines.Add(playerProductionLine);
                                    }
                                }

                                // BaseStorage
                                if (storages != null)
                                {
                                    var baseStorage = storages.Where(storage => storage.AddressableId == site.SiteId).FirstOrDefault();
                                    if (baseStorage != null)
                                    {
                                        var basePlayerStorage = new PlayerStorage();

                                        basePlayerStorage.PlayerName = UserName;
                                        basePlayerStorage.StorageType = baseStorage.Type;
                                        foreach (var baseItem in baseStorage.StorageItems)
                                        {
                                            var groupHubItem = new GroupHubInventoryItem();

                                            groupHubItem.MaterialName = baseItem.MaterialName;
                                            groupHubItem.MaterialTicker = baseItem.MaterialTicker;
                                            groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == baseItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                            groupHubItem.Units = baseItem.MaterialAmount;

                                            basePlayerStorage.Items.Add(groupHubItem);
                                        }
                                        basePlayerStorage.LastUpdated = baseStorage.Timestamp;

                                        playerLocation.BaseStorage = basePlayerStorage;
                                    }
                                }


                                // WarehouseStorage
                                if (warehouses != null && storages != null)
                                {
                                    var warehouse = warehouses.Where(warehouse => warehouse.WarehouseId == site.SiteId).FirstOrDefault();
                                    if (warehouse != null)
                                    {
                                        var warehouseStorage = storages.Where(storage => storage.AddressableId == warehouse.WarehouseId).FirstOrDefault();
                                        if (warehouseStorage != null)
                                        {
                                            var warehousePlayerStorage = new PlayerStorage();

                                            warehousePlayerStorage.PlayerName = UserName;
                                            warehousePlayerStorage.StorageType = warehouseStorage.Type;
                                            foreach (var warItem in warehouseStorage.StorageItems)
                                            {
                                                var groupHubItem = new GroupHubInventoryItem();

                                                groupHubItem.MaterialName = warItem.MaterialName;
                                                groupHubItem.MaterialTicker = warItem.MaterialTicker;
                                                groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == warItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                                groupHubItem.Units = warItem.MaterialAmount;

                                                warehousePlayerStorage.Items.Add(groupHubItem);
                                            }
                                            warehousePlayerStorage.LastUpdated = warehouseStorage.Timestamp;

                                            playerLocation.WarehouseStorage = warehousePlayerStorage;
                                        }
                                    }
                                }
                                

                                // StationaryPlayerShips
                                if (ships != null)
                                {
                                    foreach (var ship in ships.Ships)
                                    {
                                        var playerShip = new PlayerShip();

                                        playerShip.PlayerName = UserName;
                                        playerShip.ShipName = ship.Name;
                                        playerShip.ShipRegistration = ship.Registration;

                                        var flight = flights.Flights.Where(f => f.ShipId == ship.ShipId).FirstOrDefault();
                                        if (flight != null)
                                        {
                                            // In flight - skip
                                            continue;
                                        }
                                        else
                                        {
                                            playerShip.Location = ship.Location;
                                        }

                                        if ( storages != null)
                                        {
                                            var stlFuelStore = storages.Where(s => s.StorageId == ship.StlFuelStoreId).FirstOrDefault();
                                            if (stlFuelStore != null)
                                            {
                                                var sfAmount = stlFuelStore.StorageItems.Where(si => si.MaterialTicker == "SF").Select(si => si.MaterialAmount).FirstOrDefault();
                                                playerShip.Fuel.CurrentSF = sfAmount;
                                                playerShip.Fuel.MaxSF = (int)(stlFuelStore.VolumeCapacity / sfVolume);
                                            }

                                            var ftlFuelStore = storages.Where(s => s.StorageId == ship.FtlFuelStoreId).FirstOrDefault();
                                            if (ftlFuelStore != null)
                                            {
                                                var ffAmount = stlFuelStore.StorageItems.Where(si => si.MaterialTicker == "FF").Select(si => si.MaterialAmount).FirstOrDefault();
                                                playerShip.Fuel.CurrentFF = ffAmount;
                                                playerShip.Fuel.MaxFF = (int)(ftlFuelStore.VolumeCapacity / ffVolume);
                                            }

                                            var shipStore = storages.Where(s => ship.ShipId == s.AddressableId && s.Type == "SHIP_STORE").FirstOrDefault();
                                            if (shipStore != null)
                                            {
                                                playerShip.Cargo = new PlayerStorage();
                                                playerShip.Cargo.PlayerName = UserName;
                                                playerShip.Cargo.StorageType = shipStore.Type;

                                                foreach (var shipStoreItem in shipStore.StorageItems)
                                                {
                                                    var groupHubItem = new GroupHubInventoryItem();

                                                    groupHubItem.MaterialName = shipStoreItem.MaterialName;
                                                    groupHubItem.MaterialTicker = shipStoreItem.MaterialTicker;
                                                    groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == shipStoreItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                                    groupHubItem.Units = shipStoreItem.MaterialAmount;

                                                    playerShip.Cargo.Items.Add(groupHubItem);
                                                }

                                                playerShip.Cargo.LastUpdated = shipStore.Timestamp;
                                            }
                                        }

                                        playerShip.LastUpdated = ships.Timestamp;

                                        playerLocation.StationaryPlayerShips.Add(playerShip);
                                    }
                                }

                                playerModel.Locations.Add(playerLocation);
                            }
                        }

                        result.PlayerModels.Add(playerModel);
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
        }
    }
}
