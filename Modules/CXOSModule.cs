﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class CXOSModule : NancyModule
    {
        public CXOSModule() : base("/cxos")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOS();
            });

            Post("/added", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSAdded();
            });

            Post("/removed", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSRemoved();
            });

            Post("/updated", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXOSUpdated();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetCXOS(parameters.username);
            });
        }

        private Response PostCXOS()
        {
            using(var req = new FIORequest<JSONRepresentations.JsonComexTraderOrders.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var orders = req.JsonPayload.payload.message.payload.orders;
                CXOSTradeOrdersModel model = req.DB.CXOSTradeOrderModels.Where(c => c.UserNameSubmitted.ToUpper() == req.UserName).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new CXOSTradeOrdersModel();
                }
                else
                {
                    model.TradeOrders.Clear();
                }

                foreach(var order in orders)
                {
                    var newOrder = new CXOSTradeOrder();

                    newOrder.TradeOrderId = order.id;

                    newOrder.ExchangeName = order.exchange.name;
                    newOrder.ExchangeCode = order.exchange.code;

                    newOrder.BrokerId = order.brokerId;
                    newOrder.OrderType = order.type;
                    newOrder.MaterialName = order.material.name;
                    newOrder.MaterialTicker = order.material.ticker;
                    newOrder.MaterialId = order.material.id;

                    newOrder.Amount = order.amount;
                    newOrder.InitialAmount = order.initialAmount;

                    newOrder.Limit = order.limit.amount;
                    newOrder.LimitCurrency = order.limit.currency;

                    newOrder.Status = order.status;
                    newOrder.CreatedEpochMs = order.created.timestamp;

                    foreach(var trade in order.trades)
                    {
                        var newTrade = new CXOSTrade();

                        newTrade.TradeId = trade.id;
                        newTrade.Amount = trade.amount;
                        newTrade.Price = trade.price.amount;
                        newTrade.PriceCurrency = trade.price.currency;

                        newTrade.TradeTimeEpochMs = trade.time.timestamp;

                        newTrade.PartnerId = trade.partner.id;
                        newTrade.PartnerName = trade.partner.name;
                        newTrade.PartnerCode = trade.partner.code;

                        newOrder.Trades.Add(newTrade);
                    }

                    model.TradeOrders.Add(newOrder);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.CXOSTradeOrderModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSAdded()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderAdded.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var order = req.JsonPayload.payload;
                var parentModel = req.DB.CXOSTradeOrderModels.Where(cx => cx.UserNameSubmitted == req.UserName).FirstOrDefault();
                var model = req.DB.CXOSTradeOrders.Where(c => c.TradeOrderId.ToUpper() == order.id.ToUpper()).FirstOrDefault();
                if (parentModel != null && model == null)
                {
                    model = new CXOSTradeOrder();

                    model.TradeOrderId = order.id;

                    model.ExchangeName = order.exchange.name;
                    model.ExchangeCode = order.exchange.code;

                    model.BrokerId = order.brokerId;
                    model.OrderType = order.type;
                    model.MaterialName = order.material.name;
                    model.MaterialTicker = order.material.ticker;
                    model.MaterialId = order.material.id;

                    model.Amount = order.amount;
                    model.InitialAmount = order.initialAmount;

                    model.Limit = order.limit.amount;
                    model.LimitCurrency = order.limit.currency;

                    model.Status = order.status;
                    model.CreatedEpochMs = order.created.timestamp;

                    parentModel.TradeOrders.Add(model);
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var orderId = req.JsonPayload.payload.message.payload.orderId;
                var model = req.DB.CXOSTradeOrders.Where(c => c.TradeOrderId.ToUpper() == orderId.ToUpper()).FirstOrDefault();
                if (model != null)
                {
                    model.CXOSTradeOrdersModel.TradeOrders.Remove(model);
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostCXOSUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonComexTraderOrderUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var order = req.JsonPayload.payload;
                var model = req.DB.CXOSTradeOrders.Where(c => c.TradeOrderId.ToUpper() == order.id.ToUpper()).FirstOrDefault();
                if (model != null)
                {
                    model.Trades.Clear();

                    model.TradeOrderId = order.id;

                    model.ExchangeName = order.exchange.name;
                    model.ExchangeCode = order.exchange.code;

                    model.BrokerId = order.brokerId;
                    model.OrderType = order.type;
                    model.MaterialName = order.material.name;
                    model.MaterialTicker = order.material.ticker;
                    model.MaterialId = order.material.id;

                    model.Amount = order.amount;
                    model.InitialAmount = order.initialAmount;

                    model.Limit = order.limit.amount;
                    model.LimitCurrency = order.limit.currency;

                    model.Status = order.status;
                    model.CreatedEpochMs = order.created.timestamp;

                    foreach (var trade in order.trades)
                    {
                        var newTrade = new CXOSTrade();

                        newTrade.TradeId = trade.id;
                        newTrade.Amount = trade.amount;
                        newTrade.Price = trade.price.amount;
                        newTrade.PriceCurrency = trade.price.currency;

                        newTrade.TradeTimeEpochMs = trade.time.timestamp;

                        newTrade.PartnerId = trade.partner.id;
                        newTrade.PartnerName = trade.partner.name;
                        newTrade.PartnerCode = trade.partner.code;

                        model.Trades.Add(newTrade);
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetCXOS(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Contracts))
            {
                using(var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.CXOSTradeOrderModels.Where(c => c.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
