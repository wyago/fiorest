using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Nancy;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class LoginPayload
    {
        public string UserName;
        public string Password;
    }

    public class LoginResponsePayload
    {
        public string AuthToken;
        public DateTime Expiry;
        public bool IsAdministrator;
    }

    public class ChangePasswordPayload
    {
        public string OldPassword;
        public string NewPassword;
    }

    public class AuthModule : NancyModule
    {
        public AuthModule() : base("/auth")
        {
            Get("/", parameters =>
            {
                if (Request.IsReadAuthenticated())
                {
                    Response resp = Request.GetUserName();
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.OK;
                    return resp;
                }
                else return HttpStatusCode.Unauthorized;
            });

            Post("/login", _ =>
            {
                return PostLogin();
            });

            Post("/refreshauthtoken", _ =>
            {
                return PostRefreshAuthToken();
            });

            Post("/changepassword", _ =>
            {
                return PostChangePassword();
            });

            Post("/addpermission", _ =>
            {
                return PostAddPermission();
            });

            Post("/deletepermission/{username}", parameters =>
            {
                return PostDeletePermission(parameters.username);
            });

            Post("/autorequestregister", _ =>
            {
                return PostAutoRequestRegister();
            });

            Post("/register", _ =>
            {
                return PostRegister();
            });

            Post("/createapikey", _ =>
            {
                return PostCreateAPIKey();
            });

            Post("/revokeapikey", _ =>
            {
                return PostRevokeAPIKey();
            });

            Get("/listapikeys", _ =>
            {
                this.EnforceWriteAuth();
                return GetListAPIKeys();
            });

            Post("/listapikeys", _ =>
            {
                return PostListAPIKeys();
            });

            Get("/permissions", _ =>
            {
                return GetPermissions();
            });

            Get("/visibility", _ =>
            {
                return GetVisibility();
            });

            Get("/visibility/{permissiontype}", parameters =>
            {
                return GetVisibility(parameters.permissiontype);
            });

            Post("/creategroup", _ =>
            {
                this.EnforceWriteAuth();
                return PostCreateGroup();
            });

            Post("/deletegroup/{groupid:int}", parameters =>
            {
                this.EnforceWriteAuth();
                return PostDeleteGroup(parameters.groupid);
            });

            Get("/groups", _ =>
            {
                this.EnforceReadAuth();
                return GetMyGroups();
            });

            Get("/group/{groupid:int}", parameters =>
            {
                return GetGroup(parameters.groupid);
            });
        }

        private Response PostLogin()
        {
            using (var req = new FIORequest<LoginPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    // First check for failed attempts brute force
                    DateTime fiveMinutesAgo = req.Now.AddMinutes(-5);
                    var failedAttempts = result.FailedAttempts.Where(f => f.FailedAttemptDateTime > fiveMinutesAgo && f.Address == req.Request.UserHostAddress).ToList();
                    if (failedAttempts.Count > 5)
                    {
                        Response resp = "Too many failed attempts. Locked out.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.Unauthorized;
                        return resp;
                    }

                    if (!result.AccountEnabled)
                    {
                        Response resp = "Account disabled.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.Unauthorized;
                        return resp;
                    }

                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        if (req.Now < result.AuthorizationExpiry)
                        {
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }
                        else
                        {
                            result.AuthorizationKey = Guid.NewGuid();
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }

                        LoginResponsePayload response = new LoginResponsePayload();
                        response.AuthToken = result.AuthorizationKey.ToString("N");
                        response.Expiry = result.AuthorizationExpiry;
                        response.IsAdministrator = result.IsAdministrator;

                        req.DB.SaveChanges();

                        Response resp = JsonConvert.SerializeObject(response);
                        resp.ContentType = "application/json";
                        resp.Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } };
                        return resp;
                    }
                    else
                    {
                        result.FailedAttempts.Add(new FailedLoginAttempt { FailedAttemptDateTime = req.Now, Address = req.Request.UserHostAddress });
                        req.DB.SaveChanges();
                        return HttpStatusCode.Unauthorized;
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response PostRefreshAuthToken()
        {
            if(Request.IsReadAuthenticated())
            {
                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var RequestAuthToken = new Guid(Request.GetAuthToken());
                    var result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == RequestAuthToken).FirstOrDefault();
                    if (result != null)
                    {
                        result.AuthorizationExpiry = DateTime.UtcNow + new TimeSpan(1, 0, 0, 0);
                        DB.SaveChanges();
                        transaction.Commit();

                        return HttpStatusCode.OK;
                    }
                }

                return HttpStatusCode.BadRequest;
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostChangePassword()
        {
            if (Request.IsWriteAuthenticated())
            {
                using (var req = new FIORequest<ChangePasswordPayload>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.UserName.ToUpper()).FirstOrDefault();
                    if (result != null)
                    {
                        if (SecurePasswordHasher.Verify(req.JsonPayload.OldPassword, result.PasswordHash))
                        {
                            result.PasswordHash = SecurePasswordHasher.Hash(req.JsonPayload.NewPassword);
                            req.DB.SaveChanges();
                            return HttpStatusCode.OK;
                        }
                        else
                        {
                            return HttpStatusCode.Unauthorized;
                        }
                    }
                    else
                    {
                        return HttpStatusCode.Unauthorized;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostAddPermission()
        {
            if (Request.IsWriteAuthenticated())
            {
                using (var req = new FIORequest<PermissionAllowance>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var setPermissionPayload = req.JsonPayload;

                    bool UserInPayloadExists = (req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault() != null);
                    if (setPermissionPayload.UserName == "*" || UserInPayloadExists)
                    {
                        string UserName = Request.GetUserName();
                        Auth.ResetPermissionCache(UserName);
                        var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == UserName).FirstOrDefault();
                        if (result != null)
                        {
                            PermissionAllowance existingPermission = result.Allowances.Where(a => a.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault();
                            if (existingPermission != null)
                            {
                                existingPermission.FlightData = setPermissionPayload.FlightData;
                                existingPermission.BuildingData = setPermissionPayload.BuildingData;
                                existingPermission.StorageData = setPermissionPayload.StorageData;
                                existingPermission.ProductionData = setPermissionPayload.ProductionData;
                                existingPermission.WorkforceData = setPermissionPayload.WorkforceData;
                                existingPermission.ExpertsData = setPermissionPayload.ExpertsData;
                                existingPermission.ContractData = setPermissionPayload.ContractData;
                                existingPermission.ShipmentTrackingData = setPermissionPayload.ShipmentTrackingData;
                            }
                            else
                            {
                                result.Allowances.Add(setPermissionPayload);
                            }
                            req.DB.SaveChanges();
                            return HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostDeletePermission(string PermUserName)
        {
            PermUserName = PermUserName.ToUpper();
            if (Request.IsWriteAuthenticated())
            {
                string UserName = Request.GetUserName();
                Auth.ResetPermissionCache(UserName);

                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var AuthModelId = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName).AuthenticationModelId;
                    var Allowance = DB.PermissionAllowances.Where(p => p.UserName.ToUpper() == PermUserName && p.AuthenticationModelId == AuthModelId).FirstOrDefault();
                    if (Allowance != null)
                    {
                        DB.PermissionAllowances.Attach(Allowance);
                        DB.PermissionAllowances.Remove(Allowance);
                        DB.SaveChanges();
                        transaction.Commit();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        class AuthAuthenticated
        { 
            public string username { get; set; }
            public bool admin { get; set; }
        }

        private Response PostAutoRequestRegister()
        {
            using (var req = new FIORequest<JSONRepresentations.AuthAuthenticated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var userName = req.JsonPayload.payload.username;
                var ExistingUser = req.DB.AuthenticationModels.Where(a => a.UserName == userName).FirstOrDefault();
                if (ExistingUser == null)
                {
                    // Look for an existing registration
                    var registration = req.DB.Registrations.Where(r => r.UserName == userName).FirstOrDefault();
                    if (registration != null)
                    {
                        // Delete it if it's older than 30 mins
                        DateTime thirtyMinsAgo = req.Now.AddMinutes(-30.0);
                        if (registration.RegistrationTime < thirtyMinsAgo)
                        {
                            req.DB.Registrations.Remove(registration);
                            registration = null;
                        }
                    }

                    if ( registration == null)
                    {
                        registration = new Registration();
                        registration.UserName = userName;
                        registration.RegistrationGuid = Guid.NewGuid().ToString("N");
                        registration.RegistrationTime = req.Now;
                        req.DB.Registrations.Add(registration);
                    }

                    req.DB.SaveChanges();
                    return JsonConvert.SerializeObject(registration);
                }

                // Send 204 if the user already exists
                return HttpStatusCode.NoContent;
            }
        }

        class PostRegistration
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string RegistrationGuid { get; set; }
        }

        private Response PostRegister()
        {
            using (var req = new FIORequest<PostRegistration>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                // Make sure the user doesn't exist already
                var existingUser = req.DB.AuthenticationModels.Where(a => a.UserName == req.JsonPayload.UserName).FirstOrDefault();
                if (existingUser != null)
                {
                    return HttpStatusCode.Unauthorized;
                }

                if (req.JsonPayload.Password.Length <= 4)
                {
                    return HttpStatusCode.BadRequest;
                }

                // Look for the registration
                var registration = req.DB.Registrations.Where(r => r.UserName == req.JsonPayload.UserName && r.RegistrationGuid.ToUpper() == req.JsonPayload.RegistrationGuid.ToUpper()).FirstOrDefault();
                if (registration != null)
                {
                    var authModel = new AuthenticationModel();
                    authModel.AccountEnabled = true;
                    authModel.UserName = req.JsonPayload.UserName;
                    authModel.PasswordHash = SecurePasswordHasher.Hash(req.JsonPayload.Password);
                    authModel.IsAdministrator = false;
                    req.DB.AuthenticationModels.Add(authModel);

                    req.DB.Registrations.Remove(registration);

                    req.DB.SaveChanges();
                    return HttpStatusCode.OK;
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        class CreateAPIKeyPayload
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Application { get; set; }
            public bool AllowWrites { get; set; }
        }

        private Response PostCreateAPIKey()
        {
            using (var req = new FIORequest<CreateAPIKeyPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                if (String.IsNullOrWhiteSpace(req.JsonPayload.Application) || String.IsNullOrWhiteSpace(req.JsonPayload.UserName) || String.IsNullOrWhiteSpace(req.JsonPayload.Password))
                {
                    return req.ReturnBadRequest();
                }

                if (req.JsonPayload.Application.Length > 255)
				{
                    Response resp = "Application length cannot exceed 255 characters";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.BadRequest;
                    return resp;
				}

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        if ( result.APIKeys.Count > 20 )
                        {
                            Response resp = "Too many APIKeys";
                            resp.ContentType = "text/plain";
                            resp.StatusCode = HttpStatusCode.NotAcceptable;
                            return resp;
                        }
                        else
                        {
                            var apiKey = new APIKey();
                            apiKey.AuthAPIKey = Guid.NewGuid();
                            apiKey.Application = req.JsonPayload.Application;
                            apiKey.AllowWrites = req.JsonPayload.AllowWrites;
                            apiKey.LastAccessTime = DateTime.UtcNow;
                            result.APIKeys.Add(apiKey);
                            req.DB.SaveChanges();

                            Response resp = apiKey.AuthAPIKey.ToString("N");
                            resp.ContentType = "text/plain";
                            resp.StatusCode = HttpStatusCode.OK;
                            return resp;
                        }
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        class RevokeAPIKeyPayload
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string ApiKeyToRevoke { get; set; }
        }

        private Response PostRevokeAPIKey()
        {
            using (var req = new FIORequest<RevokeAPIKeyPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        Guid ApiKeyGuid;
                        if (Guid.TryParse(req.JsonPayload.ApiKeyToRevoke, out ApiKeyGuid))
                        {
                            var apiKey = result.APIKeys.Where(ap => ap.AuthAPIKey == ApiKeyGuid).FirstOrDefault();
                            if (apiKey != null)
                            {
                                req.DB.APIKeys.Remove(apiKey);
                                req.DB.SaveChanges();
                                return HttpStatusCode.OK;
                            }
                            else
                            {
                                return HttpStatusCode.NoContent;
                            }
                        }
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetListAPIKeys()
        {
            string UserName = Request.GetUserName();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var AuthModel = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName);
                return JsonConvert.SerializeObject(AuthModel.APIKeys);
            }
        }

        private Response PostListAPIKeys()
        {
            using (var req = new FIORequest<LoginPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        return JsonConvert.SerializeObject(result.APIKeys.ToList());
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetPermissions()
        {
            if (Request.IsReadAuthenticated())
            {
                string UserName = Request.GetUserName();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var AuthModel = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName);
                    if ( AuthModel.Allowances != null )
                    {
                        return JsonConvert.SerializeObject(AuthModel.Allowances);
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new List<PermissionAllowance>());
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetVisibility(string permissionType = null)
        {
            if (Request.IsReadAuthenticated())
            {
                string UserName = Request.GetUserName();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    if( permissionType != null)
                    {
                        List<string> UserNames = new List<string>();
                        switch (permissionType.ToUpper())
                        {
                            case "FLIGHT":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.FlightData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "BUILDING":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.BuildingData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "STORAGE":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.StorageData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "PRODUCTION":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ProductionData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "WORKFORCE":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.WorkforceData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "EXPERTS":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ExpertsData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "CONTRACT":
                            case "CONTRACTS":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ContractData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            case "SHIPMENTTRACKING":
                                UserNames = DB.AuthenticationModels.Where(a => a.Allowances.Any(a => a.ShipmentTrackingData && (a.UserName.ToUpper() == UserName || a.UserName == "*"))).Select(a => a.UserName).ToList();
                                break;
                            default:
                                return HttpStatusCode.BadRequest;
                        }

                        // Remove ourselves
                        UserNames.Remove(UserName);

                        // Remove duplicates
                        UserNames = UserNames.Distinct().ToList();

                        return JsonConvert.SerializeObject(UserNames);
                    }
                    else
                    {
                        List<PermissionAllowance> allAllowances = DB.AuthenticationModels
                            .SelectMany(auth => auth.Allowances, (auth, allowances) => new {auth, allowances})
                            .Where(authAndAllowances => (authAndAllowances.allowances.UserName.ToUpper() == UserName || authAndAllowances.allowances.UserName == "*") && authAndAllowances.auth.UserName.ToUpper() != UserName)
                            .Select(authAndAllowances =>
                            new PermissionAllowance
                            {
                                UserName = authAndAllowances.auth.UserName,
                                FlightData = authAndAllowances.allowances.FlightData,
                                BuildingData = authAndAllowances.allowances.BuildingData,
                                StorageData = authAndAllowances.allowances.StorageData,
                                ProductionData = authAndAllowances.allowances.ProductionData,
                                WorkforceData = authAndAllowances.allowances.WorkforceData,
                                ExpertsData = authAndAllowances.allowances.ExpertsData,
                                ContractData = authAndAllowances.allowances.ContractData,
                                ShipmentTrackingData = authAndAllowances.allowances.ShipmentTrackingData
                            }).ToList();

                        // Remove duplicates & sort
                        allAllowances = allAllowances
                            .GroupBy(pa => pa.UserName)
                            .Select(group => group.First())
                            .OrderBy(pa => pa.UserName)
                            .ToList();

                        return JsonConvert.SerializeObject(allAllowances.Distinct());
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        class CreateGroupPayload
        {
            public string GroupId { get; set; }
            public string GroupName { get; set; }
            public List<string> GroupUsers { get; set; }
        }

        class CreateGroupResponse
        {
            public int GroupId { get; set; }
            public string GroupString { get; set; }
        }

        const int GroupModelLimit = 20;
        const int GroupMembershipLimit = 50;

        private const string GroupNameRegexPattern = @"^[a-zA-Z0-9]{1}[a-zA-Z0-9_]+$";
        private static Regex GroupNameRegex = new Regex(GroupNameRegexPattern, RegexOptions.Compiled);

        private Response PostCreateGroup()
		{
            string UserName = Request.GetUserName();
            using (var req = new FIORequest<CreateGroupPayload>(Request))
			{
                if (req.BadRequest)
				{
                    return req.ReturnBadRequest();
				}

                int GroupIdInt = -1;
                if (req.JsonPayload.GroupId != null)
                {
                    if (req.JsonPayload.GroupId.Length <= 8 && !int.TryParse(req.JsonPayload.GroupId, out GroupIdInt))
                    {
                        Response resp = "GroupId specified is invalid.  GroupName must be an integer value less than or equal to 8 digits.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.NotAcceptable;
                        return req.ReturnBadRequest();
                    }
                }

                if (String.IsNullOrWhiteSpace(req.JsonPayload.GroupName) || !GroupNameRegex.IsMatch(req.JsonPayload.GroupName) || req.JsonPayload.GroupName.Length >= 32)
				{
                    Response resp = "GroupName is invalid. GroupName must not be whitespace, must be alphanumeric or underscores, not have an underscore as the first character, and be less than 32 characters.";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.NotAcceptable;
                    return resp;
				}

                var GroupModelCount = req.DB.GroupModels.Where(g => g.GroupOwner == UserName).Count();
                if (GroupModelCount >= GroupModelLimit)
				{
                    Response resp = $"You cannot have more than {GroupModelLimit} groups.";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.NotAcceptable;
                    return resp;
                }

                bool bIsAdd = false;
                GroupModel model = null;
                if (req.JsonPayload.GroupId != null)
				{
                    // This is a edit
                    model = req.DB.GroupModels.FirstOrDefault(g => g.GroupOwner == UserName && g.GroupModelId == GroupIdInt);
                }
                else
				{
                    if (req.JsonPayload.GroupUsers.Count > GroupMembershipLimit)
                    {
                        Response resp = $"You cannot have more than {GroupMembershipLimit} members in a group.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.NotAcceptable;
                        return resp;
                    }

                    // This is a new group
                    model = new GroupModel();
                    bIsAdd = true;
                }

                if (model.GroupModelId == 0)
				{
                    GroupModel existingGroupWithId = null;
                    do
                    {
                        var bytes = new byte[4];
                        var rng = RandomNumberGenerator.Create();
                        rng.GetBytes(bytes);
                        uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
                        model.GroupModelId = int.Parse(String.Format("{0:D8}", random));
                        existingGroupWithId = req.DB.GroupModels.FirstOrDefault(g => g.GroupModelId == model.GroupModelId);
                    } while (existingGroupWithId != null);
				}

                model.GroupName = req.JsonPayload.GroupName;
                model.GroupOwner = UserName;

                // Get only the valid group usernames
                var groupUserEntries = (
                    from AuthModel in req.DB.AuthenticationModels.AsEnumerable()
                    join GroupUser in req.JsonPayload.GroupUsers
                        on AuthModel.UserName.ToUpper() equals GroupUser.ToUpper()
                    select GroupUser.ToUpper())
                    .Distinct()
                    .Select( GroupUser => new GroupUserEntryModel { GroupUserName = GroupUser.ToUpper() })
                    .ToList();

                model.GroupUsers.Clear();
                model.GroupUsers.AddRange(groupUserEntries);

                if (bIsAdd)
				{
                    req.DB.GroupModels.Add(model);
				}

                req.DB.SaveChanges();

                var respObj = new CreateGroupResponse();
                respObj.GroupId = model.GroupModelId;
                respObj.GroupString = $"{respObj.GroupId}-{model.GroupName}";
                return JsonConvert.SerializeObject(respObj);
            }
		}

        private Response PostDeleteGroup(int groupid)
		{
            string UserName = Request.GetUserName();

            using (var DB = PRUNDataContext.GetNewContext())
			{
                var model = DB.GroupModels.FirstOrDefault(gm => gm.GroupOwner == UserName && gm.GroupModelId == groupid);
                if (model != null)
				{
                    DB.Database.BeginTransaction();
                    DB.GroupModels.Remove(model);
                    DB.Database.CommitTransaction();
                    DB.SaveChanges();
                    return HttpStatusCode.OK;
                }
                else
				{
                    return HttpStatusCode.NoContent;
				}
			}
		}

        private Response GetMyGroups()
		{
            string UserName = Request.GetUserName();

            using (var DB = PRUNDataContext.GetNewContext())
			{
                var myGroups = DB.GroupModels.Where(gm => gm.GroupOwner == UserName).ToList();
                return JsonConvert.SerializeObject(myGroups);
			}
		}

        private Response GetGroup(int groupid)
		{
            using(var DB = PRUNDataContext.GetNewContext())
			{
                var group = DB.GroupModels.FirstOrDefault(gm => gm.GroupModelId == groupid);
                if (group != null)
				{
                    return JsonConvert.SerializeObject(group);
				}
                else
				{
                    return HttpStatusCode.NoContent;
				}
			}
		}
    }
}
