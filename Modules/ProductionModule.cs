﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using System.Collections.Generic;

namespace FIORest.Modules
{
    public class ProductionModule : NancyModule
    {
        public ProductionModule() : base("/production")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostProduction();
            });

            Post("/lineupdated", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionLineUpdated();
            });

            Post("/lineremoved", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionLineRemoved();
            });

            Post("/orderupdated", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionOrderUpdated();
            });

            Post("/orderremoved", _ =>
            {
                this.EnforceWriteAuth();
                return PostProductionOrderRemoved();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProduction(parameters.username);
            });

            Get("/{username}/{planet_or_site_id}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProduction(parameters.username, parameters.planet_or_site_id);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetProductionPlanets(parameters.username);
            });
        }

        private Response PostProduction()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionSiteProductionLines.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = req.DB.PRODLinesModels.Where(p => p.UserNameSubmitted.ToUpper() == req.UserName.ToUpper() && p.SiteId == data.siteId).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new PRODLinesModel();
                }
                else
                {
                    model.ProductionLines.Clear();
                }

                model.SiteId = data.siteId;

                foreach (var productionLine in data.productionLines)
                {
                    ProductionLine prodLine = new ProductionLine();

                    prodLine.LineId = productionLine.id;
                    prodLine.SiteId = productionLine.siteId;

                    prodLine.PlanetId = productionLine.address.lines[1].entity.id;
                    prodLine.PlanetNaturalId = productionLine.address.lines[1].entity.naturalId;
                    prodLine.PlanetName = productionLine.address.lines[1].entity.name;

                    prodLine.Type = productionLine.type;
                    prodLine.Capacity = productionLine.capacity;

                    prodLine.Efficiency = productionLine.efficiency;
                    prodLine.Condition = productionLine.condition;

                    foreach (var order in productionLine.orders)
                    {
                        ProductionLineOrder prodOrder = new ProductionLineOrder();

                        prodOrder.ProductionId = order.id;

                        foreach (var input in order.inputs)
                        {
                            ProductionLineInput prodInput = new ProductionLineInput();

                            prodInput.MaterialName = input.material.name;
                            prodInput.MaterialTicker = input.material.ticker;
                            prodInput.MaterialId = input.material.id;
                            prodInput.MaterialAmount = input.amount;

                            prodOrder.Inputs.Add(prodInput);
                        }

                        foreach (var output in order.outputs)
                        {
                            ProductionLineOutput prodOutput = new ProductionLineOutput();

                            prodOutput.MaterialName = output.material.name;
                            prodOutput.MaterialTicker = output.material.ticker;
                            prodOutput.MaterialId = output.material.id;
                            prodOutput.MaterialAmount = output.amount;

                            prodOrder.Outputs.Add(prodOutput);
                        }

                        prodOrder.CreatedEpochMs = order.created?.timestamp;
                        prodOrder.StartedEpochMs = order.started?.timestamp;
                        prodOrder.CompletionEpochMs = order.completion?.timestamp;
                        prodOrder.DurationMs = order.duration?.millis;
                        prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;
                        prodOrder.CompletedPercentage = order?.completed;

                        prodOrder.IsHalted = order.halted;
                        prodOrder.Recurring = order.recurring;

                        prodOrder.ProductionFee = order.productionFee.amount;
                        prodOrder.ProductionFeeCurrency = order.productionFee.currency;

                        prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                        prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                        prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                        prodLine.Orders.Add(prodOrder);
                    }

                    model.ProductionLines.Add(prodLine);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.PRODLinesModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionLineUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionLineUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var productionLine = req.JsonPayload.payload;
                var prodLine = req.DB.ProductionLines.Where(pl => pl.SiteId == productionLine.siteId && pl.LineId == productionLine.id).FirstOrDefault();
                if (prodLine != null)
                {
                    prodLine.LineId = productionLine.id;
                    prodLine.SiteId = productionLine.siteId;

                    prodLine.PlanetId = productionLine.address.lines[1].entity.id;
                    prodLine.PlanetNaturalId = productionLine.address.lines[1].entity.naturalId;
                    prodLine.PlanetName = productionLine.address.lines[1].entity.name;

                    prodLine.Type = productionLine.type;
                    prodLine.Capacity = productionLine.capacity;

                    prodLine.Efficiency = productionLine.efficiency;
                    prodLine.Condition = productionLine.condition;

                    prodLine.Orders.Clear();

                    foreach (var order in productionLine.orders)
                    {
                        ProductionLineOrder prodOrder = new ProductionLineOrder();

                        prodOrder.ProductionId = order.id;

                        foreach (var input in order.inputs)
                        {
                            ProductionLineInput prodInput = new ProductionLineInput();

                            prodInput.MaterialName = input.material.name;
                            prodInput.MaterialTicker = input.material.ticker;
                            prodInput.MaterialId = input.material.id;
                            prodInput.MaterialAmount = input.amount;

                            prodOrder.Inputs.Add(prodInput);
                        }

                        foreach (var output in order.outputs)
                        {
                            ProductionLineOutput prodOutput = new ProductionLineOutput();

                            prodOutput.MaterialName = output.material.name;
                            prodOutput.MaterialTicker = output.material.ticker;
                            prodOutput.MaterialId = output.material.id;
                            prodOutput.MaterialAmount = output.amount;

                            prodOrder.Outputs.Add(prodOutput);
                        }

                        prodOrder.CreatedEpochMs = order.created?.timestamp;
                        prodOrder.StartedEpochMs = order.started?.timestamp;
                        prodOrder.CompletionEpochMs = order.completion?.timestamp;
                        prodOrder.DurationMs = order.duration?.millis;
                        prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;
                        prodOrder.CompletedPercentage = order?.completed;

                        prodOrder.IsHalted = order.halted;
                        prodOrder.Recurring = order.recurring;

                        prodOrder.ProductionFee = order.productionFee.amount;
                        prodOrder.ProductionFeeCurrency = order.productionFee.currency;

                        prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                        prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                        prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                        prodLine.Orders.Add(prodOrder);
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionLineRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionLineRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload;
                var prodLine = req.DB.ProductionLines.Where(pl => pl.SiteId == payload.siteId && pl.LineId == payload.productionLineId).FirstOrDefault();
                if (prodLine != null)
                {
                    if (prodLine.PRODLinesModelModel.ProductionLines.Count == 1)
                    {
                        req.DB.PRODLinesModels.Remove(prodLine.PRODLinesModelModel);
                    }
                    else
                    {
                        req.DB.ProductionLines.Remove(prodLine);
                    }
                    
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionOrderUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionOrderUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var order = req.JsonPayload.payload;
                var prodOrder = req.DB.ProductionLineOrders.Where(plo => plo.ProductionId == order.id).FirstOrDefault();
                if (prodOrder != null)
                {
                    prodOrder.ProductionId = order.id;

                    foreach (var input in order.inputs)
                    {
                        ProductionLineInput prodInput = new ProductionLineInput();

                        prodInput.MaterialName = input.material.name;
                        prodInput.MaterialTicker = input.material.ticker;
                        prodInput.MaterialId = input.material.id;
                        prodInput.MaterialAmount = input.amount;

                        prodOrder.Inputs.Add(prodInput);
                    }

                    foreach (var output in order.outputs)
                    {
                        ProductionLineOutput prodOutput = new ProductionLineOutput();

                        prodOutput.MaterialName = output.material.name;
                        prodOutput.MaterialTicker = output.material.ticker;
                        prodOutput.MaterialId = output.material.id;
                        prodOutput.MaterialAmount = output.amount;

                        prodOrder.Outputs.Add(prodOutput);
                    }

                    prodOrder.CreatedEpochMs = order.created?.timestamp;
                    prodOrder.StartedEpochMs = order.started?.timestamp;
                    prodOrder.CompletionEpochMs = order.completion?.timestamp;
                    prodOrder.DurationMs = order.duration?.millis;
                    prodOrder.LastUpdatedEpochMs = order.lastUpdated?.timestamp;
                    prodOrder.CompletedPercentage = order?.completed;

                    prodOrder.IsHalted = order.halted;
                    prodOrder.Recurring = order.recurring;

                    prodOrder.ProductionFee = order.productionFee.amount;
                    prodOrder.ProductionFeeCurrency = order.productionFee.currency;

                    prodOrder.ProductionFeeCollectorId = order.productionFeeCollector?.id;
                    prodOrder.ProductionFeeCollectorName = order.productionFeeCollector?.name;
                    prodOrder.ProductionFeeCollectorCode = order.productionFeeCollector?.code;

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostProductionOrderRemoved()
        {
            using (var req = new FIORequest<JSONRepresentations.ProductionOrderRemoved.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var payload = req.JsonPayload.payload.message.payload;
                var prodOrder = req.DB.ProductionLineOrders.Where(plo => plo.ProductionId == payload.orderId).FirstOrDefault();
                if (prodOrder != null)
                {
                    prodOrder.ProductionLine.Orders.Remove(prodOrder);
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetProduction(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var models = DB.PRODLinesModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).ToList();
                    if (models.Count > 0)
                    {
                        return JsonConvert.SerializeObject(models);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetProduction(string UserName, string PlanetOrSiteId)
        {
            UserName = UserName.ToUpper();
            PlanetOrSiteId = PlanetOrSiteId.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var prodLines = DB.PRODLinesModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).ToList();
                    if (prodLines.Count > 0)
                    {
                        List<ProductionLine> productionLines = new List<ProductionLine>();
                        foreach(var prodLine in prodLines)
                        {
                            var matchingProductionLines = prodLine.ProductionLines.Where(p => p.PlanetId.ToUpper() == PlanetOrSiteId || p.PlanetName.ToUpper() == PlanetOrSiteId || p.PlanetNaturalId.ToUpper() == PlanetOrSiteId || p.SiteId.ToUpper() == PlanetOrSiteId).ToList();
                            productionLines.AddRange(matchingProductionLines);
                        }

                        if(productionLines.Count > 0 )
                        {
                            return JsonConvert.SerializeObject(productionLines);
                        }
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetProductionPlanets(string UserName)
        {
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    List<string> planets = new List<string>();
                    var prodLines = DB.PRODLinesModels.Where(s => s.UserNameSubmitted == UserName).ToList();
                    foreach (var prodLine in prodLines)
                    {
                        if (prodLine.ProductionLines.Count > 0)
                        {
                            planets.Add(prodLine.ProductionLines[0].PlanetNaturalId);
                        }
                    }

                    if ( planets.Count > 0)
                    {
                        return JsonConvert.SerializeObject(planets);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
