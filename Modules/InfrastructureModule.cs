﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class InfrastructureModule : NancyModule
    {
        public InfrastructureModule() : base("/infrastructure")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostInfrastructure();
            });

            Post("/project", _ =>
            {
                this.EnforceWriteAuth();
                return PostInfrastructureProject();
            });

            Get("/{planet_or_infrastructure_id}", parameters =>
            {
                return GetInfrastructure(parameters.planet_or_infrastructure_id);
            });
        }

        private Response PostInfrastructure()
        {
            using (var req = new FIORequest<JSONRepresentations.Infrastructure.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;
                var model = req.DB.InfrastructureModels.Where(i => i.PopulationId == data.id).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new InfrastructureModel();
                }
                else
                {
                    model.InfrastructureInfos.Clear();
                    model.InfrastructureReports.Clear();
                }

                model.PopulationId = data.id;

                foreach (var infrastructure in data.infrastructure)
                {
                    InfrastructureInfo infInfo = new InfrastructureInfo();

                    infInfo.Type = infrastructure.type;
                    infInfo.Ticker = infrastructure.ticker;
                    infInfo.Name = infrastructure.projectName;
                    infInfo.ProjectId = infrastructure.projectId;
                    infInfo.Level = infrastructure.level;
                    infInfo.ActiveLevel = infrastructure.activeLevel;
                    infInfo.CurrentLevel = infrastructure.currentLevel;
                    infInfo.UpkeepStatus = infrastructure.upkeepStatus;
                    infInfo.UpgradeStatus = infrastructure.upgradeStatus;

                    model.InfrastructureInfos.Add(infInfo);
                }

                foreach( var report in data.reports)
                {
                    InfrastructureReport infReport = new InfrastructureReport();

                    infReport.ExplorersGraceEnabled = report.explorersGraceEnabled;
                    infReport.SimulationPeriod = report.simulationPeriod;
                    infReport.TimestampMs = report.time.timestamp;

                    infReport.NextPopulationPioneer = report.nextPopulation.PIONEER;
                    infReport.NextPopulationSettler = report.nextPopulation.SETTLER;
                    infReport.NextPopulationTechnician = report.nextPopulation.TECHNICIAN;
                    infReport.NextPopulationEngineer = report.nextPopulation.ENGINEER;
                    infReport.NextPopulationScientist = report.nextPopulation.SCIENTIST;

                    infReport.PopulationDifferencePioneer = report.populationDifference.PIONEER;
                    infReport.PopulationDifferenceSettler = report.populationDifference.SETTLER;
                    infReport.PopulationDifferenceTechnician = report.populationDifference.TECHNICIAN;
                    infReport.PopulationDifferenceEngineer = report.populationDifference.ENGINEER;
                    infReport.PopulationDifferenceScientist = report.populationDifference.SCIENTIST;

                    infReport.AverageHappinessPioneer = report.averageHappiness.PIONEER;
                    infReport.AverageHappinessSettler = report.averageHappiness.SETTLER;
                    infReport.AverageHappinessTechnician = report.averageHappiness.TECHNICIAN;
                    infReport.AverageHappinessEngineer = report.averageHappiness.ENGINEER;
                    infReport.AverageHappinessScientist = report.averageHappiness.SCIENTIST;

                    infReport.UnemploymentRatePioneer = report.unemploymentRate.PIONEER;
                    infReport.UnemploymentRateSettler = report.unemploymentRate.SETTLER;
                    infReport.UnemploymentRateTechnician = report.unemploymentRate.TECHNICIAN;
                    infReport.UnemploymentRateEngineer = report.unemploymentRate.ENGINEER;
                    infReport.UnemploymentRateScientist = report.unemploymentRate.SCIENTIST;

                    infReport.OpenJobsPioneer = report.openJobs.PIONEER;
                    infReport.OpenJobsSettler = report.openJobs.SETTLER;
                    infReport.OpenJobsTechnician = report.openJobs.TECHNICIAN;
                    infReport.OpenJobsEngineer = report.openJobs.ENGINEER;
                    infReport.OpenJobsScientist = report.openJobs.SCIENTIST;

                    infReport.NeedFulfillmentLifeSupport = report.needFulfillment.LIFE_SUPPORT;
                    infReport.NeedFulfillmentSafety = report.needFulfillment.SAFETY;
                    infReport.NeedFulfillmentHealth = report.needFulfillment.HEALTH;
                    infReport.NeedFulfillmentComfort = report.needFulfillment.COMFORT;
                    infReport.NeedFulfillmentCulture = report.needFulfillment.CULTURE;
                    infReport.NeedFulfillmentEducation = report.needFulfillment.EDUCATION;

                    model.InfrastructureReports.Add(infReport);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.InfrastructureModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostInfrastructureProject()
        {
            using (var req = new FIORequest<JSONRepresentations.InfrastructureProject.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;
                var model = req.DB.InfrastructureInfos.Where(i => i.ProjectId == data.id).FirstOrDefault();
                if (model != null)
                {
                    model.UpgradeCosts.Clear();
                    model.Upkeeps.Clear();
                    model.Contributions.Clear();

                    foreach (var upgradeCost in data.upgradeCosts)
                    {
                        var uc = new InfrastructureProjectUpgradeCosts();

                        uc.MaterialId = upgradeCost.material.id;
                        uc.MaterialName = upgradeCost.material.name;
                        uc.MaterialTicker = upgradeCost.material.ticker;

                        uc.Amount = upgradeCost.amount;
                        uc.CurrentAmount = upgradeCost.currentAmount;

                        model.UpgradeCosts.Add(uc);
                    }

                    foreach (var upkeep in data.upkeeps)
                    {
                        var uk = new InfrastructureProjectUpkeeps();

                        uk.MaterialId = upkeep.material.id;
                        uk.MaterialName = upkeep.material.name;
                        uk.MaterialTicker = upkeep.material.ticker;

                        uk.Stored = upkeep.stored;
                        uk.StoreCapacity = upkeep.storeCapacity;
                        uk.Duration = upkeep.duration;
                        uk.NextTickTimestampEpochMs = upkeep.nextTick.timestamp;

                        uk.Amount = upkeep.amount;
                        uk.CurrentAmount = upkeep.currentAmount;

                        model.Upkeeps.Add(uk);
                    }

                    foreach (var contribution in data.contributions)
                    {
                        foreach(var matContribution in contribution.materials)
                        {
                            var c = new InfrastructureProjectContributions();

                            c.MaterialId = matContribution.material.id;
                            c.MaterialName = matContribution.material.name;
                            c.MaterialTicker = matContribution.material.ticker;

                            c.Amount = matContribution.amount;

                            c.TimestampEpochMs = contribution.time.timestamp;
                            c.CompanyId = contribution.contributor.id;
                            c.CompanyName = contribution.contributor.name;
                            c.CompanyCode = contribution.contributor.code;

                            model.Contributions.Add(c);
                        }
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetInfrastructure(string PlanetOrInfrastructureId)
        {
            PlanetOrInfrastructureId = PlanetOrInfrastructureId.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var model = DB.InfrastructureModels.Where(i => i.PopulationId.ToUpper() == PlanetOrInfrastructureId).FirstOrDefault();
                if (model != null )
                {
                    return JsonConvert.SerializeObject(model);
                }

                var planet = DB.PlanetDataModels.Where(p => p.PlanetId.ToUpper() == PlanetOrInfrastructureId || p.PlanetNaturalId.ToUpper() == PlanetOrInfrastructureId || p.PlanetName.ToUpper() == PlanetOrInfrastructureId).FirstOrDefault();
                if ( planet != null )
                {
                    model = DB.InfrastructureModels.Where(i => i.PopulationId == planet.PopulationId).FirstOrDefault();
                    if ( model != null)
                    {
                        return JsonConvert.SerializeObject(model);
                    }
                }

                return HttpStatusCode.NoContent;
            }
        }
    }
}
