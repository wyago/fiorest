﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class ExchangeModule : NancyModule
    {
        public ExchangeModule() : base("/exchange")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostExchange();
            });

            Post("/cxpc", _ =>
            {
                this.EnforceWriteAuth();
                return PostCXPC();
            });

            Post("/station", _ =>
            {
                this.EnforceWriteAuth();
                return PostStation();
            });

            Get("/station", _ =>
            {
                this.Cacheable();
                return GetStation();
            });

            Get("/{exchange_ticker}", parameters =>
            {
                return GetExchange(parameters.exchange_ticker);
            });

            Get("/cxpc/{exchange_ticker}", parameters =>
            {
                this.Cacheable(3600);
                return GetCXPC(parameters.exchange_ticker);
            });

            Get("/cxpc/{exchange_ticker}/{timestamp:long}", parameters =>
            {
                this.Cacheable(3600);
                return GetCXPC(parameters.exchange_ticker, parameters.timestamp);
            });

            Get("/full", _ =>
            {
                this.Cacheable(30 * 60); // 30 mins
                return GetExchangeFull();
            });

            Get("/all", _ =>
            {
                this.Cacheable(15 * 60); // 15 mins
                return GetAll();
            });

            Get("/orders/{companycode}", parameters =>
            {
                return GetOrders(parameters.companycode);
            });

            Get("/orders/{companycode}/{exchangecode}", parameters =>
            {
                return GetOrders(parameters.companycode, parameters.exchangecode);
            });
        }

        private Response PostExchange()
        {
            using (var req = new FIORequest<JSONRepresentations.ComexBrokerData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var model = new CXDataModel();
                model.CXDataModelId = data.id;

                model.MaterialName = data.material.name;
                model.MaterialTicker = data.material.ticker;
                model.MaterialId = data.material.id;

                model.ExchangeName = data.exchange.name;
                model.ExchangeCode = data.exchange.code;

                model.Currency = data.currency.code;

                model.Previous = data.previous?.amount;
                model.Price = data.price?.amount;
                model.PriceTimeEpochMs = data.priceTime?.timestamp;

                model.High = data.high?.amount;
                model.AllTimeHigh = data.allTimeHigh?.amount;

                model.Low = data.low?.amount;
                model.AllTimeLow = data.allTimeLow?.amount;

                model.Ask = data.ask?.price?.amount;
                model.AskCount = data.ask?.amount;

                model.Bid = data.bid?.price?.amount;
                model.BidCount = data.bid?.amount;

                model.Supply = data.supply;
                model.Demand = data.demand;
                model.Traded = data.traded;

                model.VolumeAmount = data.volume?.amount;

                model.PriceAverage = data.priceAverage?.amount;

                model.NarrowPriceBandLow = data.narrowPriceBand?.low;
                model.NarrowPriceBandHigh = data.narrowPriceBand?.high;

                model.WidePriceBandLow = data.widePriceBand?.low;
                model.WidePriceBandHigh = data.widePriceBand?.high;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                var mmsell = data.sellingOrders.Where(ord => ord.amount == null).FirstOrDefault();
                if(mmsell != null)
                {
                    model.MMSell = mmsell.limit.amount;
                }
                var mmbuy = data.buyingOrders.Where(ord => ord.amount == null).FirstOrDefault();
                if (mmbuy != null)
                {
                    model.MMBuy = mmbuy.limit.amount;
                }

                req.DB.CXDataModels
                    .Upsert(model)
                    .On(m => new { m.CXDataModelId })
                    .Run();
                List<CXSellOrder> sellOrders = new();
                List<string> sellOrderIds = data.sellingOrders.Select(so => so.id).ToList();
                if(sellOrderIds.Count() > 0)
                {
                    req.DB.CXSellOrders.RemoveRange(req.DB.CXSellOrders.Where(so => so.CXDataModelId == model.CXDataModelId.ToString() && !sellOrderIds.Contains(so.OrderId)).ToList());
                    foreach (var sellingOrder in data.sellingOrders)
                    {
                        CXSellOrder sellOrder = new CXSellOrder();
                        sellOrder.OrderId = sellingOrder.id;
                        sellOrder.CompanyId = sellingOrder.trader.id;
                        sellOrder.CompanyName = sellingOrder.trader.name;
                        sellOrder.CompanyCode = sellingOrder.trader.code;
                        sellOrder.ItemCount = sellingOrder.amount;
                        sellOrder.ItemCost = sellingOrder.limit.amount;
                        sellOrder.CXDataModelId = model.CXDataModelId;

                        sellOrders.Add(sellOrder);
                    }
                    req.DB.CXSellOrders.UpsertRange(sellOrders)
                        .On(so => new { so.OrderId })
                        .Run();
                }
                else
                {
                    req.DB.CXSellOrders.RemoveRange(req.DB.CXSellOrders.Where(so => so.CXDataModelId == model.CXDataModelId.ToString()));
                }

                List<CXBuyOrder> buyOrders = new();
                List<string> buyOrderIds = data.buyingOrders.Select(so => so.id).ToList();
                if(buyOrderIds.Count() > 0)
                {
                    req.DB.CXBuyOrders.RemoveRange(req.DB.CXBuyOrders.Where(so => so.CXDataModelId == model.CXDataModelId.ToString() && !buyOrderIds.Contains(so.OrderId)).ToList());
                    foreach (var buyingOrder in data.buyingOrders)
                    {
                        CXBuyOrder buyOrder = new CXBuyOrder();
                        buyOrder.OrderId = buyingOrder.id;
                        buyOrder.CompanyId = buyingOrder.trader.id;
                        buyOrder.CompanyName = buyingOrder.trader.name;
                        buyOrder.CompanyCode = buyingOrder.trader.code;
                        buyOrder.ItemCount = buyingOrder.amount;
                        buyOrder.ItemCost = buyingOrder.limit.amount;
                        buyOrder.CXDataModelId = model.CXDataModelId;

                        buyOrders.Add(buyOrder);
                    }
                    req.DB.CXBuyOrders.UpsertRange(buyOrders)
                        .On(so => new { so.OrderId })
                        .Run();
                }
                else
                {
                    req.DB.CXBuyOrders.RemoveRange(req.DB.CXBuyOrders.Where(so => so.CXDataModelId == model.CXDataModelId.ToString()).ToList());
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostCXPC()
        {
            using (var req = new FIORequest<JSONRepresentations.ComexBrokerPrices.Rootobject>(Request))
            {
                if ( req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var cxdata = req.DB.CXDataModels.Where(cxdm => cxdm.CXDataModelId == data.brokerId).FirstOrDefault();
                if (cxdata != null)
                {
                    var model = req.DB.CXPCData.Where(cxpcd => cxpcd.MaterialTicker == cxdata.MaterialTicker && cxpcd.ExchangeCode == cxdata.ExchangeCode).FirstOrDefault();
                    bool bAdd = (model == null);
                    if (bAdd)
                    {
                        model = new CXPCData();
                        model.MaterialTicker = cxdata.MaterialTicker;
                        model.ExchangeCode = cxdata.ExchangeCode;
                    }

                    long MinimumStart = bAdd || model.Entries.Count == 0 ? long.MaxValue : model.StartDataEpochMs;
                    long MaximumEnd = bAdd || model.Entries.Count == 0 ? 0 : model.EndDataEpochMs;

                    var filteredNewData = data.prices.Where(p => p.date < MinimumStart || p.date > MaximumEnd).ToList();
                    if ( filteredNewData.Count > 0 )
                    {
                        var filteredNewDataMin = filteredNewData.OrderBy(fnd => fnd.date).Select(fnd => fnd.date).First();
                        var filteredNewDataMax = filteredNewData.OrderByDescending(fnd => fnd.date).Select(fnd => fnd.date).First();

                        model.StartDataEpochMs = Math.Min(MinimumStart, filteredNewDataMin);
                        model.EndDataEpochMs = Math.Max(MaximumEnd, filteredNewDataMax);
  
                        foreach(var dataentry in filteredNewData)
                        {
                            var entry = new CXPCDataEntry();
                            entry.TimeEpochMs = dataentry.date;
                            entry.Open = dataentry.open;
                            entry.Close = dataentry.close;
                            entry.Volume = dataentry.volume;
                            entry.Traded = dataentry.traded;

                            model.Entries.Add(entry);
                        }
                    }

                    if (bAdd)
                    {
                        req.DB.CXPCData.Add(model);
                    }
                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response PostStation()
        {
            using (var req = new FIORequest<JSONRepresentations.Station.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload.body;

                var model = req.DB.Stations.Where(s => s.NaturalId == data.naturalId).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new Station();
                }

                model.NaturalId = data.naturalId;
                model.Name = data.name;
                model.SystemId = data.address.lines[0].entity.id;
                model.SystemNaturalId = data.address.lines[0].entity.naturalId;
                model.SystemName = data.address.lines[0].entity.name;

                model.CommisionTimeEpochMs = data.commissioningTime.timestamp;

                model.ComexId = data.comex.id;
                model.ComexName = data.comex.name;
                model.ComexCode = data.comex.code;

                model.WarehouseId = data.warehouseId;
                model.CountryId = data.country.id;
                model.CountryCode = data.country.code;
                model.CountryName = data.country.name;

                model.CurrencyNumericCode = data.currency.numericCode;
                model.CurrencyCode = data.currency.code;
                model.CurrencyName = data.currency.name;
                model.CurrencyDecimals = data.currency.decimals;

                model.GovernorId = data.governor?.id;
                model.GovernorUserName = data.governor?.username;

                model.GovernorCorporationId = data.governingEntity?.id;
                model.GovernorCorporationName = data.governingEntity?.name;
                model.GovernorCorporationCode = data.governingEntity?.code;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.Stations.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetStation()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.Stations.ToList());
            }
        }

        private Response GetExchange(string ExchangeTicker)
        {
            string[] parts = ExchangeTicker.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if ( parts.Length == 2)
            {
                string materialCode = parts[0].ToUpper();
                string exchangeCode = parts[1].ToUpper();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.CXDataModels.Where(c => c.MaterialTicker == materialCode && c.ExchangeCode == exchangeCode).FirstOrDefault();
                    if ( res != null )
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }
            else
            {
                Response badRequest = "Invalid ticker format.";
                badRequest.ContentType = "text/plain";
                badRequest.StatusCode = HttpStatusCode.BadRequest;
                return badRequest;
            }
        }

        private Response GetCXPC(string ExchangeTicker, long timestamp = 0)
        {
            string[] parts = ExchangeTicker.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 2)
            {
                string materialCode = parts[0].ToUpper();
                string exchangeCode = parts[1].ToUpper();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.CXPCData.Where(cxpc => cxpc.MaterialTicker == materialCode && cxpc.ExchangeCode == exchangeCode).Include(cxpc => cxpc.Entries).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res.Entries.Where(e => e.TimeEpochMs >= timestamp).OrderBy(e => e.TimeEpochMs).ToList());
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }
            else
            {
                Response badRequest = "Invalid ticker format.";
                badRequest.ContentType = "text/plain";
                badRequest.StatusCode = HttpStatusCode.BadRequest;
                return badRequest;
            }
        }

        private Response GetExchangeFull()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.CXDataModels.Include(cx => cx.BuyingOrders).Include(cx => cx.SellingOrders).ToList());
            }
        }

        public class MinifiedCXDataModel
        {
            public string MaterialTicker { get; set; }
            public string ExchangeCode { get; set; }
            public double? MMBuy { get; set; }
            public double? MMSell { get; set; }
            public double? PriceAverage { get; set; }
            public int? AskCount { get; set; }
            public double? Ask { get; set; }
            public int? Supply { get; set; }
            public int? BidCount { get; set; }
            public double? Bid { get; set; }
            public int? Demand { get; set; }
        }

        private Response GetAll()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.CXDataModels.OrderBy(c => c.MaterialTicker).ThenBy(c => c.ExchangeCode).Select(c => new MinifiedCXDataModel()
                {
                    MaterialTicker = c.MaterialTicker,
                    ExchangeCode = c.ExchangeCode,
                    MMBuy = c.MMBuy,
                    MMSell = c.MMSell,
                    PriceAverage = c.PriceAverage.RoundToDecimalPlaces(2),
                    AskCount = c.AskCount,
                    Ask = c.Ask.RoundToDecimalPlaces(2),
                    Supply = c.Supply,
                    BidCount = c.BidCount,
                    Bid = c.Bid.RoundToDecimalPlaces(2),
                    Demand = c.Demand
                }).ToList());
            }
        }

        class Order
        {
            public int Count { get; set; }
            public double Cost { get; set; }
        }

        class GetOrdersResponse
        {
            public string Ticker { get; set; }

            public List<Order> Buys { get; set; } = new List<Order>();
            public List<Order> Sells { get; set; } = new List<Order>();
        }
        

        private Response GetOrders(string CompanyCode, string ExchangeCode = null)
        {
            CompanyCode = CompanyCode.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var cxdata = DB.CXDataModels.Where(cxdm => cxdm.BuyingOrders.Any(bo => bo.CompanyCode.ToUpper() == CompanyCode) || cxdm.SellingOrders.Any(so => so.CompanyCode.ToUpper() == CompanyCode));
                if (!string.IsNullOrWhiteSpace(ExchangeCode))
                {
                    cxdata = cxdata.Where(cxd => cxd.ExchangeCode == ExchangeCode.ToUpper());
                }

                var res = cxdata.Select(cxdm => new
                {
                    CXDM = cxdm,
                    Buys = cxdm.BuyingOrders.Where(bo => bo.CompanyCode.ToUpper() == CompanyCode),
                    Sells = cxdm.SellingOrders.Where(so => so.CompanyCode.ToUpper() == CompanyCode)
                });

                List<GetOrdersResponse> resp = res.Select(r => new GetOrdersResponse
                {
                    Ticker = $"{r.CXDM.MaterialTicker}.{r.CXDM.ExchangeCode}",
                    Buys = r.Buys.Select(b => new Order { Count = (b.ItemCount != null ? (int)b.ItemCount : -1), Cost = b.ItemCost }).ToList(),
                    Sells = r.Sells.Select(s => new Order { Count = (s.ItemCount != null ? (int)s.ItemCount : -1), Cost = s.ItemCost}).ToList()
                }).ToList();

                return JsonConvert.SerializeObject(resp);
            }
        }
    }
}
