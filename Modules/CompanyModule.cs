﻿using System;
using System.Linq;

using FlexLabs.EntityFrameworkCore.Upsert;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class CompanyModule : NancyModule
    {
        public CompanyModule() : base("/company")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostCompany();
            });

            Post("/data", _ =>
            {
                this.EnforceWriteAuth();
                return PostCompanyData();
            });

            Get("/code/{company_code}", parameters =>
            {
                this.EnforceReadAuth();
                return GetCompanyByCode(parameters.company_code);
            });

            Get("/name/{company_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetCompanyByName(parameters.company_name);
            });

            Get("/username/{username}", parameters =>
            {
                return GetCompanyCodeByUserName(parameters.username);
            });
        }

        private Response PostCompany()
        {
            using (var req = new FIORequest<JSONRepresentations.CompanyData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = req.DB.CompanyDataModels.Where(c => c.CompanyName == data.name).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new CompanyDataModel();
                }
                else
                {
                    model.Balances.Clear();
                }

                model.CompanyId = data.id;
                model.CompanyName = data.name;
                model.CountryId = data.countryId;

                model.CurrencyCode = data.ownCurrency.code;

                model.StartingProfile = data.startingProfile;
                model.StartingLocation = data.startingLocation.lines[1].entity.naturalId;

                foreach (var account in data.currencyAccounts)
                {
                    CompanyDataCurrencyBalance balance = new CompanyDataCurrencyBalance();

                    balance.Currency = account.currencyBalance.currency;
                    balance.Balance = account.currencyBalance.amount;

                    model.Balances.Add(balance);
                }

                model.OverallRating = data.ratingReport.overallRating;
                foreach (var subRating in data.ratingReport.subRatings)
                {
                    switch (subRating.score)
                    {
                        case "ACTIVITY":
                            model.ActivityRating = subRating.rating;
                            break;
                        case "RELIABILITY":
                            model.ReliabilityRating = subRating.rating;
                            break;
                        case "STABILITY":
                            model.StabilityRating = subRating.rating;
                            break;
                    }
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.CompanyDataModels.Add(model);
                }
                else
                {
                    req.DB.Update(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostCompanyData()
        {
            using (var req = new FIORequest<JSONRepresentations.CompanyDataData.Rootobject>(Request))
            {
                var data = req.JsonPayload.payload.message.payload;

                string UserName = Request.GetUserName();
                DateTime now = DateTime.Now.ToUniversalTime();

                var model = new CompanyDataModel();

                model.UserName = data.body.username;
                model.HighestTier = data.body.highestTier;
                model.Pioneer = data.body.pioneer;
                model.Team = data.body.team;

                model.CreatedEpochMs = data.body.created.timestamp;

                model.CompanyId = data.body.company.id;
                model.CompanyName = data.body.company.name;
                model.CompanyCode = data.body.company.code;

                model.UserNameSubmitted = UserName;
                model.Timestamp = now;

                var res = req.DB.CompanyDataModels
                    .Upsert(model)
                    .On(m => new { m.CompanyId })
                    .WhenMatched(m => new CompanyDataModel
                    {
                        UserName = model.UserName,
                        HighestTier = model.HighestTier,
                        Pioneer = model.Pioneer,
                        Team = model.Team,
                        CreatedEpochMs = model.CreatedEpochMs,
                        CompanyName = model.CompanyName,
                        CompanyCode = model.CompanyCode,
                        UserNameSubmitted = model.UserNameSubmitted,
                        Timestamp = model.Timestamp
                    })
                    .Run();
                return HttpStatusCode.OK;
            }
        }

        private Response GetCompanyByUser(string UserName)
        {
            UserName = UserName.ToUpper();

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var res = DB.CompanyDataModels.Where(c => c.UserName.ToUpper() == UserName).FirstOrDefault();
                    if (res != null)
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetCompanyByCode(string CompanyCode)
        {
            CompanyCode = CompanyCode.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.CompanyDataModels.Where(c => c.CompanyCode.ToUpper() == CompanyCode).FirstOrDefault();
                if (res != null)
                {
                    if (Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Storage))
                    {
                        return JsonConvert.SerializeObject(res);
                    }

                    return HttpStatusCode.Unauthorized;
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetCompanyByName(string CompanyName)
        {
            CompanyName = CompanyName.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.CompanyDataModels.Where(c => c.CompanyName.ToUpper() == CompanyName).FirstOrDefault();
                if (res != null)
                {
                    if (Auth.CanSeeData(Request.GetUserName(), res.UserName, Auth.PrivacyType.Storage))
                    {
                        return JsonConvert.SerializeObject(res);
                    }

                    return HttpStatusCode.Unauthorized;
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetCompanyCodeByUserName(string UserName)
		{
            UserName = UserName.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.CompanyDataModels.Where(c => c.UserName.ToUpper() == UserName).Select(c => c.CompanyCode).FirstOrDefault();
                if (res != null)
				{
                    Response resp = res;
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.OK;
                    return resp;
                }
                else
				{
                    return HttpStatusCode.NoContent;
				}
			}
		}
    }
}
