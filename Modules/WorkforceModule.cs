﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class WorkforceModule : NancyModule
    {
        public WorkforceModule() : base("/workforce")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostWorkforce();
            });

            Post("/updated", _ =>
            {
                this.EnforceWriteAuth();
                return PostWorkforceUpdated();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforce(parameters.username);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforcePlanets(parameters.username);
            });

            Get("/{username}/{planet}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforce(parameters.username, parameters.planet);
            });
        }

        private Response PostWorkforce()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforces.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var model = req.DB.WorkforceModels.Where(w => w.UserNameSubmitted.ToUpper() == req.UserName && w.SiteId == data.siteId).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new WorkforceModel();
                }
                else
                {
                    model.Workforces.Clear();
                }

                model.PlanetId = data.address.lines[1].entity.id;
                model.PlanetNaturalId = data.address.lines[1].entity.naturalId;
                model.PlanetName = data.address.lines[1].entity.name;

                model.SiteId = data.siteId;

                foreach (var workforce in data.workforces)
                {
                    WorkforceDescription workforceDesc = new WorkforceDescription();

                    workforceDesc.WorkforceTypeName = workforce.level;
                    workforceDesc.Population = workforce.population;
                    workforceDesc.Reserve = workforce.reserve;
                    workforceDesc.Capacity = workforce.capacity;
                    workforceDesc.Required = workforce.required;

                    workforceDesc.Satisfaction = workforce.satisfaction;

                    foreach (var need in workforce.needs)
                    {
                        WorkforceNeed workforceNeed = new WorkforceNeed();

                        workforceNeed.Category = need.category;
                        workforceNeed.Essential = need.essential;
                        workforceNeed.MaterialId = need.material.id;
                        workforceNeed.MaterialName = need.material.name;
                        workforceNeed.MaterialTicker = need.material.ticker;

                        workforceNeed.Satisfaction = need.satisfaction;
                        workforceNeed.UnitsPerInterval = need.unitsPerInterval;
                        workforceNeed.UnitsPerOneHundred = need.unitsPer100;

                        workforceDesc.WorkforceNeeds.Add(workforceNeed);
                    }

                    model.Workforces.Add(workforceDesc);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.WorkforceModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostWorkforceUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforcesUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload;
                var model = req.DB.WorkforceModels.Where(wm => wm.SiteId == data.siteId).FirstOrDefault();
                if (model != null)
                {
                    model.Workforces.Clear();

                    model.PlanetId = data.address.lines[1].entity.id;
                    model.PlanetNaturalId = data.address.lines[1].entity.naturalId;
                    model.PlanetName = data.address.lines[1].entity.name;

                    model.SiteId = data.siteId;

                    model.LastWorkforceUpdateTime = req.Now;

                    foreach (var workforce in data.workforces)
                    {
                        WorkforceDescription workforceDesc = new WorkforceDescription();

                        workforceDesc.WorkforceTypeName = workforce.level;
                        workforceDesc.Population = workforce.population;
                        workforceDesc.Reserve = workforce.reserve;
                        workforceDesc.Capacity = workforce.capacity;
                        workforceDesc.Required = workforce.required;

                        workforceDesc.Satisfaction = workforce.satisfaction;

                        foreach (var need in workforce.needs)
                        {
                            WorkforceNeed workforceNeed = new WorkforceNeed();

                            workforceNeed.Category = need.category;
                            workforceNeed.Essential = need.essential;
                            workforceNeed.MaterialId = need.material.id;
                            workforceNeed.MaterialName = need.material.name;
                            workforceNeed.MaterialTicker = need.material.ticker;

                            workforceNeed.Satisfaction = need.satisfaction;
                            workforceNeed.UnitsPerInterval = need.unitsPerInterval;
                            workforceNeed.UnitsPerOneHundred = need.unitsPer100;

                            workforceDesc.WorkforceNeeds.Add(workforceNeed);
                        }

                        model.Workforces.Add(workforceDesc);
                    }

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetWorkforce(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var models = DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).ToList();
                    if (models.Count > 0)
                    {
                        return JsonConvert.SerializeObject(models);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWorkforce(string UserName, string Planet)
        {
            UserName = UserName.ToUpper();
            Planet = Planet.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.PlanetId.ToUpper() == Planet || s.PlanetName.ToUpper() == Planet || s.PlanetNaturalId.ToUpper() == Planet)).FirstOrDefault();
                    if (model != null)
                    {
                        return JsonConvert.SerializeObject(model);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWorkforcePlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var planets = DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).Select(s => s.PlanetNaturalId).ToList();
                    if ( planets.Count > 0 )
                    {
                        return JsonConvert.SerializeObject(planets);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
