﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class StorageModule : NancyModule
    {
        public StorageModule() : base("/storage")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostStorage();
            });

            Post("/change", _ =>
            {
                this.EnforceWriteAuth();
                return PostStorageChange();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStorage(parameters.username);
            });

            Get("/{username}/{storage_description}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStorage(parameters.username, parameters.storage_description);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetStoragePlanets(parameters.username);
            });
        }

        private Response PostStorage()
        {
            using (var req = new FIORequest<JSONRepresentations.StorageStorages.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                foreach (var store in data.stores)
                {
                    StorageModel model = req.DB.StorageModels.Where(sm => sm.UserNameSubmitted.ToUpper() == req.UserName && sm.AddressableId == store.addressableId && sm.StorageId == store.id).FirstOrDefault();
                    bool bIsAdd = (model == null);
                    if (bIsAdd)
                    {
                        model = new StorageModel();
                    }
                    else
                    {
                        model.StorageItems.Clear();
                    }

                    model.StorageId = store.id;
                    model.AddressableId = store.addressableId;
                    model.Name = store.name;
                    model.WeightLoad = store.weightLoad;
                    model.WeightCapacity = store.weightCapacity;
                    model.VolumeLoad = store.volumeLoad;
                    model.VolumeCapacity = store.volumeCapacity;

                    foreach (var item in store.items)
                    {
                        StorageItem storageItem = new StorageItem();
                        if (item.type == "BLOCKED" || item.type == "SHIPMENT")
                        {
                            storageItem.Type = item.type;
                            storageItem.MaterialId = item.id;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        else
                        {
                            storageItem.MaterialId = item.quantity.material.id;
                            storageItem.MaterialName = item.quantity.material.name;
                            storageItem.MaterialTicker = item.quantity.material.ticker;
                            storageItem.MaterialCategory = item.quantity.material.category;
                            storageItem.MaterialWeight = item.quantity.material.weight;
                            storageItem.MaterialVolume = item.quantity.material.volume;
                            storageItem.MaterialAmount = item.quantity.amount;
                            if (item.quantity.value != null)
                            {
                                storageItem.MaterialValue = item.quantity.value.amount;
                                storageItem.MaterialValueCurrency = item.quantity.value.currency;
                            }
                            
                            storageItem.Type = item.type;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        model.StorageItems.Add(storageItem);
                    }

                    model.FixedStore = store._fixed;
                    model.Type = store.type;

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    if (bIsAdd)
                    {
                        req.DB.StorageModels.Add(model);
                    }
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;

            }
        }

        private Response PostStorageChange()
        {
            using (var req = new FIORequest<JSONRepresentations.StorageChange.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;

                foreach (var store in rootObj.payload.stores)
                {
                    StorageModel model = req.DB.StorageModels.Where(sm => sm.UserNameSubmitted.ToUpper() == req.UserName && sm.AddressableId == store.addressableId && sm.StorageId == store.id).FirstOrDefault();
                    bool bIsAdd = (model == null);
                    if (bIsAdd)
                    {
                        model = new StorageModel();
                    }
                    else
                    {
                        model.StorageItems.Clear();
                    }

                    model.StorageId = store.id;
                    model.AddressableId = store.addressableId;
                    model.Name = store.name;
                    model.WeightLoad = store.weightLoad;
                    model.WeightCapacity = store.weightCapacity;
                    model.VolumeLoad = store.volumeLoad;
                    model.VolumeCapacity = store.volumeCapacity;

                    foreach (var item in store.items)
                    {
                        StorageItem storageItem = new StorageItem();
                        if (item.type == "BLOCKED" || item.type == "SHIPMENT")
                        {
                            storageItem.Type = item.type;
                            storageItem.MaterialId = item.id;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        else
                        {
                            storageItem.MaterialId = item.quantity.material.id;
                            storageItem.MaterialName = item.quantity.material.name;
                            storageItem.MaterialTicker = item.quantity.material.ticker;
                            storageItem.MaterialCategory = item.quantity.material.category;
                            storageItem.MaterialWeight = item.quantity.material.weight;
                            storageItem.MaterialVolume = item.quantity.material.volume;
                            storageItem.MaterialAmount = item.quantity.amount;
                            if (item.quantity.value != null)
                            {
                                storageItem.MaterialValue = item.quantity.value.amount;
                                storageItem.MaterialValueCurrency = item.quantity.value.currency;
                            }
                            storageItem.Type = item.type;
                            storageItem.TotalWeight = item.weight;
                            storageItem.TotalVolume = item.volume;
                        }
                        model.StorageItems.Add(storageItem);
                    }

                    model.FixedStore = store._fixed;
                    model.Type = store.type;

                    model.UserNameSubmitted = req.UserName;
                    model.Timestamp = req.Now;

                    if (bIsAdd)
                    {
                        req.DB.StorageModels.Add(model);
                    }
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetStorage(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var models = DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).ToList();
                    if ( models.Count > 0 )
                    {
                        return JsonConvert.SerializeObject(models);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }  
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetStorage(string UserName, string StorageDesc)
        {
            UserName = UserName.ToUpper();
            StorageDesc = StorageDesc.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var site = sites.Sites.Where(s => s.PlanetId.ToUpper() == StorageDesc || s.PlanetIdentifier.ToUpper() == StorageDesc || s.PlanetName.ToUpper() == StorageDesc).FirstOrDefault();
                        if (site != null)
                        {
                            var model = DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName && s.AddressableId == site.SiteId && s.StorageId != null).FirstOrDefault();
                            return JsonConvert.SerializeObject(model);
                        }
                    }

                    var storage = DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.StorageId.ToUpper() == StorageDesc)).FirstOrDefault();
                    if (storage != null)
                    {
                        return JsonConvert.SerializeObject(storage);
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetStoragePlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var sites = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (sites != null)
                    {
                        var planets = sites.Sites.Select(s => s.PlanetIdentifier).ToList();
                        return JsonConvert.SerializeObject(planets);
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
