﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class UserModule : NancyModule
    {
        public UserModule() : base("/user")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostUser();
            });

            Get("/allusers", _ =>
            {
                this.EnforceReadAuth();
                return GetAllUsers();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUser(parameters.username);
            });

            Post("/resetalldata", _ =>
            {
                this.EnforceWriteAuth();
                return PostResetAllData();
            });
        }

        private Response PostUser()
        {
            using (var req = new FIORequest<JSONRepresentations.UserData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = req.DB.UserDataModels.Where(c => c.UserName == data.username).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new UserDataModel();
                }

                model.UserName = data.username;
                model.Tier = data.highestTier;

                model.Team = data.team;
                model.Pioneer = data.pioneer;

                model.SystemNamingRights = data.systemNamingRights;
                model.PlanetNamingRights = data.planetNamingRights;

                model.IsPayingUser = data.isPayingUser;
                model.IsModeratorChat = data.isModeratorChat;

                model.CreatedEpochMs = data.created.timestamp;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.UserDataModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetAllUsers()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.AuthenticationModels.Select(u => u.UserName).ToList());
                //return JsonConvert.SerializeObject(DB.UserDataModels.Select(u => u.UserName).ToList());
            }
        }

        private Response GetUser(string UserName)
        {
            UserName = UserName.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.UserDataModels.Where(u => u.UserName.ToUpper() == UserName).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response PostResetAllData()
        {
            string UserName = Request.GetUserName().ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.CompanyDataModels.RemoveRange(DB.CompanyDataModels.Where(c => c.UserNameSubmitted.ToUpper() == UserName));
                DB.PRODLinesModels.RemoveRange(DB.PRODLinesModels.Where(p => p.UserNameSubmitted.ToUpper() == UserName));
                DB.SHIPSModels.RemoveRange(DB.SHIPSModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.SITESModels.RemoveRange(DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.WarehouseModels.RemoveRange(DB.WarehouseModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.StorageModels.RemoveRange(DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.WorkforceModels.RemoveRange(DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.UserDataModels.RemoveRange(DB.UserDataModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.ContractModels.RemoveRange(DB.ContractModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }
    }
}
