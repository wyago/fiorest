﻿using System;
using System.Collections.Generic;
using System.Text;

using FIORest.Authentication;

using Nancy;

namespace FIORest.Modules
{
    public class TestModule : NancyModule
    {
        public TestModule() : base("/test")
        {
            Get("/", _ => "Success");

            Get("/read", _ =>
            {
                this.EnforceReadAuth();
                return HttpStatusCode.OK;
            });

            Get("/write", _ =>
            {
                this.EnforceWriteAuth();
                return HttpStatusCode.OK;
            });
        }
    }
}
