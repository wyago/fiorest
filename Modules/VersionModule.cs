﻿using System.IO;

using Nancy;
using Nancy.Responses;

namespace FIORest.Modules
{
    public class VersionModule : NancyModule
    {
        public VersionModule() : base("/version")
        {
            Get("/latest", _ =>
            {
                return GetLatestVersion();
            });

            Get("/releasenotes", _ =>
            {
                return GetReleaseNotes();
            });

            Get("/download", _ =>
            {
                return GetDownload();
            });

            Get("/extension/download", _ =>
            {
                return GetDownloadExtension();
            });

            Get("/extension/script", _ =>
            {
                return GetExtensionScript();
            });
        }

        private string FIOUIReleasePath = Globals.Opts.UpdateDirectory;

        private Response GetLatestVersion()
        {
            const string LatestTxt = "Latest.txt";
            if ( File.Exists(Path.Combine(FIOUIReleasePath, LatestTxt)) )
            {
                return File.ReadAllText(Path.Combine(FIOUIReleasePath, LatestTxt));
            }

            return HttpStatusCode.NoContent;
        }

        private Response GetReleaseNotes()
        {
            const string ReleaseNotesRtf = "ReleaseNotes.rtf";
            if (File.Exists(Path.Combine(FIOUIReleasePath, ReleaseNotesRtf)))
            {
                var fs = new FileStream(Path.Combine(FIOUIReleasePath, ReleaseNotesRtf), FileMode.Open);
                var mimeType = MimeTypes.GetMimeType(ReleaseNotesRtf);
                var response = new StreamResponse(() => fs, mimeType);
                return response.AsAttachment(ReleaseNotesRtf);
            }
            return HttpStatusCode.NoContent;
        }

        private Response GetDownload()
        {
            const string SetupExe = "FIOUI-Setup.exe";
            if (File.Exists(Path.Combine(FIOUIReleasePath, SetupExe)))
            {
                var fs = new FileStream(Path.Combine(FIOUIReleasePath, SetupExe), FileMode.Open);
                var mimeType = MimeTypes.GetMimeType(SetupExe);
                var response = new StreamResponse(() => fs, mimeType);
                return response.AsAttachment(SetupExe);
            }
            return HttpStatusCode.NoContent;
        }

        private Response GetDownloadExtension()
        {
            const string ExtensionUrl = "https://chrome.google.com/webstore/detail/fio-client/honhnhpbngledkpkocmeihfgkfmocmkh";
            return new RedirectResponse(ExtensionUrl);
        }

        private Response GetExtensionScript()
        {
            const string ExtensionScript = "script.js";
            if ( File.Exists(Path.Combine(FIOUIReleasePath, ExtensionScript)))
            {
                return File.ReadAllText(Path.Combine(FIOUIReleasePath, ExtensionScript));
            }
            return HttpStatusCode.NoContent;
        }
    }
}
