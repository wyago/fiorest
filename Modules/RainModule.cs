using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FIORest.Authentication;
using FIORest.Database;
using Microsoft.EntityFrameworkCore;
using Nancy;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class RainModule : NancyModule
    {
        public RainModule() : base("/rain")
        {
            Get("/buildings", _ =>
            {
                this.Cacheable();
                return GetBuildings();
            });

            Get("/buildingcosts", _ =>
            {
                this.Cacheable();
                return GetBuildingCosts();
            });

            Get("/buildingworkforces", _ =>
            {
                this.Cacheable();
                return GetBuildingWorkforces();
            });

            Get("/buildingrecipes", _ =>
            {
                this.Cacheable();
                return GetBuildingRecipes();
            });

            Get("/materials", _ =>
            {
                this.Cacheable();
                return GetMaterials();
            });

            Get("/prices", _ =>
            {
                this.Cacheable(10 * 60); // 10 minutes
                return GetPrices();
            });

            Get("/prices/condensed", _ =>
            {
                this.Cacheable(10 * 60); // 10 minutes
                return GetPricesCondensed();
            });

            Get("/orders", _ =>
            {
                this.Cacheable(10 * 60);
                return GetOrders();
            });

            Get("/bids", _ =>
            {
                this.Cacheable(10 * 60);
                return GetBids();
            });

            Get("/recipeinputs", _ =>
            {
                this.Cacheable();
                return GetRecipeInputs();
            });

            Get("/recipeoutputs", _ =>
            {
                this.Cacheable();
                return GetRecipeOutputs();
            });

            Get("/planetresources", _ =>
            {
                this.Cacheable();
                return GetPlanetResources();
            });

            Get("/planetproductionfees", _ =>
            {
                this.Cacheable();
                return GetPlanetProductionFees();
            });

            Get("/userliquid/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserLiquid(parameters.username);
            });

            Get("/userplanets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanets(parameters.username);
            });

            Get("/userplanetbuildings/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetBuildings(parameters.username);
            });

            Get("/userplanetbuildingreclaimables/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetBuildingReclaimables(parameters.username);
            });

            Get("/userplanetproduction/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetProduction(parameters.username);
            });

            Get("/userplanetproductioninput/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetProductionInput(parameters.username);
            });

            Get("/userplanetproductionoutput/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetProductionOutput(parameters.username);
            });

            Get("/userplanetworkforce/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserPlanetWorkforce(parameters.username);
            });

            Get("/userstorage/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetUserStorage(parameters.username);
            });

            Get("/systems", _ =>
            {
                this.Cacheable();
                return GetSystems();
            });

            Get("/systemlinks", _ =>
            {
                this.Cacheable();
                return GetSystemLinks();
            });

            Get("/systemplanets", _ =>
            {
                this.Cacheable();
                return GetSystemPlanets();
            });
        }

        public class RainBuilding
        {
            [JsonProperty(Order = 0)]
            public string Ticker;

            [JsonProperty(Order = 1)]
            public string Name;

            [JsonProperty(Order = 2)]
            public int Area;

            [JsonProperty(Order = 3)]
            public string Expertise;
        }

        private Response GetBuildings()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buildings = DB.BUIModels;
                var res = from b in buildings
                          orderby b.Ticker
                          select new RainBuilding
                          {
                              Ticker = b.Ticker,
                              Name = b.Name,
                              Area = b.AreaCost,
                              Expertise = b.Expertise
                          };

                return JsonConvert.SerializeObject(res.ToList());
            }
        }

        public class RainBuildingCost
        {
            [JsonProperty(Order = 0)]
            public string Key; // TICKER-MAT

            [JsonProperty(Order = 1)]
            public string Building;

            [JsonProperty(Order = 2)]
            public string Material;

            [JsonProperty(Order = 3)]
            public int Amount;
        }

        private Response GetBuildingCosts()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buildingCosts = new List<RainBuildingCost>();

                foreach (var building in DB.BUIModels.Include(b => b.BuildingCosts).OrderBy(b => b.Ticker))
                {
                    foreach (var buildingCost in building.BuildingCosts.OrderBy(bc => bc.CommodityTicker))
                    {
                        RainBuildingCost rbc = new RainBuildingCost();

                        rbc.Key = $"{building.Ticker}-{buildingCost.CommodityTicker}";
                        rbc.Building = building.Ticker;
                        rbc.Material = buildingCost.CommodityTicker;
                        rbc.Amount = buildingCost.Amount;

                        buildingCosts.Add(rbc);
                    }
                }

                return JsonConvert.SerializeObject(buildingCosts);
            }
        }

        public class RainBuildingWorkforce
        {
            [JsonProperty(Order = 0)]
            public string Key; // BUI-WORKER

            [JsonProperty(Order = 1)]
            public string Building;

            [JsonProperty(Order = 2)]
            public string Level; // WORKER

            [JsonProperty(Order = 3)]
            public int Capacity;
        }

        private void PopulateWorkforce(ref List<RainBuildingWorkforce> buildingWorkforces, int workforceCount, string buildingTicker, string workforceType)
        {
            if (workforceCount > 0)
            {
                var rbw = new RainBuildingWorkforce();

                rbw.Key = $"{buildingTicker}-{workforceType}";
                rbw.Building = buildingTicker;
                rbw.Level = workforceType;
                rbw.Capacity = workforceCount;

                buildingWorkforces.Add(rbw);
            }
        }

        private Response GetBuildingWorkforces()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buildingWorkforces = new List<RainBuildingWorkforce>();

                foreach (var building in DB.BUIModels.OrderBy(b => b.Ticker))
                {
                    // Rain keeps them in alphabetical order, so do that
                    PopulateWorkforce(ref buildingWorkforces, building.Engineers, building.Ticker, "ENGINEER");
                    PopulateWorkforce(ref buildingWorkforces, building.Pioneers, building.Ticker, "PIONEER");
                    PopulateWorkforce(ref buildingWorkforces, building.Scientists, building.Ticker, "SCIENTIST");
                    PopulateWorkforce(ref buildingWorkforces, building.Settlers, building.Ticker, "SETTLER");
                    PopulateWorkforce(ref buildingWorkforces, building.Technicians, building.Ticker, "TECHNICIAN");
                }

                return JsonConvert.SerializeObject(buildingWorkforces);
            }
        }

        public class RainBuildingRecipe
        {
            [JsonProperty(Order = 0)]
            public string Key; // BUI-OUTPUT

            [JsonProperty(Order = 1)]
            public string Building;

            [JsonProperty(Order = 2)]
            public long Duration;
        }

        private Response GetBuildingRecipes()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buildingRecipes = new List<RainBuildingRecipe>();
                var buildings = DB.BUIModels
                    .Include(b => b.Recipes).ThenInclude(r => r.Outputs)
                    .Include(b => b.Recipes).ThenInclude(r => r.Inputs)
                    .OrderBy(b => b.Ticker);
                foreach (var building in buildings)
                {
                    foreach (var recipe in building.Recipes.Where(r => r.Outputs.Count > 0).OrderBy(r => r.Outputs[0].CommodityTicker))
                    {
                        var rbr = new RainBuildingRecipe();
                        rbr.Key = $"{building.Ticker}-";
                        rbr.Key += string.Join('-', recipe.Outputs.Select(o => o.CommodityTicker).ToList());
                        rbr.Building = building.Ticker;
                        rbr.Duration = recipe.DurationMs / 1000;

                        buildingRecipes.Add(rbr);
                    }
                }

                return JsonConvert.SerializeObject(buildingRecipes);
            }
        }

        public class RainMaterial
        {
            [JsonProperty(Order = 0)]
            public string Ticker;

            [JsonProperty(Order = 1)]
            public string Name;

            [JsonProperty(Order = 2)]
            public string Category;

            [JsonProperty(Order = 3)]
            public double Weight; // 3 decimal points

            [JsonProperty(Order = 4)]
            public double Volume; // 3 decimal points
        }

        private Response GetMaterials()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = from m in DB.MATModels
                          orderby m.Ticker
                          select new RainMaterial
                          {
                              Ticker = m.Ticker,
                              Name = m.Name,
                              Category = m.CategoryName,
                              Weight = m.Weight.RoundToDecimalPlaces(3),
                              Volume = m.Volume.RoundToDecimalPlaces(3)
                          };

                return JsonConvert.SerializeObject(res);
            }
        }

        private class RainPrices
        {
            [JsonProperty(Order = 1)]
            public string Ticker { get; set; }
            [JsonProperty(Order = 2)]
            public double? MMBuy { get; set; }
            [JsonProperty(Order = 3)]
            public double? MMSell { get; set; }

            [JsonProperty("AI1-Average", Order = 4)]
            public double? AI1_Average { get; set; }
            [JsonProperty("AI1-AskAmt", Order = 5)]
            public int? AI1_AskAmt { get; set; }
            [JsonProperty("AI1-AskPrice", Order = 6)]
            public double? AI1_AskPrice { get; set; }
            [JsonProperty("AI1-AskAvail", Order = 7)]
            public int? AI1_AskAvail { get; set; }
            [JsonProperty("AI1-BidAmt", Order = 8)]
            public int? AI1_BidAmt { get; set; }
            [JsonProperty("AI1-BidPrice", Order = 9)]
            public double? AI1_BidPrice { get; set; }
            [JsonProperty("AI1-BidAvail", Order = 10)]
            public int? AI1_BidAvail { get; set; }

            [JsonProperty("CI1-Average", Order = 11)]
            public double? CI1_Average { get; set; }
            [JsonProperty("CI1-AskAmt", Order = 12)]
            public int? CI1_AskAmt { get; set; }
            [JsonProperty("CI1-AskPrice", Order = 13)]
            public double? CI1_AskPrice { get; set; }
            [JsonProperty("CI1-AskAvail", Order = 14)]
            public int? CI1_AskAvail { get; set; }
            [JsonProperty("CI1-BidAmt", Order = 15)]
            public int? CI1_BidAmt { get; set; }
            [JsonProperty("CI1-BidPrice", Order = 16)]
            public double? CI1_BidPrice { get; set; }
            [JsonProperty("CI1-BidAvail", Order = 17)]
            public int? CI1_BidAvail { get; set; }

            [JsonProperty("NC1-Average", Order = 18)]
            public double? NC1_Average { get; set; }
            [JsonProperty("NC1-AskAmt", Order = 19)]
            public int? NC1_AskAmt { get; set; }
            [JsonProperty("NC1-AskPrice", Order = 20)]
            public double? NC1_AskPrice { get; set; }
            [JsonProperty("NC1-AskAvail", Order = 21)]
            public int? NC1_AskAvail { get; set; }
            [JsonProperty("NC1-BidAmt", Order = 22)]
            public int? NC1_BidAmt { get; set; }
            [JsonProperty("NC1-BidPrice", Order = 23)]
            public double? NC1_BidPrice { get; set; }
            [JsonProperty("NC1-BidAvail", Order = 24)]
            public int? NC1_BidAvail { get; set; }

            [JsonProperty("IC1-Average", Order = 25)]
            public double? IC1_Average { get; set; }
            [JsonProperty("IC1-AskAmt", Order = 26)]
            public int? IC1_AskAmt { get; set; }
            [JsonProperty("IC1-AskPrice", Order = 27)]
            public double? IC1_AskPrice { get; set; }
            [JsonProperty("IC1-AskAvail", Order = 28)]
            public int? IC1_AskAvail { get; set; }
            [JsonProperty("IC1-BidAmt", Order = 29)]
            public int? IC1_BidAmt { get; set; }
            [JsonProperty("IC1-BidPrice", Order = 30)]
            public double? IC1_BidPrice { get; set; }
            [JsonProperty("IC1-BidAvail", Order = 31)]
            public int? IC1_BidAvail { get; set; }

            public object this[string propertyName]
            {
                get
                {
                    // probably faster without reflection:
                    // like:  return Properties.Settings.Default.PropertyValues[propertyName] 
                    // instead of the following
                    Type myType = typeof(RainPrices);
                    PropertyInfo myPropInfo = myType.GetProperty(propertyName);
                    return myPropInfo.GetValue(this, null);
                }
                set
                {
                    Type myType = typeof(RainPrices);
                    PropertyInfo myPropInfo = myType.GetProperty(propertyName);
                    myPropInfo.SetValue(this, value, null);

                }
            }

        }

        private class CXPricePiece
        {
            [JsonProperty(Order = 0)]
            public string Ticker { get; set; }

            [JsonProperty(Order = 1)]
            public string CXCode { get; set; }

            [JsonProperty(Order = 2)]
            public double? MMBuy { get; set; }

            [JsonProperty(Order = 3)]
            public double? MMSell { get; set; }

            [JsonProperty(Order = 4)]
            public double? Average { get; set; }

            [JsonProperty(Order = 5)]
            public int? AskAmt { get; set; }

            [JsonProperty(Order = 6)]
            public double? AskPrice { get; set; }

            [JsonProperty(Order = 7)]
            public int? AskAvail { get; set; }

            [JsonProperty(Order = 8)]
            public int? BidAmt { get; set; }

            [JsonProperty(Order = 9)]
            public double? BidPrice { get; set; }

            [JsonProperty(Order = 10)]
            public int? BidAvail { get; set; }
        }

        private RainPrices createPriceFor(String matname, List<CXPricePiece> cxData)
        {
            var cxPrices = cxData.FindAll(p => p.Ticker == matname).ToList();
            var price = new RainPrices
            {
                Ticker = matname,
                MMBuy = cxPrices.First().MMBuy,
                MMSell = cxPrices.First().MMSell,
            };
            foreach (var entry in cxPrices)
            {
                var cxid = entry.CXCode;
                price[$"{cxid}_Average"] = entry.Average;
                price[$"{cxid}_AskAmt"] = entry.AskAmt;
                price[$"{cxid}_AskPrice"] = entry.AskPrice;
                price[$"{cxid}_AskAvail"] = entry.AskAvail;
                price[$"{cxid}_BidAmt"] = entry.BidAmt;
                price[$"{cxid}_BidPrice"] = entry.BidPrice;
                price[$"{cxid}_BidAvail"] = entry.BidAvail;
            }
            return price;
        }

        private Response GetPrices()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                List<CXPricePiece> cxData = DB.CXDataModels
                    .Select(cx => new CXPricePiece
                    {
                        Ticker = cx.MaterialTicker,
                        CXCode = cx.ExchangeCode,
                        MMBuy = cx.MMBuy,
                        MMSell = cx.MMSell,
                        Average = cx.PriceAverage,
                        AskAmt = cx.AskCount,
                        AskPrice = cx.Ask,
                        AskAvail = cx.Supply,
                        BidAmt = cx.BidCount,
                        BidPrice = cx.Bid,
                        BidAvail = cx.Demand,
                    })
                    .ToList();
                List<string> cxs = cxData.Select(p => p.CXCode).Distinct().ToList();
                var matlist = cxData.Select(dm => dm.Ticker).Distinct().ToList();
                var prices = matlist.Select((matname) => createPriceFor(matname, cxData));
                return JsonConvert.SerializeObject(prices.OrderBy(r => r.Ticker).ToList());
            };
        }

        // Id,Ticker,CX,Type,Value
        private class RainPricesCondensed
        {
            [JsonProperty(Order = 0)]
            public string Id { get; set; }

            [JsonProperty(Order = 1)]
            public string Ticker { get; set; }

            [JsonProperty(Order = 2)]
            public string CX { get; set; }

            [JsonProperty(Order = 2)]
            public string Type { get; set; }

            [JsonProperty(Order = 2)]
            public double? Value { get; set; }
        }

        private Response GetPricesCondensed()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var rawCXData = DB.CXDataModels.OrderBy(cx => cx.MaterialTicker).ToList();
                var rainPricesCondensed = rawCXData.SelectMany(cx => new[]
                    {
                        new RainPricesCondensed
                        {
                            Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-ask",
                            Ticker = cx.MaterialTicker,
                            CX = cx.ExchangeCode,
                            Type = "ask",
                            Value = cx.Ask
                        },
                        new RainPricesCondensed
                        {
                            Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-avg",
                            Ticker = cx.MaterialTicker,
                            CX = cx.ExchangeCode,
                            Type = "avg",
                            Value = cx.PriceAverage
                        },
                        new RainPricesCondensed
                        {
                            Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-bid",
                            Ticker = cx.MaterialTicker,
                            CX = cx.ExchangeCode,
                            Type = "bid",
                            Value = cx.Bid
                        },
                        new RainPricesCondensed
                        {
                            Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-mm-buy",
                            Ticker = cx.MaterialTicker,
                            CX = cx.ExchangeCode,
                            Type = "mm-buy",
                            Value = cx.MMBuy
                        },
                        new RainPricesCondensed
                        {
                            Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-mm-sell",
                            Ticker = cx.MaterialTicker,
                            CX = cx.ExchangeCode,
                            Type = "mm-sell",
                            Value = cx.MMSell
                        }
                    }).ToList();
                return JsonConvert.SerializeObject(rainPricesCondensed);
            }
        }

        public class RainOrderOrBid
        {
            [JsonProperty(Order = 0)]
            public string MaterialName { get; set; }

            [JsonProperty(Order = 1)]
            public string ExchangeCode { get; set; }

            [JsonProperty(Order = 2)]
            public string CompanyId { get; set; }

            [JsonProperty(Order = 3)]
            public string CompanyName { get; set; }

            [JsonProperty(Order = 4)]
            public string CompanyCode { get; set; }

            [JsonProperty(Order = 5)]
            public int ItemCount { get; set; } // -1 = infinite

            [JsonProperty(Order = 6)]
            public double ItemCost { get; set; }
        }

        private Response GetOrders()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allData = DB.CXDataModels
                    .Include(cxdm => cxdm.BuyingOrders).Include(cxdm => cxdm.SellingOrders)
                    .OrderBy(cxdm => cxdm.MaterialName).ThenBy(cxdm => cxdm.ExchangeCode);

                var orders = new List<RainOrderOrBid>();
                foreach (var data in allData)
                {
                    foreach (var entry in data.SellingOrders)
                    {
                        orders.Add(new RainOrderOrBid
                        {
                            MaterialName = data.MaterialName,
                            ExchangeCode = data.ExchangeCode,
                            CompanyId = entry.CompanyId,
                            CompanyName = entry.CompanyName,
                            CompanyCode = entry.CompanyCode,
                            ItemCount = (entry.ItemCount != null) ? (int)entry.ItemCount : -1,
                            ItemCost = entry.ItemCost
                        });
                    }
                }

                return JsonConvert.SerializeObject(orders);
            }
        }

        private Response GetBids()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allData = DB.CXDataModels
                    .Include(cxdm => cxdm.BuyingOrders).Include(cxdm => cxdm.SellingOrders)
                    .OrderBy(cxdm => cxdm.MaterialName).ThenBy(cxdm => cxdm.ExchangeCode);

                var orders = new List<RainOrderOrBid>();
                foreach (var data in allData)
                {
                    foreach (var entry in data.BuyingOrders)
                    {
                        orders.Add(new RainOrderOrBid
                        {
                            MaterialName = data.MaterialName,
                            ExchangeCode = data.ExchangeCode,
                            CompanyId = entry.CompanyId,
                            CompanyName = entry.CompanyName,
                            CompanyCode = entry.CompanyCode,
                            ItemCount = (entry.ItemCount != null) ? (int)entry.ItemCount : -1,
                            ItemCost = entry.ItemCost
                        });
                    }
                }

                return JsonConvert.SerializeObject(orders);
            }
        }

        public class RainRecipeInput
        {
            [JsonProperty(Order = 0)]
            public string Key; // BUI-OUTPUT

            [JsonProperty(Order = 1)]
            public string Material;

            [JsonProperty(Order = 2)]
            public int Amount;
        }

        private Response GetRecipeInputs()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var recipeInputs = new List<RainRecipeInput>();
                var buildings = DB.BUIModels
                    .Include(b => b.Recipes).ThenInclude(r => r.Inputs)
                    .Include(b => b.Recipes).ThenInclude(r => r.Outputs)
                    .OrderBy(b => b.Ticker)
                    .AsSplitQuery();
                foreach (var building in buildings)
                {
                    foreach (var recipe in building.Recipes.Where(r => r.Outputs.Count > 0).OrderBy(r => r.Outputs[0].CommodityTicker))
                    {
                        string RecipeKey = $"{building.Ticker}-";
                        RecipeKey += string.Join('-', recipe.Outputs.Select(o => o.CommodityTicker).ToList());

                        foreach (var input in recipe.Inputs.OrderBy(i => i.CommodityTicker))
                        {
                            var rri = new RainRecipeInput();

                            rri.Key = RecipeKey;
                            rri.Material = input.CommodityTicker;
                            rri.Amount = input.Amount;

                            recipeInputs.Add(rri);
                        }
                    }
                }

                return JsonConvert.SerializeObject(recipeInputs);
            }
        }

        public class RainRecipeOutput
        {
            [JsonProperty(Order = 0)]
            public string Key; // BUI-OUTPUT

            [JsonProperty(Order = 1)]
            public string Material; // Ticker

            [JsonProperty(Order = 2)]
            public int Amount;
        }

        private Response GetRecipeOutputs()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var recipeOutputs = new List<RainRecipeOutput>();
                var buildings = DB.BUIModels
                    .Include(b => b.Recipes).ThenInclude(r => r.Outputs)
                    .OrderBy(b => b.Ticker)
                    .AsSplitQuery();

                foreach (var building in buildings)
                {
                    foreach (var recipe in building.Recipes.Where(r => r.Outputs.Count > 0).OrderBy(r => r.Outputs[0].CommodityTicker))
                    {
                        string RecipeKey = $"{building.Ticker}-";
                        RecipeKey += string.Join('-', recipe.Outputs.Select(o => o.CommodityTicker).ToList());

                        foreach (var output in recipe.Outputs.OrderBy(o => o.CommodityTicker))
                        {
                            var rro = new RainRecipeOutput();

                            rro.Key = RecipeKey;
                            rro.Material = output.CommodityTicker;
                            rro.Amount = output.Amount;

                            recipeOutputs.Add(rro);
                        }
                    }
                }

                return JsonConvert.SerializeObject(recipeOutputs);
            }
        }

        public class RainPlanetResource
        {
            [JsonProperty(Order = 0)]
            public string Key; // PlanetNaturalId-MAT

            [JsonProperty(Order = 1)]
            public string Planet;

            [JsonProperty(Order = 2)]
            public string Ticker;

            [JsonProperty(Order = 3)]
            public string Type;

            [JsonProperty(Order = 4)]
            public double Factor;
        }

        private Response GetPlanetResources()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allPlanetResources = new List<RainPlanetResource>();
                var planets = DB.PlanetDataModels
                    .OrderBy(pdm => pdm.PlanetNaturalId)
                    .Include(pdm => pdm.Resources)
                    .AsSplitQuery()
                    .ToList();
                var planetMaterialList = planets.SelectMany(p => p.Resources.Select(r => r.MaterialId)).Distinct().ToList();
                var materials = DB.MATModels
                    .Where(m => planetMaterialList.Contains(m.MatId))
                    .ToList();
                foreach (var planet in planets)
                {
                    var planetResources = new List<RainPlanetResource>();
                    foreach (var resource in planet.Resources)
                    {
                        var planetResource = new RainPlanetResource();

                        planetResource.Planet = planet.PlanetNaturalId;
                        planetResource.Ticker = materials.Where(m => m.MatId == resource.MaterialId).FirstOrDefault().Ticker;
                        planetResource.Type = resource.ResourceType;
                        planetResource.Factor = resource.Factor;
                        planetResource.Key = $"{planetResource.Planet}-{planetResource.Ticker}";
                        planetResources.Add(planetResource);
                    }

                    planetResources = planetResources.OrderBy(pr => pr.Ticker).ToList();
                    allPlanetResources.AddRange(planetResources);
                }

                return JsonConvert.SerializeObject(allPlanetResources);
            }
        }

        public class RainPlanetProductionFee
        {
            public string PlanetNaturalId { get; set; }
            public string PlanetName { get; set; }

            public string Category { get; set; }
            public double FeeAmount { get; set; }
            public string FeeCurrency { get; set; }
        }

        private Response GetPlanetProductionFees()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allPlanetProductionFees = new List<RainPlanetProductionFee>();
                var planets = DB.PlanetDataModels.OrderBy(pdm => pdm.PlanetNaturalId).Include(pdm => pdm.ProductionFees).AsSplitQuery().ToList();
                foreach(var planet in planets)
                {
                    foreach( var productionFee in planet.ProductionFees)
                    {
                        var planetProductionFee = new RainPlanetProductionFee();
                        planetProductionFee.PlanetNaturalId = planet.PlanetNaturalId;
                        planetProductionFee.PlanetName = planet.PlanetName;
                        planetProductionFee.Category = productionFee.Category;
                        planetProductionFee.FeeAmount = productionFee.FeeAmount;
                        planetProductionFee.FeeCurrency = productionFee.FeeCurrency;
                        allPlanetProductionFees.Add(planetProductionFee);
                    }
                }

                allPlanetProductionFees = allPlanetProductionFees.OrderBy(appf => appf.PlanetNaturalId).ToList();
                return JsonConvert.SerializeObject(allPlanetProductionFees);
            }
        }

        public class RainLiquidData
        {
            [JsonProperty(Order = 0)]
            public string Source;

            [JsonProperty(Order = 1)]
            public string Currency;

            [JsonProperty(Order = 2)]
            public double Amount;
        }

        private Response GetUserLiquid(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.CompanyDataModels
                        .Include(cdm => cdm.Balances)
                        .Where(c => c.UserName.ToUpper() == UserName).FirstOrDefault();
                    if (model != null && model.Balances?.Count > 0)
                    {
                        var allLiquidData = new List<RainLiquidData>();
                        foreach (var balance in model.Balances)
                        {
                            if (balance.Balance > 0.0)
                            {
                                var liquidData = new RainLiquidData();

                                liquidData.Source = $"WALLET.{balance.Currency}";
                                liquidData.Currency = balance.Currency;
                                liquidData.Amount = balance.Balance;

                                allLiquidData.Add(liquidData);
                            }
                        }

                        return JsonConvert.SerializeObject(allLiquidData.OrderBy(l => l.Source).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanet
        {
            [JsonProperty(Order = 0)]
            public string NaturalId;

            [JsonProperty(Order = 1)]
            public string Name;
        }

        private Response GetUserPlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (model != null)
                    {
                        var allPlanets = new List<RainPlanet>();
                        foreach (var site in model.Sites)
                        {
                            var planet = new RainPlanet();

                            planet.NaturalId = site.PlanetIdentifier;
                            planet.Name = site.PlanetName;

                            allPlanets.Add(planet);
                        }

                        return JsonConvert.SerializeObject(allPlanets.OrderBy(p => p.NaturalId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetBuilding
        {
            [JsonProperty(Order = 0)]
            public string NaturalId;

            [JsonProperty(Order = 1)]
            public string Id;

            [JsonProperty(Order = 2)]
            public string Ticker;

            [JsonProperty(Order = 3)]
            public long Created;

            [JsonProperty(Order = 4)]
            public double Condition;
        }

        private Response GetUserPlanetBuildings(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (model != null)
                    {
                        var allPlanetBuildings = new List<RainPlanetBuilding>();
                        foreach (var site in model.Sites)
                        {
                            foreach (var building in site.Buildings)
                            {
                                if (building.BuildingId != null)
                                {
                                    var planetBuilding = new RainPlanetBuilding();

                                    planetBuilding.NaturalId = site.PlanetIdentifier;
                                    planetBuilding.Id = $"{building.BuildingTicker}-{building.BuildingId.ToUpper().Substring(0, 8)}";
                                    planetBuilding.Ticker = building.BuildingTicker;
                                    planetBuilding.Created = building.BuildingCreated;
                                    planetBuilding.Condition = building.Condition;

                                    allPlanetBuildings.Add(planetBuilding);
                                }
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetBuildings.OrderBy(pb => pb.NaturalId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetBuildingReclaimable
        {
            [JsonProperty(Order = 0)]
            public string BuildingId;

            [JsonProperty(Order = 1)]
            public string Material;

            [JsonProperty(Order = 2)]
            public int Amount;
        }

        private Response GetUserPlanetBuildingReclaimables(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Building))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.SITESModels
                        .Include(sm => sm.Sites)
                            .ThenInclude(sms => sms.Buildings)
                                .ThenInclude(smsb => smsb.ReclaimableMaterials)
                        .AsSplitQuery()
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName).FirstOrDefault();
                    if (model != null)
                    {
                        var allPlanetBuildingReclaimables = new List<RainPlanetBuildingReclaimable>();
                        foreach (var site in model.Sites)
                        {
                            foreach (var building in site.Buildings)
                            {
                                foreach (var reclaimable in building.ReclaimableMaterials)
                                {
                                    if (building.BuildingId != null)
                                    {
                                        var planetBuildingReclaimable = new RainPlanetBuildingReclaimable();

                                        planetBuildingReclaimable.BuildingId = $"{building.BuildingTicker}-{building.BuildingId.ToUpper().Substring(0, 8)}";
                                        planetBuildingReclaimable.Material = reclaimable.MaterialTicker;
                                        planetBuildingReclaimable.Amount = reclaimable.MaterialAmount;

                                        allPlanetBuildingReclaimables.Add(planetBuildingReclaimable);
                                    }
                                }
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetBuildingReclaimables.OrderBy(pbr => pbr.BuildingId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetProduction
        {
            [JsonProperty(Order = 0)]
            public string NaturalId;

            [JsonProperty(Order = 1)]
            public string Type;

            [JsonProperty(Order = 2)]
            public string Id;

            [JsonProperty(Order = 3)]
            public double Completed;

            [JsonProperty(Order = 4)]
            public long? Remaining;
        }

        private Response GetUserPlanetProduction(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.PRODLinesModels
                        .Where(plm => plm.UserNameSubmitted.ToUpper() == UserName)
                        .Include(plm => plm.ProductionLines)
                            .ThenInclude(pl => pl.Orders)
                        .AsSplitQuery()
                        .ToList();
                    if (model != null && model.Count > 0)
                    {
                        var allPlanetProduction = new List<RainPlanetProduction>();
                        foreach (var prodLineModel in model)
                        {
                            foreach(var prodLine in prodLineModel.ProductionLines)
                            {
                                foreach (var order in prodLine.Orders)
                                {
                                    if (order.ProductionId != null && order.StartedEpochMs != null)
                                    {
                                        var prod = new RainPlanetProduction();

                                        prod.NaturalId = prodLine.PlanetNaturalId;
                                        prod.Type = prodLine.Type;
                                        prod.Id = $"{prodLine.PlanetNaturalId}-{prodLine.Type}-{order.ProductionId.ToUpper().Substring(0, 8)}";
                                        prod.Completed = (order.CompletedPercentage != null) ? (double)order.CompletedPercentage : 0.0;
                                        prod.Remaining = order.CompletionEpochMs - DateTime.UtcNow.ToEpochMs();

                                        allPlanetProduction.Add(prod);
                                    }
                                }
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetProduction.OrderBy(pp => pp.NaturalId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetProductionInput
        {
            [JsonProperty(Order = 0)]
            public string OrderId;

            [JsonProperty(Order = 1)]
            public string Material;

            [JsonProperty(Order = 2)]
            public int Count;
        }

        private Response GetUserPlanetProductionInput(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.PRODLinesModels
                        .Where(plm => plm.UserNameSubmitted.ToUpper() == UserName)
                        .Include(plm => plm.ProductionLines)
                            .ThenInclude(pl => pl.Orders)
                                .ThenInclude(plo => plo.Inputs)
                        .AsSplitQuery()
                        .ToList();
                    if (model != null)
                    {
                        var allPlanetProductionInputs = new List<RainPlanetProductionInput>();
                        foreach (var prodLinesModel in model)
                        {
                            foreach (var prodLine in prodLinesModel.ProductionLines)
                            {
                                foreach (var order in prodLine.Orders)
                                {
                                    if (order.ProductionId != null && order.StartedEpochMs != null)
                                    {
                                        foreach (var input in order.Inputs)
                                        {
                                            var inp = new RainPlanetProductionInput();

                                            inp.OrderId = $"{prodLine.PlanetNaturalId}-{prodLine.Type}-{order.ProductionId.ToUpper().Substring(0, 8)}";
                                            inp.Material = input.MaterialTicker;
                                            inp.Count = input.MaterialAmount;

                                            allPlanetProductionInputs.Add(inp);
                                        }
                                    }
                                }
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetProductionInputs.OrderBy(ppi => ppi.OrderId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetProductionOutput
        {
            [JsonProperty(Order = 0)]
            public string OrderId;

            [JsonProperty(Order = 1)]
            public string Material;

            [JsonProperty(Order = 2)]
            public int Count;
        }

        private Response GetUserPlanetProductionOutput(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Production))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.PRODLinesModels
                        .Where(plm => plm.UserNameSubmitted.ToUpper() == UserName)
                        .Include(plm => plm.ProductionLines)
                            .ThenInclude(pl => pl.Orders)
                                .ThenInclude(plo => plo.Outputs)
                        .AsSplitQuery()
                        .ToList();
                    if (model != null)
                    {
                        var allPlanetProductionOutputs = new List<RainPlanetProductionOutput>();

                        foreach (var prodLinesModel in model)
                        {
                            foreach (var prodLine in prodLinesModel.ProductionLines)
                            {
                                foreach (var order in prodLine.Orders)
                                {
                                    if (order.ProductionId != null && order.StartedEpochMs != null)
                                    {
                                        foreach (var output in order.Outputs)
                                        {
                                            var outp = new RainPlanetProductionOutput();

                                            outp.OrderId = $"{prodLine.PlanetNaturalId}-{prodLine.Type}-{order.ProductionId.ToUpper().Substring(0, 8)}";
                                            outp.Material = output.MaterialTicker;
                                            outp.Count = output.MaterialAmount;

                                            allPlanetProductionOutputs.Add(outp);
                                        }
                                    }
                                }
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetProductionOutputs.OrderBy(ppo => ppo.OrderId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainPlanetWorkforce
        {
            [JsonProperty(Order = 0)]
            public string NaturalId;

            [JsonProperty(Order = 1)]
            public string Workforce;

            [JsonProperty(Order = 2)]
            public int Population;

            [JsonProperty(Order = 3)]
            public int Capacity;

            [JsonProperty(Order = 4)]
            public int Required;

            [JsonProperty(Order = 5)]
            public double Satisfaction;
        }

        private Response GetUserPlanetWorkforce(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.WorkforceModels
                        .Where(w => w.UserNameSubmitted.ToUpper() == UserName)
                        .Include(wfm => wfm.Workforces)
                        .ToList();
                    if (model != null)
                    {
                        var allPlanetWorkforces = new List<RainPlanetWorkforce>();
                        foreach (var workforceModel in model)
                        {
                            foreach (var desc in workforceModel.Workforces)
                            {
                                var rpw = new RainPlanetWorkforce();

                                rpw.NaturalId = workforceModel.PlanetNaturalId;
                                rpw.Workforce = desc.WorkforceTypeName;
                                rpw.Population = desc.Population;
                                rpw.Capacity = desc.Capacity;
                                rpw.Required = desc.Required;
                                rpw.Satisfaction = desc.Satisfaction;

                                allPlanetWorkforces.Add(rpw);
                            }
                        }

                        return JsonConvert.SerializeObject(allPlanetWorkforces.OrderBy(w => w.NaturalId).ToList());
                    }

                    return HttpStatusCode.NoContent;
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainStorage
        {
            [JsonProperty(Order = 0)]
            public string NaturalId;

            [JsonProperty(Order = 1)]
            public string Name;

            [JsonProperty(Order = 2)]
            public string Type;

            [JsonProperty(Order = 3)]
            public string Ticker;

            [JsonProperty(Order = 4)]
            public int Amount;
        }

        private Response GetUserStorage(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var allStorage = new List<RainStorage>();

                    var model = DB.StorageModels
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName && s.StorageId != null)
                        .Include(sm => sm.StorageItems)
                        .ToList();
                    List<string> storageNames = model
                        .FindAll(sm => sm.Type != "STORE" && sm.Name != null)
                        .Select(sm => sm.Name.ToUpper())
                        .ToList();
                    var ships = DB.SHIPSShips
                        .Where(ss => storageNames.Contains(ss.Registration.ToUpper()))
                        .ToList();
                    foreach (var storageModel in model)
                    {
                        var site = DB.SITESSites.Where(site => site.SiteId == storageModel.AddressableId).FirstOrDefault();
                        if (site != null)
                        {
                            foreach (var item in storageModel.StorageItems)
                            {
                                var s = new RainStorage();

                                s.NaturalId = site.PlanetIdentifier;
                                s.Name = site.PlanetName;
                                s.Type = storageModel.Type;
                                s.Ticker = item.MaterialTicker;
                                s.Amount = item.MaterialAmount;

                                allStorage.Add(s);
                            }
                        }

                        if (storageModel.Type != "STORE")
                        {
                            foreach (var item in storageModel.StorageItems)
                            {
                                if (item.Type == "SHIPMENT" || storageModel.Name == null)
                                {
                                    continue;
                                }

                                var s = new RainStorage();

                                s.NaturalId = storageModel.Name;
                                var ship = ships.Find(s => s.Registration.ToUpper() == storageModel.Name.ToUpper());
                                if (ship != null && ship.Name != null)
                                {
                                    s.Name = ship.Name;
                                }
                                s.Type = storageModel.Type;
                                s.Ticker = item.MaterialTicker;
                                s.Amount = item.MaterialAmount;

                                allStorage.Add(s);
                            }
                        }
                    }

                    // Grab CX sells
                    var cxModel = DB.CXOSTradeOrderModels
                        .Where(cx => cx.UserNameSubmitted.ToUpper() == UserName)
                        .Include(cx => cx.TradeOrders)
                        .FirstOrDefault();
                    if (cxModel != null)
                    {
                        var sells = cxModel.TradeOrders.Where(t => t.Status.ToUpper() == "PLACED" && t.OrderType.ToUpper() == "SELLING");
                        foreach (var sell in sells)
                        {
                            var rs = new RainStorage();

                            rs.NaturalId = $"CX-{sell.ExchangeCode}";
                            rs.Name = sell.ExchangeName;
                            rs.Type = "CX-SELL";
                            rs.Ticker = sell.MaterialTicker;
                            rs.Amount = sell.Amount;

                            allStorage.Add(rs);
                        }
                    }

                    return JsonConvert.SerializeObject(allStorage.OrderBy(s => s.NaturalId).ToList());
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        public class RainSystem
        {
            [JsonProperty(Order = 0)]
            public string NaturalId { get; set; }

            [JsonProperty(Order = 1)]
            public string Name { get; set; }

            [JsonProperty(Order = 2)]
            public string Type { get; set; }

            [JsonProperty(Order = 3)]
            public double PositionX { get; set; }

            [JsonProperty(Order = 4)]
            public double PositionY { get; set; }

            [JsonProperty(Order = 5)]
            public double PositionZ { get; set; }

            [JsonProperty(Order = 6)]
            public string SectorId { get; set; }

            [JsonProperty(Order = 7)]
            public string SubSectorId { get; set; }
        }

        private Response GetSystems()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.SystemStarsModels.OrderBy(ssm => ssm.NaturalId).Select(ssm => new RainSystem
                {
                    NaturalId = ssm.NaturalId,
                    Name = ssm.Name,
                    Type = ssm.Type,
                    PositionX = ssm.PositionX,
                    PositionY = ssm.PositionY,
                    PositionZ = ssm.PositionZ,
                    SectorId = ssm.SectorId,
                    SubSectorId = ssm.SubSectorId
                }).ToList());
            }
        }

        public class RainSystemLink
        {
            [JsonProperty(Order = 0)]
            public string Left { get; set; }

            [JsonProperty(Order = 1)]
            public string Right { get; set; }
        }

        private Response GetSystemLinks()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var systemLinks = new List<RainSystemLink>();
                var systems = DB.SystemStarsModels
                    .OrderBy(ssm => ssm.NaturalId)
                    .Include(ssm => ssm.Connections)
                    .ToList();
                var otherSystemIds = systems.SelectMany(s => s.Connections.Select(c => c.Connection).ToList()).Distinct().ToList();
                var connectedSystems = DB.SystemStarsModels
                    .Where(ossm => otherSystemIds.Contains(ossm.SystemId))
                    .Select(ossm => new { ossm.SystemId, ossm.NaturalId })
                    .ToList();
                foreach(var system in systems)
                {
                    var RainConnections = new List<string>();
                    foreach(var connection in system.Connections)
                    {
                        var otherSystem = connectedSystems.Find(cs => cs.SystemId == connection.Connection);
                        if (otherSystem != null)
                        {
                            RainConnections.Add(otherSystem.NaturalId);
                        }
                    }

                    RainConnections.Sort();
                    foreach(var RainConnection in RainConnections)
                    {
                        var link = new RainSystemLink();

                        link.Left = system.NaturalId;
                        link.Right = RainConnection;

                        systemLinks.Add(link);
                    }
                }

                return JsonConvert.SerializeObject(systemLinks);
            }
        }

        public class RainSystemPlanet
        {
            [JsonProperty(Order = 0)]
            public string NaturalId { get; set; }

            [JsonProperty(Order = 1)]
            public string Name { get; set; }

            [JsonProperty(Order = 2)]
            public double Radiation { get; set; }

            [JsonProperty(Order = 3)]
            public double Pressure { get; set; }

            [JsonProperty(Order = 4)]
            public int OrbitIndex { get; set; }

            [JsonProperty(Order = 5)]
            public double Fertility { get; set; } // -1 = infertile

            [JsonProperty(Order = 6)]
            public double Sunlight { get; set; }

            [JsonProperty(Order = 7)]
            public bool Surface { get; set; }

            [JsonProperty(Order = 8)]
            public double Gravity { get; set; }

            [JsonProperty(Order = 9)]
            public double Radius { get; set; }

            [JsonProperty(Order = 10)]
            public double Temperature { get; set; }

            [JsonProperty(Order = 11)]
            public double Mass { get; set; }

            [JsonProperty(Order = 12)]
            public double MagneticField { get; set; }

            [JsonProperty(Order = 13)]
            public double MassEarth { get; set; }
        }

        private Response GetSystemPlanets()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.PlanetDataModels.OrderBy(pdm => pdm.PlanetNaturalId).Select(pdm => new RainSystemPlanet
                {
                    NaturalId = pdm.PlanetNaturalId,
                    Name = pdm.PlanetName,
                    Radiation = pdm.Radiation,
                    Pressure = pdm.Pressure,
                    OrbitIndex = pdm.OrbitIndex,
                    Fertility = pdm.Fertility,
                    Sunlight = pdm.Sunlight,
                    Surface = pdm.Surface,
                    Gravity = pdm.Gravity,
                    Radius = pdm.Radius,
                    Temperature = pdm.Temperature,
                    Mass = pdm.Mass,
                    MagneticField = pdm.MagneticField,
                    MassEarth = pdm.MassEarth
                }).ToList());
            }
        }
    }
}
