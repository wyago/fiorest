﻿using System;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class MaterialModule : NancyModule
    {
        public MaterialModule() : base("/material")
        {
            this.Cacheable();

            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostMaterial();
            });

            Get("/allmaterials", _ =>
            {
                return GetAllMaterials();
            });

            Get("/{material}", parameters =>
            {
                return GetMaterial(parameters.material);
            });

            Get("/category/{category_name}", parameters =>
            {
                return GetMaterialByCategory(parameters.category_name);
            });
        }

        private Response PostMaterial()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldMaterialCategories.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                foreach (var category in data.categories)
                {
                    foreach (var material in category.materials)
                    {
                        MATModel model = new MATModel();
                        MATModel oldModel = req.DB.MATModels.Where(b => b.Ticker == material.ticker).FirstOrDefault();
                        bool bIsAdd = (oldModel == null);

                        model.CategoryName = category.name;
                        model.CategoryId = category.id;
                        model.Name = material.name;
                        model.MatId = material.id;
                        model.Ticker = material.ticker;
                        model.Weight = material.weight;
                        model.Volume = material.volume;

                        model.UserNameSubmitted = req.UserName;
                        model.Timestamp = req.Now;

                        if (bIsAdd)
                        {
                            req.DB.MATModels.Add(model);
                        }
                        else if ( !model.Equals(oldModel))
                        {
                            req.DB.MATModels.Remove(oldModel);
                            req.DB.MATModels.Add(model);
                        }
                    }
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetAllMaterials()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var allMaterials = DB.MATModels.ToList();
                return JsonConvert.SerializeObject(allMaterials);
            }
        }

        private Response GetMaterial(string Material)
        {
            Material = Material.ToUpper();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.MATModels.Where(m => m.Ticker.ToUpper() == Material || m.Name.ToUpper() == Material || m.MatId.ToUpper() == Material).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetMaterialByCategory(string Category)
        {
            Category = Category.ToUpper();
            using(var DB = PRUNDataContext.GetNewContext())
            {
                var res = DB.MATModels.Where(m => m.CategoryName.ToUpper() == Category).ToList();
                if ( res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}
