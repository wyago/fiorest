﻿using System;
using System.Collections.Generic;
using System.Linq;

using Dijkstra.NET.Graph;
using Dijkstra.NET.ShortestPath;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class SystemStarsModule : NancyModule
    {
        public SystemStarsModule() : base("/systemstars")
        {
            this.Cacheable();

            Post("/", _ =>
            {
                this.EnforceAuthAdmin();
                return PostSystemStars();
            });

            Post("/worldsectors", _ =>
            {
                this.EnforceAuthAdmin();
                return PostWorldSectors();
            });

            Post("/star", _ =>
            {
                this.EnforceWriteAuth();
                return PostStar();
            });

            Get("/", _ =>
            {
                this.Cacheable();
                return GetSystemStars();
            });

            Get("/hash", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/star/{star}", parameters =>
            {
                return GetStar(parameters.star);
            });

            Get("/worldsectors", _ =>
            {
                this.Cacheable();
                return GetWorldSectors();
            });

            Get("/worldsectors/hash", _ =>
            {
                this.EnforceReadAuth();
                return null;
            });

            Get("/jumpcount/{source}/{destination}", parameters =>
            {
                return GetJumpCount(parameters.source, parameters.destination);
            });

            Get("/jumproute/{source}/{destination}", parameters =>
            {
                return GetJumpRoute(parameters.source, parameters.destination);
            });
        }

        private Response PostSystemStars()
        {
            using(var req = new FIORequest<JSONRepresentations.SystemStarsdata.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                req.DB.SystemStarsModels.RemoveRange(req.DB.SystemStarsModels);

                var stars = rootObj.payload.message.payload.stars;
                foreach(var star in stars)
                {
                    SystemStarsModel model = new SystemStarsModel();

                    model.SystemId = star.systemId;
                    model.Name = star.address.lines[0].entity.name;
                    model.NaturalId = star.address.lines[0].entity.naturalId;
                    model.Type = star.type;

                    model.PositionX = star.position.x;
                    model.PositionY = star.position.y;
                    model.PositionZ = star.position.z;

                    model.SectorId = star.sectorId;
                    model.SubSectorId = star.subSectorId;

                    foreach(var connection in star.connections)
                    {
                        SystemConnection sysConnection = new SystemConnection();

                        sysConnection.Connection = connection;

                        model.Connections.Add(sysConnection);
                    }

                    req.DB.SystemStarsModels.Add(model);
                }

                req.DB.SaveChanges();
                JumpCalculator.Initialize(req.DB.SystemStarsModels.ToList());
                return HttpStatusCode.OK;
            }
        }

        private Response PostWorldSectors()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldSectors.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                req.DB.WorldSectorsModels.RemoveRange(req.DB.WorldSectorsModels);

                var sectors = rootObj.payload.message.payload.sectors;
                foreach( var sector in sectors)
                {
                    WorldSectorsModel model = new WorldSectorsModel();

                    model.SectorId = sector.id;
                    model.Name = sector.name;
                    model.HexQ = sector.hex.q;
                    model.HexR = sector.hex.r;
                    model.HexS = sector.hex.s;
                    model.Size = sector.size;

                    foreach( var subsector in sector.subsectors )
                    {
                        SubSector ss = new SubSector();

                        ss.SSId = subsector.id;
                        
                        foreach(var vertex in subsector.vertices)
                        {
                            SubSectorVertex ssv = new SubSectorVertex();

                            ssv.X = vertex.x;
                            ssv.Y = vertex.y;
                            ssv.Z = vertex.z;

                            ss.Vertices.Add(ssv);
                        }

                        model.SubSectors.Add(ss);
                    }

                    req.DB.WorldSectorsModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostStar()
        {
            using (var req = new FIORequest<JSONRepresentations.SystemStar.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var jsonStarPayload = req.JsonPayload.payload.message.payload.body;
                var star = req.DB.SystemStars.Where(ss => ss.SectorId == jsonStarPayload.star.sectorId && ss.SubSectorId == jsonStarPayload.star.subSectorId).FirstOrDefault();
                bool bAdd = (star == null);
                if (bAdd)
                {
                    star = new SystemStar();
                }

                star.SystemId = jsonStarPayload.address.lines[0].entity.id;
                star.SystemNaturalId = jsonStarPayload.address.lines[0].entity.naturalId;
                star.SystemName = jsonStarPayload.address.lines[0].entity.name;

                star.Type = jsonStarPayload.star.type;
                star.Luminosity = jsonStarPayload.star.luminosity;
                star.PositionX = jsonStarPayload.star.position.x;
                star.PositionY = jsonStarPayload.star.position.y;
                star.PositionZ = jsonStarPayload.star.position.z;
                star.SectorId = jsonStarPayload.star.sectorId;
                star.SubSectorId = jsonStarPayload.star.subSectorId;
                star.Mass = jsonStarPayload.star.mass;
                star.MassSol = jsonStarPayload.star.massSol;

                if (bAdd)
                {
                    req.DB.SystemStars.Add(star);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSystemStars()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.SystemStarsModels.ToList());
            }
        }

        private Response GetStar(string Star)
        {
            Star = Star.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var star = DB.SystemStars.Where(ss => ss.SystemId.ToUpper() == Star || ss.SystemName.ToUpper() == Star || ss.SystemNaturalId.ToUpper() == Star).FirstOrDefault();
                if (star != null )
                {
                    return JsonConvert.SerializeObject(star);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetWorldSectors()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.WorldSectorsModels.ToList());
            }
        }

        enum JumpResponseType
        {
            Count = 0,
            Route = 1,
        }

        private Response GetJump(string source, string destination, JumpResponseType type)
        {
            if (String.IsNullOrWhiteSpace(source) || String.IsNullOrWhiteSpace(destination))
            {
                return NancyExtensionMethods.ReturnBadResponse(Request, new ArgumentException("Source and destination must both be specified"));
            }

            string VerifiedSource = JumpCalculator.GetSystemId(source);
            string VerifiedDestination = JumpCalculator.GetSystemId(destination);
            if (VerifiedSource != null && VerifiedDestination != null)
            {
                if (type == JumpResponseType.Count)
                {
                    return JumpCalculator.GetJumpCount(VerifiedSource, VerifiedDestination).ToString();
                }
                else
                {
                    System.Diagnostics.Debug.Assert(type == JumpResponseType.Route);
                    return JsonConvert.SerializeObject(JumpCalculator.GetRoute(VerifiedSource, VerifiedDestination));
                }
            }
            else
            {
                return NancyExtensionMethods.ReturnBadResponse(Request, new ArgumentException("Failed to resolve source or destination"));
            }
        }

        private Response GetJumpCount(string source, string destination)
        {
            return GetJump(source, destination, JumpResponseType.Count);
        }

        private Response GetJumpRoute(string source, string destination)
        {
            return GetJump(source, destination, JumpResponseType.Route);
        }
    }
}
