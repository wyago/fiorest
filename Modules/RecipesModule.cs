﻿using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using System.Data.Entity;

namespace FIORest.Modules
{
    public class RecipesModule : NancyModule
    {
        public RecipesModule() : base("/recipes")
        {
            this.Cacheable();

            Get("/allrecipes", _ =>
            {
                return GetRecipes();
            });

            Get("/{ticker}", parameters =>
            {
                return GetRecipesForTicker(parameters.ticker);
            });
        }

        class MinimalInput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalOutput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalRecipe
        {
            public string BuildingTicker { get; set; }
            public string RecipeName { get; set; }
            public List<MinimalInput> Inputs { get; set; } = new List<MinimalInput>();
            public List<MinimalOutput> Outputs { get; set; } = new List<MinimalOutput>();
            public int TimeMs { get; set; }
        }

        private Response GetRecipes()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                List<List<BUIRecipe>> res = DB.BUIModels.Select(b => b.Recipes).ToList();

                List<MinimalRecipe> minimalRecipes = new List<MinimalRecipe>();
                foreach(var listRecipes in res)
                {
                    foreach(var recipe in listRecipes)
                    {
                        MinimalRecipe mr = new MinimalRecipe();
                        mr.BuildingTicker = recipe.BUIModel.Ticker;
                        mr.RecipeName = recipe.RecipeName;

                        foreach (var input in recipe.Inputs)
                        {
                            MinimalInput mi = new MinimalInput();
                            mi.Ticker = input.CommodityTicker;
                            mi.Amount = input.Amount;
                            mr.Inputs.Add(mi);
                        }

                        foreach (var output in recipe.Outputs)
                        {
                            MinimalOutput mo = new MinimalOutput();
                            mo.Ticker = output.CommodityTicker;
                            mo.Amount = output.Amount;
                            mr.Outputs.Add(mo);
                        }

                        mr.TimeMs = recipe.DurationMs;
                        minimalRecipes.Add(mr);
                    }
                }

                return JsonConvert.SerializeObject(minimalRecipes);
            }
        }

        class RecipeForTickerPayload : BUIRecipe
        {
            public string BuildingTicker { get; set; }
        }

        private Response GetRecipesForTicker(string ticker)
        {
            ticker = ticker.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var results = new List<RecipeForTickerPayload>();

                var buildings = DB.BUIModels.ToList();
                foreach(var building in buildings)
                {
                    var recipeInputs = building.Recipes.SelectMany(r => r.Inputs).Where(i => i.CommodityTicker == ticker);
                    foreach(var recipeInput in recipeInputs)
                    {
                        var payload = new RecipeForTickerPayload();
                        payload.BuildingTicker = recipeInput.BUIRecipe.BUIModel.Ticker;
                        payload.RecipeName = recipeInput.BUIRecipe.RecipeName;
                        payload.Inputs = recipeInput.BUIRecipe.Inputs;
                        payload.Outputs = recipeInput.BUIRecipe.Outputs;
                        payload.DurationMs = recipeInput.BUIRecipe.DurationMs;
                        results.Add(payload);
                    }

                    var recipeOutputs = building.Recipes.SelectMany(r => r.Outputs).Where(o => o.CommodityTicker == ticker);
                    foreach(var recipeOutput in recipeOutputs)
                    {
                        var payload = new RecipeForTickerPayload();
                        payload.BuildingTicker = recipeOutput.BUIRecipe.BUIModel.Ticker;
                        payload.RecipeName = recipeOutput.BUIRecipe.RecipeName;
                        payload.Inputs = recipeOutput.BUIRecipe.Inputs;
                        payload.Outputs = recipeOutput.BUIRecipe.Outputs;
                        payload.DurationMs = recipeOutput.BUIRecipe.DurationMs;
                        results.Add(payload);
                    }
                }

                return JsonConvert.SerializeObject(results);
            }
        }
    }
}
