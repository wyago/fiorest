﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

using Nancy;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class GlobalModule : NancyModule
    {
        public GlobalModule() : base("/global")
        {
            this.Cacheable();

            Post("/comexexchanges", _ =>
            {
                this.EnforceAuthAdmin();
                return PostComexExchanges();
            });

            Post("/countries" , _ =>
            {
                this.EnforceAuthAdmin();
                return PostCountries();
            });

            Post("/simulationdata", _ =>
            {
                this.EnforceAuthAdmin();
                return PostSimulationData();
            });

            Post("/workforceneeds", _ =>
            {
                this.EnforceAuthAdmin();
                return PostWorkforceNeeds();
            });

            Get("/comexexchanges", _ =>
            {
                return GetComexExchanges();
            });

            Get("/countries", _ =>
            {
                return GetCountries();
            });

            Get("/simulationdata", _ =>
            {
                return GetSimulationData();
            });

            Get("/workforceneeds", _ =>
            {
                return GetWorkforceNeeds();
            });
        }

        private Response PostComexExchanges()
        {
            using (var req = new FIORequest<JSONRepresentations.ComexExchangeList.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var exchanges = req.JsonPayload.payload.message.payload.exchanges;
                foreach( var exchange in exchanges)
                {
                    var existingExchange = req.DB.ComexExchanges.Where(ce => ce.ExchangeCode == exchange.code).FirstOrDefault();
                    bool bAdd = (existingExchange == null);
                    if (bAdd)
                    {
                        existingExchange = new ComexExchange();
                    }

                    existingExchange.ExchangeId = exchange.id;
                    existingExchange.ExchangeName = exchange.name;
                    existingExchange.ExchangeCode = exchange.code;
                    if ( exchange._operator != null )
                    {
                        existingExchange.ExchangeOperatorId = exchange._operator.id;
                        existingExchange.ExchangeOperatorName = exchange._operator.name;
                        existingExchange.ExchangeOperatorCode = exchange._operator.code;
                    }

                    if (exchange.currency != null)
                    {
                        existingExchange.CurrencyNumericCode = exchange.currency.numericCode;
                        existingExchange.CurrencyCode = exchange.currency.code;
                        existingExchange.CurrencyName = exchange.currency.name;
                        existingExchange.CurrencyDecimals = exchange.currency.decimals;
                    }

                    if (exchange.address != null && exchange.address.lines.Length > 0)
                    {
                        var lastLine = exchange.address.lines[exchange.address.lines.Length - 1];
                        existingExchange.LocationId = lastLine.entity.id;
                        existingExchange.LocationName = lastLine.entity.name;
                        existingExchange.LocationNaturalId = lastLine.entity.naturalId;
                    }

                    if (bAdd)
                    {
                        req.DB.ComexExchanges.Add(existingExchange);
                    }
                }

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostCountries()
        {
            using (var req = new FIORequest<JSONRepresentations.CountryRegistryCountries.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var countries = req.JsonPayload.payload.message.payload.countries;
                foreach( var country in countries)
                {
                    var existingCountry = req.DB.CountryRegistryCountries.Where(crc => crc.CountryId == country.id).FirstOrDefault();
                    bool bAdd = (existingCountry == null);
                    if (bAdd)
                    {
                        existingCountry = new CountryRegistryCountry();
                    }

                    existingCountry.CountryId = country.id;
                    existingCountry.CountryCode = country.code;
                    existingCountry.CountryName = country.name;

                    existingCountry.CurrencyNumericCode = country.currency.numericCode;
                    existingCountry.CurrencyCode = country.currency.code;
                    existingCountry.CurrencyName = country.currency.name;
                    existingCountry.CurrencyDecimals = country.currency.decimals;

                    if (bAdd)
                    {
                        req.DB.CountryRegistryCountries.Add(existingCountry);
                    }
                }

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostSimulationData()
        {
            using (var req = new FIORequest<JSONRepresentations.SimulationData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var simulationData = req.JsonPayload.payload.message.payload;
                var existingSimulationData = req.DB.SimulationData.FirstOrDefault();
                bool bAdd = (existingSimulationData == null);
                if (bAdd)
                {
                    existingSimulationData = new SimulationData();
                }

                existingSimulationData.SimulationInterval = simulationData.simulationInterval;
                existingSimulationData.FlightSTLFactor = simulationData.flightSTLFactor;
                existingSimulationData.FlightFTLFactor = simulationData.flightFTLFactor;
                existingSimulationData.PlanetaryMotionFactor = simulationData.planetaryMotionFactor;
                existingSimulationData.ParsecLength = simulationData.parsecLength;


                if (bAdd)
                {
                    req.DB.SimulationData.Add(existingSimulationData);
                }

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response PostWorkforceNeeds()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforces.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var workforces = req.JsonPayload.payload.message.payload.workforces;
                foreach(var workforce in workforces)
                {
                    var workforcePer100 = req.DB.WorkforcePerOneHundreds.Where(wpoh => wpoh.WorkforceType.ToUpper() == workforce.level.ToUpper()).FirstOrDefault();
                    bool bAddWorkforcePer100 = (workforcePer100 == null);
                    if (bAddWorkforcePer100)
                    {
                        workforcePer100 = new WorkforcePerOneHundred();
                    }

                    workforcePer100.WorkforceType = workforce.level;

                    foreach(var need in workforce.needs)
                    {
                        var workforcePer100Need = workforcePer100.Needs.Where(n => n.MaterialTicker.ToUpper() == need.material.ticker.ToUpper()).FirstOrDefault();
                        bool bAddWorkforcePer100Need = (workforcePer100Need == null);
                        if (bAddWorkforcePer100Need)
                        {
                            workforcePer100Need = new WorkforcePerOneHundreedNeed();
                        }

                        workforcePer100Need.MaterialId = need.material.id;
                        workforcePer100Need.MaterialName = need.material.name;
                        workforcePer100Need.MaterialTicker = need.material.ticker;
                        workforcePer100Need.MaterialCategory = need.material.category;
                        workforcePer100Need.Amount = need.unitsPer100;

                        if (bAddWorkforcePer100Need)
                        {
                            workforcePer100.Needs.Add(workforcePer100Need);
                        }
                    }

                    if (bAddWorkforcePer100)
                    {
                        req.DB.WorkforcePerOneHundreds.Add(workforcePer100);
                    }

                    req.DB.SaveChanges();
                }

                return HttpStatusCode.OK;
            }
        }

        private Response GetComexExchanges()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.ComexExchanges.ToList());
            }
        }

        private Response GetCountries()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.CountryRegistryCountries.ToList());
            }
        }

        private Response GetSimulationData()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var simData = DB.SimulationData.FirstOrDefault();
                if (simData != null)
                {
                    return JsonConvert.SerializeObject(simData);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetWorkforceNeeds()
        {
            using(var DB = PRUNDataContext.GetNewContext())
            {
                return JsonConvert.SerializeObject(DB.WorkforcePerOneHundreds.Include(wpoh => wpoh.Needs).ToList());
            }
        }
    }
}
