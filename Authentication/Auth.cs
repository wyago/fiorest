﻿//#define DISABLE_AUTH

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;

using Nancy;
using Nancy.ErrorHandling;
using Nancy.Extensions;

using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Authentication
{
    public static class Auth
    {
        private static TimeSpan cacheExpiry = TimeSpan.FromHours(2);
        private static MemoryCache apiKeyCache = new MemoryCache(new MemoryCacheOptions());
        private static MemoryCache authTokenCache = new MemoryCache(new MemoryCacheOptions());
        private static MemoryCache permissionCache = new MemoryCache(new MemoryCacheOptions());

        public static bool IsWriteAuthenticated(this Request request)
		{
            return request.EnforceAuthInner(RequireAdmin: false, RequireWrite: true) == null;
		}

        public static bool IsReadAuthenticated(this Request request)
        {
            return request.EnforceAuthInner(RequireAdmin: false, RequireWrite: true) == null;
        }

        public static string GetUserName(this Request request)
        {
            string AuthorizationStr = GetAuthToken(request);
            if (AuthorizationStr == null)
            {
                return null;
            }

            Guid AuthorizationGuid;
            if (Guid.TryParse(AuthorizationStr, out AuthorizationGuid))
            {
                AuthenticationModel result = null;

                // Check the caches first
                APIKey apires = apiKeyCache.Get<APIKey>(AuthorizationGuid);
                if (apires != null)
                {
                    result = apires.AuthenticationModel;
                }
                else
                {
                    // Second cache to check.
                    var tokenres = authTokenCache.Get<AuthenticationModel>(AuthorizationGuid);
                    if (tokenres != null)
                    {
                        result = tokenres;
                    }
                    else
                    {
                        using (var DB = PRUNDataContext.GetNewContext())
                        {
                            // See if it's an APIKey first
                            var apiKey = DB.APIKeys
                                .Include(ak => ak.AuthenticationModel)
                                .Where(ak => ak.AuthAPIKey == AuthorizationGuid)
                                .FirstOrDefault();
                            if (apiKey != null)
                            {
                                result = apiKey.AuthenticationModel;
                                apiKeyCache.Set<APIKey>(AuthorizationGuid, apiKey, cacheExpiry);
                            }
                            else
                            {
                                result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == AuthorizationGuid).FirstOrDefault();
                                authTokenCache.Set<AuthenticationModel>(AuthorizationGuid, result, cacheExpiry);
                            }
                        }
                    }
                }

                if (result != null)
                {
                    return result.UserName.ToUpper();
                }
            }

            return null;
        }

        public static bool HasAuthToken(this Request request)
        {
            return request.Headers.Keys.ToList().Contains("Authorization");
        }

        public static string GetAuthToken(this Request request)
        {
            if (!HasAuthToken(request))
            {
                return null;
            }

            return request.Headers["Authorization"].ToList()[0];
        }


        private static Response EnforceAuthInner(this Request request, bool RequireAdmin, bool RequireWrite)
        {
#if DISABLE_AUTH
            return null;
#else
            bool bTriedAdminAndFailed = false;
            bool bContainsAutorization = HasAuthToken(request);
            if (bContainsAutorization)
            {
                DateTime now = DateTime.Now.ToUniversalTime();

                Guid AuthorizationGuid;
                if (Guid.TryParse(GetAuthToken(request), out AuthorizationGuid))
                {
                    AuthenticationModel result = null;
                    using (var DB = PRUNDataContext.GetNewContext())
                    {
                        // Try the cache first.
                        APIKey apires = apiKeyCache.Get<APIKey>(AuthorizationGuid);
                        if (apires != null && (RequireWrite && apires.AllowWrites))
                        {
                            result = apires.AuthenticationModel;
                        }
                        else
                        {
                            var tokenres = authTokenCache.Get<AuthenticationModel>(AuthorizationGuid);
                            if (tokenres != null)
                            {
                                result = tokenres;
                            }
                            else
                            {
                                // See if it's an APIKey first
                                var apiKey = DB.APIKeys
                                    .Include(ak => ak.AuthenticationModel)
                                    .Where(ak => ak.AuthAPIKey == AuthorizationGuid && (!RequireWrite || ak.AllowWrites))
                                    .FirstOrDefault();
                                if (apiKey != null)
                                {
                                    result = apiKey.AuthenticationModel;

                                    DB.Database.BeginTransaction();
                                    apiKey.LastAccessTime = DateTime.UtcNow;
                                    DB.SaveChanges();
                                    DB.Database.CommitTransaction();
                                    apiKeyCache.Set<APIKey>(AuthorizationGuid, apiKey, cacheExpiry);
                                }
                                else
                                {
                                    result = DB.AuthenticationModels
                                        .Where(a => a.AuthorizationKey == AuthorizationGuid && now < a.AuthorizationExpiry)
                                        .FirstOrDefault();
                                    authTokenCache.Set<AuthenticationModel>(AuthorizationGuid, result, cacheExpiry);
                                }
                            }
                        }

                        bTriedAdminAndFailed = (RequireAdmin && result != null && !result.IsAdministrator);
                    }

                    if (!bTriedAdminAndFailed && result != null)
                    {
                        // Success
                        return null;
                    }
                }
            }

            string message = "You are not logged in or your ticket expired.";
            if (bTriedAdminAndFailed)
            {
                message = "You are not an administrator.";
            }

            Response resp = message;
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.Unauthorized;
            return resp;
#endif
        }

        public enum PrivacyType
        {
            Flight,
            Building,
            Storage,
            Production,
            Workforce,
            Experts,
            Contracts,
            ShipmentTracking
        }

        private static bool CheckPrivacyType(PermissionAllowance permissionAllowance, PrivacyType privacyType)
        {
            if (permissionAllowance != null)
            {
                switch (privacyType)
                {
                    case PrivacyType.Flight:
                        return permissionAllowance.FlightData;

                    case PrivacyType.Building:
                        return permissionAllowance.BuildingData;

                    case PrivacyType.Storage:
                        return permissionAllowance.StorageData;

                    case PrivacyType.Production:
                        return permissionAllowance.ProductionData;

                    case PrivacyType.Workforce:
                        return permissionAllowance.WorkforceData;

                    case PrivacyType.Experts:
                        return permissionAllowance.ExpertsData;

                    case PrivacyType.Contracts:
                        return permissionAllowance.ContractData;

                    case PrivacyType.ShipmentTracking:
                        return permissionAllowance.ShipmentTrackingData;
                }
            }

            return false;
        }

        public static bool CanSeeData(string RequesterUserName, string AccessUserName, PrivacyType privacyType)
        {
            if (RequesterUserName == null)
            {
                return false;
            }

            RequesterUserName = RequesterUserName.ToUpper();
            AccessUserName = AccessUserName.ToUpper();

#if DISABLE_AUTH
            return true;
#else
            if (RequesterUserName == AccessUserName)
            {
                // Everyone has permission to see their own data
                return true;
            }

            // Check the permissions cache first.
            AuthenticationModel accessModel = permissionCache.Get<AuthenticationModel>(AccessUserName);
            if (accessModel == null)
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    // find result & cache it.
                    accessModel = DB.AuthenticationModels
                        .Where(e => e.UserName.ToUpper() == AccessUserName)
                        .Include(am => am.Allowances)
                        .FirstOrDefault();
                    permissionCache.Set<AuthenticationModel>(AccessUserName, accessModel, cacheExpiry);
                }
            }
            // if there are permissions, process them.
            if (accessModel != null)
            {
                // First check for "any user" entries
                PermissionAllowance permissionAllowance = accessModel.Allowances.Where(a => a.UserName == "*").FirstOrDefault();
                if (CheckPrivacyType(permissionAllowance, privacyType))
                {
                    return true;
                }
                permissionAllowance = accessModel.Allowances.Where(a => a.UserName.ToUpper() == RequesterUserName).FirstOrDefault();
                if (CheckPrivacyType(permissionAllowance, privacyType))
                {
                    return true;
                }
            }
            return false;
#endif
        }

        public static void ResetPermissionCache(string Name)
        {
            permissionCache.Remove(Name.ToUpper());
        }

        public static void EnforceAuthAdmin(this NancyModule module)
        {
            if (module.RouteExecuting())
            {
                var result = module.Context.Request.EnforceAuthInner(RequireAdmin: true, RequireWrite: true);
                if (result != null)
                {
                    throw new RouteExecutionEarlyExitException(result);
                }
            }
            else
            {
                module.Before += ctx =>
                {
                    return ctx.Request.EnforceAuthInner(RequireAdmin: true, RequireWrite: true);
                };
            }
        }

        private static void EnforceAuthInternal(this NancyModule module, bool RequireWrite)
		{
            if (module.RouteExecuting())
            {
                var result = module.Context.Request.EnforceAuthInner(RequireAdmin: false, RequireWrite: RequireWrite);
                if (result != null)
                {
                    throw new RouteExecutionEarlyExitException(result);
                }
            }
            else
            {
                module.Before += ctx =>
                {
                    return ctx.Request.EnforceAuthInner(RequireAdmin: false, RequireWrite: RequireWrite);
                };
            }
        }

        public static void EnforceWriteAuth(this NancyModule module)
		{
            EnforceAuthInternal(module, RequireWrite: true);
		}

        public static void EnforceReadAuth(this NancyModule module)
        {
            EnforceAuthInternal(module, RequireWrite: false);
        }
    }
}
