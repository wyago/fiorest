﻿namespace FIORest.JSONRepresentations.SystemStar
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string naturalId { get; set; }
        public string name { get; set; }
        public object namer { get; set; }
        public object namingDate { get; set; }
        public bool nameable { get; set; }
        public Star star { get; set; }
        public Planet[] planets { get; set; }
        public object[] celestialBodies { get; set; }
        public string[] connections { get; set; }
        public Address address { get; set; }
        public object country { get; set; }
        public object currency { get; set; }
        public string id { get; set; }
    }

    public class Star
    {
        public string type { get; set; }
        public float luminosity { get; set; }
        public Position position { get; set; }
        public string sectorId { get; set; }
        public string subSectorId { get; set; }
        public float mass { get; set; }
        public float massSol { get; set; }
    }

    public class Position
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Planet
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public Orbit orbit { get; set; }
        public float mass { get; set; }
        public float massEarth { get; set; }
        public bool surface { get; set; }
        public Address1 address { get; set; }
    }

    public class Orbit
    {
        public long semiMajorAxis { get; set; }
        public float eccentricity { get; set; }
        public float inclination { get; set; }
        public int rightAscension { get; set; }
        public int periapsis { get; set; }
    }

    public class Address1
    {
        public Line1[] lines { get; set; }
    }

    public class Line1
    {
        public Entity1 entity { get; set; }
        public string type { get; set; }
    }

    public class Entity1
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

}
