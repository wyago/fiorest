﻿namespace FIORest.JSONRepresentations.LocalMarket
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body[] body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string localMarketId { get; set; }
        public int naturalId { get; set; }
        public string status { get; set; }
        public Creator creator { get; set; }
        public string type { get; set; }
        public Address address { get; set; }
        public Quantity quantity { get; set; }
        public Price price { get; set; }
        public int advice { get; set; }
        public Creationtime creationTime { get; set; }
        public string minimumRating { get; set; }
        public Expiry expiry { get; set; }
        public string id { get; set; }
        public Origin origin { get; set; }
        public Destination destination { get; set; }
        public float cargoWeight { get; set; }
        public float cargoVolume { get; set; }
    }

    public class Creator
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Quantity
    {
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Price
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Creationtime
    {
        public long timestamp { get; set; }
    }

    public class Expiry
    {
        public long timestamp { get; set; }
    }

    public class Origin
    {
        public Line1[] lines { get; set; }
    }

    public class Line1
    {
        public Entity1 entity { get; set; }
        public string type { get; set; }
    }

    public class Entity1
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Destination
    {
        public Line2[] lines { get; set; }
    }

    public class Line2
    {
        public Entity2 entity { get; set; }
        public string type { get; set; }
    }

    public class Entity2
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

}
