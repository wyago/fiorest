﻿namespace FIORest.JSONRepresentations.COGCData
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public Planet planet { get; set; }
        public Address address { get; set; }
        public Completiondate completionDate { get; set; }
        public string status { get; set; }
        public Program[] programs { get; set; }
        public Vote[] votes { get; set; }
        public string[] programTypes { get; set; }
        public Upkeep upkeep { get; set; }
        public string id { get; set; }
    }

    public class Planet
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Completiondate
    {
        public long timestamp { get; set; }
    }

    public class Upkeep
    {
        public Billofmaterial[] billOfMaterial { get; set; }
        public Contribution[] contributions { get; set; }
        public Duedate dueDate { get; set; }
    }

    public class Duedate
    {
        public long timestamp { get; set; }
    }

    public class Billofmaterial
    {
        public Material material { get; set; }
        public int amount { get; set; }
        public int currentAmount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Contribution
    {
        public Material1[] materials { get; set; }
        public Time time { get; set; }
        public Contributor contributor { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }

    public class Contributor
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Material1
    {
        public Material2 material { get; set; }
        public int amount { get; set; }
    }

    public class Material2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Program
    {
        public string type { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
    }

    public class Start
    {
        public long timestamp { get; set; }
    }

    public class End
    {
        public long timestamp { get; set; }
    }

    public class Vote
    {
        public Voter voter { get; set; }
        public float influence { get; set; }
        public string type { get; set; }
        public Time1 time { get; set; }
    }

    public class Voter
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Time1
    {
        public long timestamp { get; set; }
    }
}
