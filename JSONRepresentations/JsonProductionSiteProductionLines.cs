﻿namespace FIORest.JSONRepresentations.ProductionSiteProductionLines
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string siteId { get; set; }
        public Productionline[] productionLines { get; set; }
    }

    public class Productionline
    {
        public string id { get; set; }
        public string siteId { get; set; }
        public Address address { get; set; }
        public string type { get; set; }
        public int capacity { get; set; }
        public int slots { get; set; }
        public float efficiency { get; set; }
        public float condition { get; set; }
        public Workforce[] workforces { get; set; }
        public Order[] orders { get; set; }
        public Productiontemplate[] productionTemplates { get; set; }
        public Efficiencyfactor[] efficiencyFactors { get; set; }
    }

    public class Address
    {
        public Line[] lines { get; set; }
    }

    public class Line
    {
        public Entity entity { get; set; }
        public string type { get; set; }
    }

    public class Entity
    {
        public string id { get; set; }
        public string naturalId { get; set; }
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Workforce
    {
        public string level { get; set; }
        public float efficiency { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public string productionLineId { get; set; }
        public Input[] inputs { get; set; }
        public Output[] outputs { get; set; }
        public Created created { get; set; }
        public Started started { get; set; }
        public Completion completion { get; set; }
        public Duration duration { get; set; }
        public Lastupdated lastUpdated { get; set; }
        public float completed { get; set; }
        public bool halted { get; set; }
        public bool recurring { get; set; }
        public Productionfee productionFee { get; set; }
        public Productionfeecollector productionFeeCollector { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }

    public class Started
    {
        public long timestamp { get; set; }
    }

    public class Completion
    {
        public long timestamp { get; set; }
    }

    public class Duration
    {
        public long millis { get; set; }
    }

    public class Lastupdated
    {
        public long timestamp { get; set; }
    }

    public class Productionfee
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Productionfeecollector
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Input
    {
        public Value value { get; set; }
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Value
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Output
    {
        public Value1 value { get; set; }
        public Material1 material { get; set; }
        public int amount { get; set; }
    }

    public class Value1
    {
        public int amount { get; set; }
        public string currency { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Productiontemplate
    {
        public string id { get; set; }
        public string name { get; set; }
        public Inputfactor[] inputFactors { get; set; }
        public Outputfactor[] outputFactors { get; set; }
        public float effortFactor { get; set; }
        public float efficiency { get; set; }
        public Duration1 duration { get; set; }
        public Productionfeefactor productionFeeFactor { get; set; }
        public Productionfeecollector1 productionFeeCollector { get; set; }
    }

    public class Duration1
    {
        public long millis { get; set; }
    }

    public class Productionfeefactor
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Productionfeecollector1
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Inputfactor
    {
        public Material2 material { get; set; }
        public int factor { get; set; }
    }

    public class Material2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Outputfactor
    {
        public Material3 material { get; set; }
        public int factor { get; set; }
    }

    public class Material3
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Efficiencyfactor
    {
        public string expertiseCategory { get; set; }
        public string type { get; set; }
        public float effectivity { get; set; }
        public float value { get; set; }
    }
}
