﻿namespace FIORest.JSONRepresentations.ProductionOrderUpdated
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public string productionLineId { get; set; }
        public Input[] inputs { get; set; }
        public Output[] outputs { get; set; }
        public Created created { get; set; }
        public Started started { get; set; }
        public Completion completion { get; set; }
        public Duration duration { get; set; }
        public Lastupdated lastUpdated { get; set; }
        public float completed { get; set; }
        public bool halted { get; set; }
        public Productionfee productionFee { get; set; }
        public Productionfeecollector productionFeeCollector { get; set; }
        public bool recurring { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }

    public class Started
    {
        public long timestamp { get; set; }
    }

    public class Completion
    {
        public long timestamp { get; set; }
    }

    public class Duration
    {
        public long millis { get; set; }
    }

    public class Lastupdated
    {
        public long timestamp { get; set; }
    }

    public class Productionfee
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Productionfeecollector
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Input
    {
        public Value value { get; set; }
        public Material material { get; set; }
        public int amount { get; set; }
    }

    public class Value
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Output
    {
        public Value1 value { get; set; }
        public Material1 material { get; set; }
        public int amount { get; set; }
    }

    public class Value1
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }
}
