﻿namespace FIORest
{
    public class Options
    {
        public int VerbosityLevel = 0;
        public int Port = 4443;
        public string BadRequestPath = "./BadRequests";

        public Options.DBTypes DatabaseType = Options.DBTypes.Sqlite;
        public string DatabaseTypeStr = "sqlite";
        // FilePath used for sqlite databases
        public string DatabaseFilePath = "./prundata.db";
        // Host, User, Pass, Name used for postgresql databases
        public string DatabaseHost = "127.0.0.1";
        public string DatabaseUser = "fio";
        public string DatabasePass = "fio";
        public string DatabaseName = "fiorest";

        public bool DebugDatabase = false;

        public string UpdateDirectory = "FIOUIRelease";
        public bool ShouldApplyMigration = false;

        public enum DBTypes
        {
            Postgres,
            Sqlite
        }
    }
}
