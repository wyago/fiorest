﻿using FIORest.Database;

namespace FIORest
{
    public static class Globals
    {
        public static Options Opts = new Options();

        public static string ConnectionString
        {
            get
            {
                switch(Opts.DatabaseType)
                {
                    case Options.DBTypes.Postgres:
                        var addl = "";
                        if (Globals.Opts.DebugDatabase)
                        {
                            addl = ";Include Error Detail=true";
                        }
                        return $"Host={Globals.Opts.DatabaseHost};Database={Globals.Opts.DatabaseName};Username={Globals.Opts.DatabaseUser};Password={Globals.Opts.DatabasePass}{addl}";
                    case Options.DBTypes.Sqlite:
                    default:
                        return $"Data Source={Globals.Opts.DatabaseFilePath}";
                }
            }
        }
    }
}
