﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class WarehouseModel
    {
        [JsonIgnore]
        public int WarehouseModelId { get; set; }

        [StringLength(32)]
        public string WarehouseId { get; set; }
        [StringLength(32)]
        public string StoreId { get; set; }
        public int Units { get; set; }
        public double WeightCapacity { get; set; }
        public double VolumeCapacity { get; set; }
        public long NextPaymentTimestampEpochMs { get; set; }

        public double FeeAmount { get; set; }
        [StringLength(8)]
        public string FeeCurrency { get; set; }

        [StringLength(32)]
        public string FeeCollectorId { get; set; }
        [StringLength(64)]
        public string FeeCollectorName { get; set; }
        [StringLength(8)]
        public string FeeCollectorCode { get; set; }

        [StringLength(64)]
        public string LocationName { get; set; }
        [StringLength(32)]
        public string LocationNaturalId { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
