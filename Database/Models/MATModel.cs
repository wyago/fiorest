﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class MATModel
    {
        [JsonIgnore]
        public int MATModelId { get; set; }

        [StringLength(32)]
        public string CategoryName { get; set; }
        [StringLength(32)]
        public string CategoryId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }
        [StringLength(32)]
        public string MatId { get; set; }
        [StringLength(8)]
        public string Ticker { get; set; }

        public double Weight { get; set; }
        public double Volume { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as MATModel;
            if (other != null)
            {
                return CategoryName == other.CategoryName &&
                       CategoryId == other.CategoryId &&
                       Name == other.Name &&
                       MatId == other.MatId &&
                       Ticker == other.Ticker &&
                       Weight == other.Weight &&
                       Volume == other.Volume;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CategoryName, CategoryId, Name, MatId, Ticker, Weight, Volume);
        }
    }
}
