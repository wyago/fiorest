﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class BUIModel
    {
        public BUIModel() { }

        [JsonIgnore]
        public int BUIModelId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }
        [StringLength(8)]
        public string Ticker { get; set; }

        [StringLength(32)]
        public string Expertise { get; set; }

        public int Pioneers { get; set; }
        public int Settlers { get; set; }
        public int Technicians { get; set; }
        public int Engineers { get; set; }
        public int Scientists { get; set; }

        public int AreaCost { get; set; }

        public virtual List<BUIBuildingCost> BuildingCosts { get; set; } = new List<BUIBuildingCost>();
        public virtual List<BUIRecipe> Recipes { get; set; } = new List<BUIRecipe>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BUIModel;
            if (other != null)
            {
                bool BuildingsTheSame = BuildingCosts.OrderBy(bc => bc.CommodityTicker).SequenceEqual(other.BuildingCosts.OrderBy(bc => bc.CommodityTicker));

                var ThisRecipes = Recipes.OrderBy(r => r.RecipeName).ToList();
                var OtherRecipes = other.Recipes.OrderBy(r => r.RecipeName).ToList();
                bool RecipesTheSame = ThisRecipes.SequenceEqual(OtherRecipes);

                bool bRes = Name == other.Name &&
                            Ticker == other.Ticker &&
                            Expertise == other.Expertise &&
                            Pioneers == other.Pioneers &&
                            Settlers == other.Settlers &&
                            Technicians == other.Technicians &&
                            Engineers == other.Engineers &&
                            Scientists == other.Scientists &&
                            AreaCost == other.AreaCost &&
                            BuildingsTheSame &&
                            RecipesTheSame;
                return bRes;
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();
            hashCode.Add(Name);
            hashCode.Add(Ticker);
            hashCode.Add(Expertise);
            hashCode.Add(Pioneers);
            hashCode.Add(Settlers);
            hashCode.Add(Technicians);
            hashCode.Add(Engineers);
            hashCode.Add(Scientists);
            hashCode.Add(AreaCost);
            foreach (var buildingCost in BuildingCosts)
            {
                hashCode.Add(buildingCost);
            }
            foreach (var recipe in Recipes)
            {
                hashCode.Add(recipe);
            }

            return hashCode.ToHashCode();
        }
    }

    public class BUIBuildingCost
    {
        public BUIBuildingCost() { }

        [JsonIgnore]
        public int BUIBuildingCostId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIModelId { get; set; }

        [JsonIgnore]
        public virtual BUIModel BuidModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BUIBuildingCost;
            if (other != null)
            {
                bool bRes = CommodityName == other.CommodityName &&
                            CommodityTicker == other.CommodityTicker &&
                            Weight.AboutEquals(other.Weight) &&
                            Volume.AboutEquals(other.Volume) &&
                            Amount == other.Amount;
                return bRes;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CommodityName, CommodityTicker, Weight, Volume, Amount);
        }
    }

    public class BUIRecipeInput
    {
        public BUIRecipeInput() { }

        [JsonIgnore]
        public int BUIRecipeInputId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }
        [JsonIgnore]
        public virtual BUIRecipe BUIRecipe { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BUIRecipeInput;
            if (other != null)
            {
                bool bRes = CommodityName == other.CommodityName &&
                            CommodityTicker == other.CommodityTicker &&
                            Weight.AboutEquals(other.Weight) &&
                            Volume.AboutEquals(other.Volume) &&
                            Amount == other.Amount;
                return bRes;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CommodityName, CommodityTicker, Weight, Volume, Amount);
        }
    }

    public class BUIRecipeOutput
    {
        public BUIRecipeOutput() { }

        [JsonIgnore]
        public int BUIRecipeOutputId { get; set; }

        [StringLength(64)]
        public string CommodityName { get; set; }
        [StringLength(8)]
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }
        [JsonIgnore]
        public virtual BUIRecipe BUIRecipe { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BUIRecipeOutput;
            if (other != null)
            {
                bool bRes = CommodityName == other.CommodityName &&
                            CommodityTicker == other.CommodityTicker &&
                            Weight.AboutEquals(other.Weight) &&
                            Volume.AboutEquals(other.Volume) &&
                            Amount == other.Amount;
                return bRes;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CommodityName, CommodityTicker, Weight, Volume, Amount);
        }
    }

    public class BUIRecipe
    {
        public BUIRecipe() { }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }

        public virtual List<BUIRecipeInput> Inputs { get; set; } = new List<BUIRecipeInput>();
        public virtual List<BUIRecipeOutput> Outputs { get; set; } = new List<BUIRecipeOutput>();
        public int DurationMs { get; set; }
        [StringLength(64)]
        public string RecipeName { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BUIRecipe;
            if (other != null)
            {
                bool bRes = Inputs.OrderBy(i => i.CommodityTicker).SequenceEqual(other.Inputs.OrderBy(i => i.CommodityTicker)) &&
                            Outputs.OrderBy(o => o.CommodityTicker).SequenceEqual(other.Outputs.OrderBy(o => o.CommodityTicker)) &&
                            DurationMs == other.DurationMs &&
                            RecipeName == other.RecipeName;
                return bRes;
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();
            foreach(var input in Inputs)
            {
                hashCode.Add(input);
            }
            foreach(var output in Outputs)
            {
                hashCode.Add(output);
            }
            hashCode.Add(DurationMs);
            hashCode.Add(RecipeName);
            return hashCode.ToHashCode();
        }

        [JsonIgnore]
        public int BUIModelId { get; set; }
        [JsonIgnore]
        public virtual BUIModel BUIModel { get; set; }
    }
}
