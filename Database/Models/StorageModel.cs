﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class StorageModel
    {
        [JsonIgnore]
        public int StorageModelId { get; set; }

        [StringLength(32)]
        public string StorageId { get; set; }
        [StringLength(32)]
        public string AddressableId { get; set; }

        [StringLength(32)]
        public string Name { get; set; }
        public double WeightLoad { get; set; }
        public double WeightCapacity { get; set; }
        public double VolumeLoad { get; set; }
        public double VolumeCapacity { get; set; }

        public virtual List<StorageItem> StorageItems { get; set; } = new List<StorageItem>();

        public bool FixedStore { get; set; }
        [StringLength(16)]
        public string Type { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class StorageItem
    {
        [JsonIgnore]
        public int StorageItemId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public float MaterialValue { get; set; }
        [StringLength(8)]
        public string MaterialValueCurrency { get; set; }

        [StringLength(16)]
        public string Type { get; set; }

        public double TotalWeight { get; set; }
        public double TotalVolume { get; set; }

        [JsonIgnore]
        public int StorageModelId { get; set; }
        [JsonIgnore]
        public virtual StorageModel StorageModel { get; set; }
    }
}
