﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
	[Index(nameof(BuildingTicker), nameof(DaysSinceLastRepair), nameof(HasBeenRepaired), IsUnique = true)]
	public class BuildingDegradationModel
	{
		public BuildingDegradationModel()
		{

		}

		public BuildingDegradationModel(SITESBuilding building)
		{
			bool bRepaired = (building.BuildingLastRepair != null);
			long buildingAgeMs = Utils.GetCurrentEpochMs() - (bRepaired ? (long)building.BuildingLastRepair : building.BuildingCreated);
			TimeSpan buildingAgeTimeSpan = TimeSpan.FromMilliseconds(buildingAgeMs);
			double buildingAgeDays = buildingAgeTimeSpan.TotalDays.RoundToDecimalPlaces(3);

			BuildingTicker = building.BuildingTicker;
			BuildingCreated = building.BuildingCreated;
			HasBeenRepaired = bRepaired;
			Condition = building.Condition;
			DaysSinceLastRepair = buildingAgeDays;

			if (building.ReclaimableMaterials != null)
			{
				foreach (var reclaimableMaterial in building.ReclaimableMaterials)
				{
					var bdrmm = new BuildingDegradationReclaimableMaterialModel();
					bdrmm.MaterialTicker = reclaimableMaterial.MaterialTicker;
					bdrmm.MaterialCount = reclaimableMaterial.MaterialAmount;

					ReclaimableMaterials.Add(bdrmm);
				}
			}

			if (building.RepairMaterials != null)
			{
				foreach (var repairMaterial in building.RepairMaterials)
				{
					var bdrmm = new BuildingDegradationRepairMaterialModel();
					bdrmm.MaterialTicker = repairMaterial.MaterialTicker;
					bdrmm.MaterialCount = repairMaterial.MaterialAmount;

					RepairMaterials.Add(bdrmm);
				}
			}
		}

		[JsonIgnore]
		public int BuildingDegradationModelId { get; set; }

		[StringLength(8)]
		public string BuildingTicker { get; set; }
		public long BuildingCreated { get; set; }
		public bool HasBeenRepaired { get; set; }

		public double Condition { get; set; }
		public double DaysSinceLastRepair { get; set; }

		public virtual List<BuildingDegradationReclaimableMaterialModel> ReclaimableMaterials { get; set; } = new List<BuildingDegradationReclaimableMaterialModel>();
		public virtual List<BuildingDegradationRepairMaterialModel> RepairMaterials { get; set; } = new List<BuildingDegradationRepairMaterialModel>();
	}

	[Index(nameof(BuildingDegradationModelId), nameof(MaterialTicker), IsUnique = true)]
	public class BuildingDegradationReclaimableMaterialModel
	{
		[JsonIgnore]
		public int BuildingDegradationReclaimableMaterialModelId { get; set; }

		[StringLength(8)]
		public string MaterialTicker { get; set; }
		public int MaterialCount { get; set; }

		[JsonIgnore]
		public int BuildingDegradationModelId { get; set; }

		[JsonIgnore]
		public virtual BuildingDegradationModel BuildingDegradationModel { get; set; }
	}

	[Index(nameof(BuildingDegradationModelId), nameof(MaterialTicker), IsUnique = true)]
	public class BuildingDegradationRepairMaterialModel
	{
		[JsonIgnore]
		public int BuildingDegradationRepairMaterialModelId { get; set; }

		[StringLength(8)]
		public string MaterialTicker { get; set; }
		public int MaterialCount { get; set; }

		[JsonIgnore]
		public int BuildingDegradationModelId { get; set; }

		[JsonIgnore]
		public virtual BuildingDegradationModel BuildingDegradationModel { get; set; }
	}
}
