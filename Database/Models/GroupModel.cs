﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
	public class GroupModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		[Key]
		public int GroupModelId { get; set; }

		[StringLength(32)]
		public string GroupOwner { get; set; }

		[StringLength(32)]
		public string GroupName { get; set; }

		public virtual List<GroupUserEntryModel> GroupUsers { get; set; } = new List<GroupUserEntryModel>();
	}

	public class GroupUserEntryModel
	{
		[JsonIgnore]
		public int GroupUserEntryModelId { get; set; }

		[StringLength(32)]
		public string GroupUserName { get; set; }

		[JsonIgnore]
		public int GroupModelId { get; set; }

		[JsonIgnore]
		public virtual GroupModel GroupModel { get; set; }
	}
}
