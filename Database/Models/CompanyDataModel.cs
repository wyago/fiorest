﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    [Index(nameof(UserName))]
    [Index(nameof(CompanyId), IsUnique = true)]
    public class CompanyDataModel
    {
        [JsonIgnore]
        public int CompanyDataModelId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(32)]
        public string HighestTier { get; set; }

        public bool Pioneer { get; set; }
        public bool Team { get; set; }

        public long CreatedEpochMs { get; set; }

        [StringLength(32)]
        public string CompanyId { get; set; }
        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }

        [StringLength(4)]
        public string CurrencyCode { get; set; }

        [StringLength(32)]
        public string StartingProfile { get; set; }

        [StringLength(64)]
        public string StartingLocation { get; set; }

        public virtual List<CompanyDataCurrencyBalance> Balances { get; set; } = new List<CompanyDataCurrencyBalance>();

        [StringLength(8)]
        public string OverallRating { get; set; }
        [StringLength(8)]
        public string ActivityRating { get; set; }
        [StringLength(8)]
        public string ReliabilityRating { get; set; }
        [StringLength(8)]
        public string StabilityRating { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class CompanyDataCurrencyBalance
    {
        [JsonIgnore]
        public int CompanyDataCurrencyBalanceId { get; set; }

        [StringLength(8)]
        public string Currency { get; set; }

        public double Balance { get; set; }

        [JsonIgnore]
        public int CompanyDataModelId { get; set; }

        [JsonIgnore]
        public virtual CompanyDataModel CompanyDataModel { get; set; }
    }
}
