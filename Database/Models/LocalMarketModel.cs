﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class LocalMarketModel
    {
        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        public string MarketId { get; set; }

        public virtual List<ShippingAd> ShippingAds { get; set; } = new List<ShippingAd>();
        public virtual List<BuyingAd> BuyingAds { get; set; } = new List<BuyingAd>();
        public virtual List<SellingAd> SellingAds { get; set; } = new List<SellingAd>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ShippingAd
    {
        [JsonIgnore]
        public int ShippingAdId { get; set; }

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string OriginPlanetId { get; set; }
        [StringLength(8)]
        public string OriginPlanetNaturalId { get; set; }
        [StringLength(32)]
        public string OriginPlanetName { get; set; }

        [StringLength(32)]
        public string DestinationPlanetId { get; set; }
        [StringLength(8)]
        public string DestinationPlanetNaturalId { get; set; }
        [StringLength(32)]
        public string DestinationPlanetName { get; set; }

        public double CargoWeight { get; set; }
        public double CargoVolume { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        public double PayoutPrice { get; set; }
        [StringLength(8)]
        public string PayoutCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }

    public class BuyingAd
    {
        [JsonIgnore]
        public int BuyingAdId { get; set; }

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }
        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }

    public class SellingAd
    {
        [JsonIgnore]
        public int SellingAdId { get; set; }

        public int ContractNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string CreatorCompanyId { get; set; }
        [StringLength(64)]
        public string CreatorCompanyName { get; set; }
        [StringLength(8)]
        public string CreatorCompanyCode { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }
        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        [StringLength(8)]
        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }
}
