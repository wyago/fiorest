﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class AuthenticationModel
    {
        public int AuthenticationModelId { get; set; }

        public bool AccountEnabled { get; set; }
        [StringLength(32)]
        public string DisabledReason { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(128)]
        public string PasswordHash { get; set; }

        public bool IsAdministrator { get; set; }

        public virtual List<PermissionAllowance> Allowances { get; set; } = new List<PermissionAllowance>();
        public virtual List<FailedLoginAttempt> FailedAttempts { get; set; } = new List<FailedLoginAttempt>();

        public virtual List<APIKey> APIKeys { get; set; } = new List<APIKey>();

        public Guid AuthorizationKey { get; set; }
        public DateTime AuthorizationExpiry { get; set; }
    }

    public class PermissionAllowance
    {
        [JsonIgnore]
        public int PermissionAllowanceId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }
        public bool ContractData { get; set; }
        public bool ShipmentTrackingData { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class FailedLoginAttempt
    {
        [JsonIgnore]
        public int FailedLoginAttemptId { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [StringLength(32)]
        public string Address { get; set; }

        public DateTime FailedAttemptDateTime { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class APIKey
    {
        [JsonIgnore]
        public int APIKeyId { get; set; }

        public Guid AuthAPIKey { get; set; }

        [StringLength(255)]
        public string Application { get; set; }

        public bool AllowWrites { get; set; }

        public DateTime LastAccessTime { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class Registration
    {
        [JsonIgnore]
        public int RegistrationId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(64)]
        public string RegistrationGuid { get; set; }

        public DateTime RegistrationTime { get; set; }
    }
}
