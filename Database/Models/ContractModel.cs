﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ContractModel
    {
        [JsonIgnore]
        public int ContractModelId { get; set; }

        [StringLength(32)]
        public string ContractId { get; set; }
        [StringLength(32)]
        public string ContractLocalId { get; set; }

        public long? DateEpochMs { get; set; }
        public long? ExtensionDeadlineEpochMs { get; set; }
        public long? DueDateEpochMs { get; set; }

        public bool CanExtend { get; set; }

        [StringLength(32)]
        public string Party { get; set; }

        [StringLength(64)]
        public string Status { get; set; }

        [StringLength(32)]
        public string PartnerId { get; set; }
        [StringLength(64)]
        public string PartnerName { get; set; }
        [StringLength(8)]
        public string PartnerCompanyCode { get; set; }

        public virtual List<ContractCondition> Conditions { get; set; } = new List<ContractCondition>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ContractCondition
    {
        [JsonIgnore]
        public int ContractConditionId { get; set; }

        [StringLength(32)]
        public string Address { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        public int? MaterialAmount { get; set; }
        
        public double? Weight { get; set; }
        public double? Volume { get; set; }

        [StringLength(32)]
        public string BlockId { get; set; }
        [StringLength(32)]
        public string Type { get; set; }
        [StringLength(32)]
        public string ConditionId { get; set; }
        [StringLength(32)]
        public string Party { get; set; }
        public int ConditionIndex { get; set; }
        [StringLength(64)]
        public string Status { get; set; }

        public virtual List<ContractDependency> Dependencies { get; set; } = new List<ContractDependency>();

        public long? DeadlineEpochMs { get; set; }

        public double? Amount { get; set; }
        [StringLength(8)]
        public string Currency { get; set; }

        [StringLength(32)]
        public string Destination { get; set; }

        [StringLength(32)]
        public string ShipmentItemId { get; set; }

        [StringLength(32)]
        public string PickedUpMaterialId { get; set; }
        [StringLength(8)]
        public string PickedUpMaterialTicker { get; set; }
        public int? PickedUpAmount { get; set; }


        [JsonIgnore]
        public int ContractModelId { get; set; }
        [JsonIgnore]
        public virtual ContractModel ContractModel { get; set; }
    }

    public class ContractDependency
    {
        [JsonIgnore]
        public int ContractDependencyId { get; set; }

        [StringLength(32)]
        public string Dependency { get; set; }

        [JsonIgnore]
        public int ContractConditionId { get; set; }
        [JsonIgnore]
        public virtual ContractCondition ContractCondition { get; set; }
    }
}
