﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    [Microsoft.EntityFrameworkCore.Index(nameof(ExchangeCode))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialName))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialTicker))]
    [Microsoft.EntityFrameworkCore.Index(nameof(MaterialId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(ExchangeCode), nameof(MaterialId), IsUnique = true)]
    public class CXDataModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [StringLength(32)]
        public string CXDataModelId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(8)]
        public string Currency { get; set; }

        public double? Previous { get; set; }
        public double? Price { get; set; }
        public long? PriceTimeEpochMs { get; set; }

        public double? High { get; set; }
        public double? AllTimeHigh { get; set; }

        public double? Low { get; set; }
        public double? AllTimeLow { get; set; }

        public double? Ask { get; set; }
        public int? AskCount { get; set; }

        public double? Bid { get; set; }
        public int? BidCount { get; set; }

        public int? Supply { get; set; }
        public int? Demand { get; set; }
        
        public int? Traded { get; set; }

        public double? VolumeAmount { get; set; }

        public double? PriceAverage { get; set; }

        public double? NarrowPriceBandLow { get; set; }
        public double? NarrowPriceBandHigh { get; set; }

        public double? WidePriceBandLow { get; set; }
        public double? WidePriceBandHigh { get; set; }

        public double? MMBuy { get; set; }
        public double? MMSell { get; set; }

        public virtual List<CXBuyOrder> BuyingOrders { get; set; } = new List<CXBuyOrder>();
        public virtual List<CXSellOrder> SellingOrders { get; set; } = new List<CXSellOrder>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(CXDataModelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId), nameof(CXDataModelId), IsUnique = true)]
    public class CXBuyOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [StringLength(32)]
        public string OrderId { get; set; }
        [StringLength(32)]
        public string CompanyId { get; set; }
        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [JsonIgnore]
        [StringLength(32)]
        public string CXDataModelId { get; set; }
        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(CXDataModelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(OrderId), nameof(CXDataModelId), IsUnique = true)]
    public class CXSellOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [StringLength(32)]
        public string OrderId { get; set; }
        [StringLength(32)]
        public string CompanyId { get; set; }
        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [JsonIgnore]
        [StringLength(32)]
        public string CXDataModelId { get; set; }
        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }
    }

    public class CXPCData
    {
        [JsonIgnore]
        public int CXPCDataId { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        public long StartDataEpochMs { get; set; }
        public long EndDataEpochMs { get; set; }

        public virtual List<CXPCDataEntry> Entries { get; set; } = new List<CXPCDataEntry>();
    }

    public class CXPCDataEntry
    {
        [JsonIgnore]
        public int CXPCDataEntryId { get; set; }

        public long TimeEpochMs { get; set; }

        public double Open { get; set; }
        public double Close { get; set; }
        public double Volume { get; set; }
        public int Traded { get; set; }

        [JsonIgnore]
        public int CXPCDataId { get; set; }
        [JsonIgnore]
        public virtual CXPCData CXPCData { get; set; }
    }

    public class Station
    {
        [JsonIgnore]
        public int StationId { get; set; }

        [StringLength(8)]
        public string NaturalId { get; set; }
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(32)]
        public string SystemId { get; set; }
        [StringLength(8)]
        public string SystemNaturalId { get; set; }
        [StringLength(32)]
        public string SystemName { get; set; }

        public long CommisionTimeEpochMs { get; set; }

        [StringLength(32)]
        public string ComexId { get; set; }
        [StringLength(64)]
        public string ComexName { get; set; }
        [StringLength(8)]
        public string ComexCode { get; set; }

        [StringLength(32)]
        public string WarehouseId { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }
        [StringLength(8)]
        public string CountryCode { get; set; }
        [StringLength(64)]
        public string CountryName { get; set; }

        public int CurrencyNumericCode { get; set; }
        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }
        public int CurrencyDecimals { get; set; }

        [StringLength(32)]
        public string GovernorId { get; set; }
        [StringLength(32)]
        public string GovernorUserName { get; set; }

        [StringLength(32)]
        public string GovernorCorporationId { get; set; }
        [StringLength(64)]
        public string GovernorCorporationName { get; set; }
        [StringLength(8)]
        public string GovernorCorporationCode { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
