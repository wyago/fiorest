﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class SITESModel
    {
        [JsonIgnore]
        public int SITESModelId { get; set; }

        public virtual List<SITESSite> Sites { get; set; } = new List<SITESSite>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SITESSite
    {
        [JsonIgnore]
        public int SITESSiteId { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetIdentifier { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        public long PlanetFoundedEpochMs { get; set; }

        public virtual List<SITESBuilding> Buildings { get; set; } = new List<SITESBuilding>();

        [JsonIgnore]
        public int SITESModelId { get; set; }
        [JsonIgnore]
        public virtual SITESModel SITESModel { get; set; }
    }

    public class SITESBuilding
    {
        [JsonIgnore]
        public int SITESBuildingId { get; set; }

        public long BuildingCreated { get; set; }
        [StringLength(32)]
        public string BuildingId { get; set; }
        [StringLength(32)]
        public string BuildingName { get; set; }
        [StringLength(8)]
        public string BuildingTicker { get; set; }

        public long? BuildingLastRepair { get; set; }
        public double Condition { get; set; }
        public virtual List<SITESReclaimableMaterial> ReclaimableMaterials { get; set; } = new List<SITESReclaimableMaterial>();
        public virtual List<SITESRepairMaterial> RepairMaterials { get; set; } = new List<SITESRepairMaterial>();

        [JsonIgnore]
        public int SITESSiteId { get; set; }
        [JsonIgnore]
        public virtual SITESSite SITESSite { get; set; }
    }

    public class SITESReclaimableMaterial
    {
        [JsonIgnore]
        public int SITESReclaimableMaterialId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int SITESBuildingId { get; set; }
        [JsonIgnore]
        public virtual SITESBuilding SITESBuilding { get; set; }
    }

    public class SITESRepairMaterial
    {
        [JsonIgnore]
        public int SITESRepairMaterialId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int SITESBuildingId { get; set; }
        [JsonIgnore]
        public virtual SITESBuilding SITESBuilding { get; set; }
    }
}
