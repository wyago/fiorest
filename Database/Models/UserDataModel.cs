﻿using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class UserDataModel
    {
        [JsonIgnore]
        public int UserDataModelId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }
        [StringLength(16)]
        public string Tier { get; set; }

        public bool Team { get; set; }
        public bool Pioneer { get; set; }

        public int SystemNamingRights { get; set; }
        public int PlanetNamingRights { get; set; }

        public bool IsPayingUser { get; set; }
        public bool IsModeratorChat { get; set; }
        
        public long CreatedEpochMs { get; set; }

        [StringLength(32)]
        public string CompanyId { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
