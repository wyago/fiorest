﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class CXOSTradeOrdersModel
    {
        [JsonIgnore]
        public int CXOSTradeOrdersModelId { get; set; }

        public virtual List<CXOSTradeOrder> TradeOrders { get; set; } = new List<CXOSTradeOrder>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class CXOSTradeOrder
    {
        [JsonIgnore]
        public int CXOSTradeOrderId { get; set; }

        [StringLength(32)]
        public string TradeOrderId { get; set; }

        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(32)]
        public string BrokerId { get; set; }
        [StringLength(16)]
        public string OrderType { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }

        public int Amount { get; set; }
        public int InitialAmount { get; set; }

        public double Limit { get; set; }
        [StringLength(8)]
        public string LimitCurrency { get; set; }

        [StringLength(16)]
        public string Status { get; set; }
        public long CreatedEpochMs { get; set; }

        public virtual List<CXOSTrade> Trades { get; set; } = new List<CXOSTrade>();

        [JsonIgnore]
        public int CXOSTradeOrdersModelId { get; set; }
        [JsonIgnore]
        public virtual CXOSTradeOrdersModel CXOSTradeOrdersModel { get; set; }
    }

    public class CXOSTrade
    {
        [JsonIgnore]
        public int CXOSTradeId { get; set; }

        [StringLength(32)]
        public string TradeId { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        [StringLength(8)]
        public string PriceCurrency { get; set; }

        public long TradeTimeEpochMs { get; set; }

        [StringLength(32)]
        public string PartnerId { get; set; }
        [StringLength(64)]
        public string PartnerName { get; set; }
        [StringLength(8)]
        public string PartnerCode { get; set; }

        [JsonIgnore]
        public int CXOSTradeOrderId { get; set; }
        [JsonIgnore]
        public virtual CXOSTradeOrder CXOSTradeOrder { get; set; }
    }
}
