﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class SystemStarsModel
    {
        [JsonIgnore]
        public int SystemStarsModelId { get; set; }

        [StringLength(32)]
        public string SystemId { get; set; }
        [StringLength(32)]
        public string Name { get; set; }
        [StringLength(8)]
        public string NaturalId { get; set; }
        [StringLength(16)]
        public string Type { get; set; }

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        [StringLength(32)]
        public string SectorId { get; set; }
        [StringLength(32)]
        public string SubSectorId { get; set; }

        public virtual List<SystemConnection> Connections { get; set; } = new List<SystemConnection>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as SystemStarsModel;
            if (other != null)
            {
                return SystemId == other.SystemId &&
                       Name == other.Name &&
                       NaturalId == other.NaturalId &&
                       Type == other.Type &&
                       PositionX == other.PositionX &&
                       PositionY == other.PositionY &&
                       PositionZ == other.PositionZ &&
                       SectorId == other.SectorId &&
                       SubSectorId == other.SubSectorId &&
                       Connections.OrderBy(c => c.Connection).SequenceEqual(other.Connections.OrderBy(c => c.Connection));
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();
            hashCode.Add(SystemId);
            hashCode.Add(Name);
            hashCode.Add(NaturalId);
            hashCode.Add(Type);
            hashCode.Add(PositionX);
            hashCode.Add(PositionY);
            hashCode.Add(PositionZ);
            hashCode.Add(SectorId);
            hashCode.Add(SubSectorId);
            foreach(var connection in Connections)
            {
                hashCode.Add(connection);
            }

            return hashCode.ToHashCode();
        }
    }

    public class SystemConnection
    {
        [JsonIgnore]
        public int SystemConnectionId { get; set; }

        [StringLength(32)]
        public string Connection { get; set; }

        [JsonIgnore]
        public int SystemStarsModelId { get; set; }
        [JsonIgnore]
        public virtual SystemStarsModel SystemStarsModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as SystemConnection;
            if (other != null)
            {
                return Connection == other.Connection;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Connection);
        }
    }

    public class SystemStar
    {
        [JsonIgnore]
        public int SystemStarId { get; set; }

        [StringLength(32)]
        public string SystemId { get; set; }
        [StringLength(8)]
        public string SystemNaturalId { get; set; }
        [StringLength(32)]
        public string SystemName { get; set; }

        [StringLength(16)]
        public string Type { get; set; }
        public double Luminosity { get; set; }
        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        [StringLength(32)]
        public string SectorId { get; set; }
        [StringLength(32)]
        public string SubSectorId { get; set; }

        public double Mass { get; set; }
        public double MassSol { get; set; }
    }
}
