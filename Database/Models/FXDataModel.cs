using System;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class FXDataModel
    {
        [JsonIgnore]
        public int FXDataModelId { get; set; }

        [StringLength(32)]
        public string BrokerId { get; set; }

        [StringLength(8)]
        public string BaseCurrencyCode { get; set; }
        [StringLength(32)]
        public string BaseCurrencyName { get; set; }
        public int BaseCurrencyNumericCode { get; set; }

        [StringLength(8)]
        public string QuoteCurrencyCode { get; set; }
        [StringLength(32)]
        public string QuoteCurrencyName { get; set; }
        public int QuoteCurrencyNumericCode { get; set; }

        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Open { get; set; }
        public decimal Previous { get; set; }
        public decimal Traded { get; set; }
        public decimal Volume { get; set; }
        public long PriceUpdateEpochMs { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}

