﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class WorkforceModel
    {
        [JsonIgnore]
        public int WorkforceModelId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        public virtual List<WorkforceDescription> Workforces { get; set; } = new List<WorkforceDescription>();

        public DateTime? LastWorkforceUpdateTime { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class WorkforceDescription
    {
        [JsonIgnore]
        public int WorkforceDescriptionId { get; set; }

        [StringLength(16)]
        public string WorkforceTypeName { get; set; }
        public int Population { get; set; }
        public int Reserve { get; set; }
        public int Capacity { get; set; }
        public int Required { get; set; }

        public double Satisfaction { get; set; }

        public virtual List<WorkforceNeed> WorkforceNeeds { get; set; } = new List<WorkforceNeed>();

        [JsonIgnore]
        public int WorkforceModelId { get; set; }
        [JsonIgnore]
        public virtual WorkforceModel WorkforceModel { get; set; }
    }

    public class WorkforceNeed
    {
        [JsonIgnore]
        public int WorkforceNeedId { get; set; }

        [StringLength(32)]
        public string Category { get; set; }
        public bool Essential { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public double Satisfaction { get; set; }
        public double UnitsPerInterval { get; set; }
        public double UnitsPerOneHundred { get; set; }

        [JsonIgnore]
        public int WorkforceDescriptionId { get; set; }
        [JsonIgnore]
        public virtual WorkforceDescription WorkforceDescription { get; set; }
    }
}
