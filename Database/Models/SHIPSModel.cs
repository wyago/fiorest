﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class SHIPSModel
    {
        [JsonIgnore]
        public int SHIPSModelId { get; set; }

        public virtual List<SHIPSShip> Ships { get; set; } = new List<SHIPSShip>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SHIPSShip
    {
        [JsonIgnore]
        public int SHIPSShipId { get; set; }

        [StringLength(32)]
        public string ShipId { get; set; }
        [StringLength(32)]
        public string StoreId { get; set; }
        [StringLength(32)]
        public string StlFuelStoreId { get; set; }
        [StringLength(32)]
        public string FtlFuelStoreId { get; set; }
        [StringLength(32)]
        public string Registration { get; set; }
        [StringLength(32)]
        public string Name { get; set; }
        public long CommissioningTimeEpochMs { get; set; }
        [StringLength(32)]
        public string BlueprintNaturalId { get; set; }

        [StringLength(32)]
        public string FlightId { get; set; }
        public double Acceleration { get; set; }
        public double Thrust { get; set; }
        public double Mass { get; set; }
        public double OperatingEmptyMass { get; set; }
        public double ReactorPower { get; set; }
        public double EmitterPower { get; set; }
        public double Volume { get; set; }

        public double Condition { get; set; }
        public long? LastRepairEpochMs { get; set; }

        public virtual List<SHIPSRepairMaterial> RepairMaterials { get; set; } = new List<SHIPSRepairMaterial>();

        [StringLength(64)]
        public string Location { get; set; }

        public double StlFuelFlowRate { get; set; }

        [JsonIgnore]
        public int SHIPSModelId { get; set; }
        [JsonIgnore]
        public virtual SHIPSModel SHIPSModel { get; set; }
    }

    public class SHIPSRepairMaterial
    {
        [JsonIgnore]
        public int SHIPSRepairMaterialId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int SHIPSShipId { get; set; }
        [JsonIgnore]
        public virtual SHIPSShip SHIPSShip { get; set; }
    }
}
