﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class InfrastructureModel
    {
        [JsonIgnore]
        public int InfrastructureModelId { get; set; }

        [StringLength(32)]
        public string PopulationId { get; set; }

        public virtual List<InfrastructureInfo> InfrastructureInfos { get; set; } = new List<InfrastructureInfo>();
        public virtual List<InfrastructureReport> InfrastructureReports { get; set; } = new List<InfrastructureReport>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class InfrastructureInfo
    {
        [JsonIgnore]
        public int InfrastructureInfoId { get; set; }

        [StringLength(32)]
        public string Type { get; set; }
        [StringLength(8)]
        public string Ticker { get; set; }
        [StringLength(64)]
        public string Name { get; set; }
        [StringLength(32)]
        public string ProjectId { get; set; }
        public int Level { get; set; }
        public int ActiveLevel { get; set; }
        public int CurrentLevel { get; set; }
        public double UpkeepStatus { get; set; }
        public double UpgradeStatus { get; set; }

        public virtual List<InfrastructureProjectUpgradeCosts> UpgradeCosts { get; set; } = new List<InfrastructureProjectUpgradeCosts>();
        public virtual List<InfrastructureProjectUpkeeps> Upkeeps { get; set; } = new List<InfrastructureProjectUpkeeps>();
        public virtual List<InfrastructureProjectContributions> Contributions { get; set; } = new List<InfrastructureProjectContributions>();

        [JsonIgnore]
        public int InfrastructureModelId { get; set;}

        [JsonIgnore]
        public virtual InfrastructureModel InfrastructureModel { get; set; }
    }

    public class InfrastructureProjectUpgradeCosts
    {
        [JsonIgnore]
        public int InfrastructureProjectUpgradeCostsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }
        public int CurrentAmount { get; set; }

        [JsonIgnore]
        public int InfrastructureInfoId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureInfo InfrastructureInfo { get; set; }
    }

    public class InfrastructureProjectUpkeeps
    {
        [JsonIgnore]
        public int InfrastructureProjectUpkeepsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Stored { get; set; }
        public int StoreCapacity { get; set; }
        public int Duration { get; set; }
        public long NextTickTimestampEpochMs { get; set; }

        public int Amount { get; set; }
        public int CurrentAmount { get; set; }

        [JsonIgnore]
        public int InfrastructureInfoId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureInfo InfrastructureInfo { get; set; }
    }

    public class InfrastructureProjectContributions
    {
        [JsonIgnore]
        public int InfrastructureProjectContributionsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }

        public long TimestampEpochMs { get; set; }
        [StringLength(32)]
        public string CompanyId { get; set; }
        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        [JsonIgnore]
        public int InfrastructureInfoId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureInfo InfrastructureInfo { get; set; }
    }

    public class InfrastructureReport
    {
        [JsonIgnore]
        public int InfrastructureReportId { get; set; }

        public bool ExplorersGraceEnabled { get; set; }
        public int SimulationPeriod { get; set; }
        public long TimestampMs { get; set; }

        public int NextPopulationPioneer { get; set; }
        public int NextPopulationSettler { get; set; }
        public int NextPopulationTechnician { get; set; }
        public int NextPopulationEngineer { get; set; }
        public int NextPopulationScientist { get; set; }

        public int PopulationDifferencePioneer { get; set; }
        public int PopulationDifferenceSettler { get; set; }
        public int PopulationDifferenceTechnician { get; set; }
        public int PopulationDifferenceEngineer { get; set; }
        public int PopulationDifferenceScientist { get; set; }

        public float AverageHappinessPioneer { get; set; }
        public float AverageHappinessSettler { get; set; }
        public float AverageHappinessTechnician { get; set; }
        public float AverageHappinessEngineer { get; set; }
        public float AverageHappinessScientist { get; set; }

        public float UnemploymentRatePioneer { get; set; }
        public float UnemploymentRateSettler { get; set; }
        public float UnemploymentRateTechnician { get; set; }
        public float UnemploymentRateEngineer { get; set; }
        public float UnemploymentRateScientist { get; set; }

        public float OpenJobsPioneer { get; set; }
        public float OpenJobsSettler { get; set; }
        public float OpenJobsTechnician { get; set; }
        public float OpenJobsEngineer { get; set; }
        public float OpenJobsScientist { get; set; }

        public float NeedFulfillmentLifeSupport { get; set; }
        public float NeedFulfillmentSafety { get; set; }
        public float NeedFulfillmentHealth { get; set; }
        public float NeedFulfillmentComfort { get; set; }
        public float NeedFulfillmentCulture { get; set; }
        public float NeedFulfillmentEducation { get; set; }

        [JsonIgnore]
        public int InfrastructureModelId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureModel InfrastructureModel { get; set; }
    }
}
