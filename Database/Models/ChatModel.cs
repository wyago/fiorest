﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ChatModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [StringLength(32)]
        public string ChannelId { get; set; }

        public long CreationTime { get; set; }

        [StringLength(64)]
        public string DisplayName { get; set; }

        public long LastActivity { get; set; }

        [StringLength(16)]
        public string NaturalId { get; set; }

        [StringLength(8)]
        public string Type { get; set; }

        public int UserCount { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual List<ChatMessage> Messages { get; set; } = new List<ChatMessage>();
    }

    public class ChatMessage
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [StringLength(32)]
        public string MessageId { get; set; }

        [StringLength(32)]
        public string MessageType { get; set; }

        [StringLength(32)]
        public string SenderId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        public string MessageText { get; set; }
        public long MessageTimestamp { get; set; }
        public bool MessageDeleted { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        [StringLength(32)]
        public string ChatModelId { get; set; }

        [JsonIgnore]
        public virtual ChatModel ChatModel { get; set; }
    }
}
