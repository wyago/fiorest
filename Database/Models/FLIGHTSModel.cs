﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class FLIGHTSModel
    {
        [JsonIgnore]
        public int FLIGHTSModelId { get; set; }

        public virtual List<FLIGHTSFlight> Flights { get; set; } = new List<FLIGHTSFlight>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class FLIGHTSFlight
    {
        [JsonIgnore]
        public int FLIGHTSFlightId { get; set; }

        [StringLength(32)]
        public string FlightId { get; set; }
        [StringLength(32)]
        public string ShipId { get; set; }

        [StringLength(64)]
        public string Origin { get; set; }
        [StringLength(64)]
        public string Destination { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public virtual List<FLIGHTSFlightSegment> Segments { get; set; } = new List<FLIGHTSFlightSegment>();

        public int CurrentSegmentIndex { get; set; }

        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }

        public bool IsAborted { get; set; }

        [JsonIgnore]
        public int FLIGHTSModelId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSModel FLIGHTSModel { get; set; }
    }

    public class FLIGHTSFlightSegment
    {
        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        [StringLength(64)]
        public string Origin { get; set; }
        [StringLength(64)]
        public string Destination { get; set; }

        public virtual List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public virtual List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();

        [JsonIgnore]
        public int FLIGHTSFlightId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlight FLIGHTSFlight { get; set; }
    }

    public class OriginLine
    {
        [JsonIgnore]
        public int OriginLineId { get; set; }

        [StringLength(32)]
        public string Type { get; set; }
        [StringLength(32)]
        public string LineId { get; set; }
        [StringLength(16)]
        public string LineNaturalId { get; set; }
        [StringLength(64)]
        public string LineName { get; set; }

        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlightSegment FLIGHTSFlightSegment { get; set; }
    }

    public class DestinationLine
    {
        [JsonIgnore]
        public int DestinationLineId { get; set; }

        [StringLength(32)]
        public string Type { get; set; }
        [StringLength(32)]
        public string LineId { get; set; }
        [StringLength(16)]
        public string LineNaturalId { get; set; }
        [StringLength(64)]
        public string LineName { get; set; }

        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlightSegment FLIGHTSFlightSegment { get; set; }
    }
}
