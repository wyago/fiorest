﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class UserSettingsModel
    {
        [JsonIgnore]
        public int UserSettingsModelId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        public virtual List<UserSettingsBurnRate> BurnRateSettings { get; set; } = new List<UserSettingsBurnRate>();
    }

    public class UserSettingsBurnRate
    {
        [JsonIgnore]
        public int UserSettingsBurnRateId { get; set; }

        [StringLength(32)]
        public string PlanetNaturalId { get; set; }
        public virtual List<UserSettingsBurnRateExclusion> MaterialExclusions { get; set; } = new List<UserSettingsBurnRateExclusion>();

        [JsonIgnore]
        public int UserSettingsModelId { get; set; }
        [JsonIgnore]
        public virtual UserSettingsModel UserSettingsModel { get; set; }
    }

    public class UserSettingsBurnRateExclusion
    {
        [JsonIgnore]
        public int UserSettingsBurnRateExclusionId { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        [JsonIgnore]
        public int UserSettingsBurnRateId { get; set; }
        [JsonIgnore]
        public virtual UserSettingsBurnRate UserSettingsBurnRate { get; set; }
    }
}
