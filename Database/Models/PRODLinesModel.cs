﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class PRODLinesModel
    {
        [JsonIgnore]
        public int PRODLinesModelId { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        public virtual List<ProductionLine> ProductionLines { get; set; } = new List<ProductionLine>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ProductionLine
    {
        [JsonIgnore]
        public int ProductionLineId { get; set; }

        [StringLength(32)]
        public string LineId { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string Type { get; set; }
        public int Capacity { get; set; }

        public double Efficiency { get; set; }
        public double Condition { get; set; }

        public virtual List<ProductionLineOrder> Orders { get; set; } = new List<ProductionLineOrder>();

        [JsonIgnore]
        public int PRODLinesModelId { get; set; }
        [JsonIgnore]
        public virtual PRODLinesModel PRODLinesModelModel { get; set; }
    }

    public class ProductionLineOrder
    {
        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }

        [StringLength(32)]
        public string ProductionId { get; set; }

        public virtual List<ProductionLineInput> Inputs { get; set; } = new List<ProductionLineInput>();
        public virtual List<ProductionLineOutput> Outputs { get; set; } = new List<ProductionLineOutput>();

        public long? CreatedEpochMs { get; set; }
        public long? StartedEpochMs { get; set; }
        public long? CompletionEpochMs { get; set; }
        public long? DurationMs { get; set; }
        public long? LastUpdatedEpochMs { get; set; }
        public double? CompletedPercentage { get; set; }

        public bool IsHalted { get; set; }
        public bool Recurring { get; set; }

        public double ProductionFee { get; set; }
        [StringLength(8)]
        public string ProductionFeeCurrency { get; set; }

        [StringLength(32)]
        public string ProductionFeeCollectorId { get; set; }
        [StringLength(64)]
        public string ProductionFeeCollectorName { get; set; }
        [StringLength(8)]
        public string ProductionFeeCollectorCode { get; set; }

        [JsonIgnore]
        public int ProductionLineId { get; set; }
        [JsonIgnore]
        public virtual ProductionLine ProductionLine { get; set; }
    }

    public class ProductionLineInput
    {
        [JsonIgnore]
        public int ProductionLineInputId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }
        [JsonIgnore]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }
    }

    public class ProductionLineOutput
    {
        [JsonIgnore]
        public int ProductionLineOutputId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int ProductionLineOrderId { get; set; }
        [JsonIgnore]
        public virtual ProductionLineOrder ProductionLineOrder { get; set; }
    }
}
