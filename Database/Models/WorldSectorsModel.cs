﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class WorldSectorsModel
    {
        [JsonIgnore]
        public int WorldSectorsModelId { get; set; }

        [StringLength(32)]
        public string SectorId { get; set; }
        [StringLength(32)]
        public string Name { get; set; }

        public int HexQ { get; set; }
        public int HexR { get; set; }
        public int HexS { get; set; }

        public int Size { get; set; }

        public virtual List<SubSector> SubSectors { get; set; } = new List<SubSector>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as WorldSectorsModel;
            if (other != null)
            {
                return SectorId == other.SectorId &&
                       Name == other.Name &&
                       HexQ == other.HexQ &&
                       HexR == other.HexR &&
                       HexS == other.HexS &&
                       Size == other.Size &&
                       SubSectors.OrderBy(ss => ss.SSId).SequenceEqual(other.SubSectors.OrderBy(ss => ss.SSId));
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();
            hashCode.Add(SectorId);
            hashCode.Add(Name);
            hashCode.Add(HexR);
            hashCode.Add(HexQ);
            hashCode.Add(HexS);
            hashCode.Add(Size);
            foreach( var subSector in SubSectors)
            {
                hashCode.Add(subSector);
            }
            return hashCode.ToHashCode();
        }
    }

    public class SubSector
    {
        [JsonIgnore]
        public int SubSectorId { get; set; }

        [StringLength(32)]
        public string SSId { get; set; }

        public virtual List<SubSectorVertex> Vertices { get; set; } = new List<SubSectorVertex>();

        [JsonIgnore]
        public int WorldSectorsModelId { get; set; }
        [JsonIgnore]
        public virtual WorldSectorsModel WorldSectorsModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as SubSector;
            if (other != null)
            {
                return SSId == other.SSId &&
                       Vertices.OrderBy(v => v.X).ThenBy(v => v.Y).ThenBy(v => v.Z).SequenceEqual(other.Vertices.OrderBy(v => v.X).ThenBy(v => v.Y).ThenBy(v => v.Z));
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();
            hashCode.Add(SSId);
            foreach(var vertex in Vertices)
            {
                hashCode.Add(vertex);
            }
            return hashCode.ToHashCode();
        }
    }

    public class SubSectorVertex
    {
        [JsonIgnore]
        public int SubSectorVertexId { get; set; }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        [JsonIgnore]
        public int SubSectorId { get; set; }
        [JsonIgnore]
        public virtual SubSector SubSector { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as SubSectorVertex;
            if (other != null)
            {
                return X == other.X &&
                       Y == other.Y &&
                       Z == other.Z;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, Z);
        }
    }
}
