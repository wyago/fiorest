﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ComexExchange
    {
        [JsonIgnore]
        public int ComexExchangeId { get; set; }

        [StringLength(32)]
        public string ExchangeId { get; set; }
        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(32)]
        public string ExchangeOperatorId { get; set; }
        [StringLength(8)]
        public string ExchangeOperatorCode { get; set; }
        [StringLength(64)]
        public string ExchangeOperatorName { get; set; }

        public int CurrencyNumericCode { get; set; }
        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }
        public int CurrencyDecimals { get; set; }

        [StringLength(32)]
        public string LocationId { get; set; }
        [StringLength(64)]
        public string LocationName { get; set; }
        [StringLength(16)]
        public string LocationNaturalId { get; set; }
    }


    public class CountryRegistryCountry
    {
        [JsonIgnore]
        public int CountryRegistryCountryId { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }
        [StringLength(8)]
        public string CountryCode { get; set; }
        [StringLength(64)]
        public string CountryName { get; set; }

        public int CurrencyNumericCode { get; set; }
        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }
        public int CurrencyDecimals { get; set; }
    }

    public class SimulationData
    {
        [JsonIgnore]
        public int SimulationDataId { get; set; }

        public int SimulationInterval { get; set; }
        public int FlightSTLFactor { get; set; }
        public int FlightFTLFactor { get; set; }
        public int PlanetaryMotionFactor { get; set; }
        public int ParsecLength { get; set; }
    }

    public class WorkforcePerOneHundred
    {
        [JsonIgnore]
        public int WorkforcePerOneHundredId { get; set; }

        [StringLength(16)]
        public string WorkforceType { get; set; }

        public virtual List<WorkforcePerOneHundreedNeed> Needs { get; set; } = new List<WorkforcePerOneHundreedNeed>();
    }

    public class WorkforcePerOneHundreedNeed
    {
        [JsonIgnore]
        public int WorkforcePerOneHundreedNeedId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double Amount { get; set; }

        [JsonIgnore]
        public int WorkforcePerOneHundredId { get; set; }

        [JsonIgnore]
        public virtual WorkforcePerOneHundred WorkforcePerOneHundred { get; set; }
    }
}
