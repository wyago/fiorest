﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ExpertsModel
    {
        [JsonIgnore]
        public int ExpertsModelId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }

        public int AgricultureActive { get; set; }
        public int AgricultureAvailable { get; set; }
        public double AgricultureEfficiencyGain { get; set; }

        public int ResourceExtractioneActive { get; set; }
        public int ResourceExtractionAvailable { get; set; }
        public double ResourceExtractionEfficiencyGain { get; set; }

        public int FoodIndustriesActive { get; set; }
        public int FoodIndustriesAvailable { get; set; }
        public double FoodIndustriesEfficiencyGain { get; set; }

        public int ChemistryActive { get; set; }
        public int ChemistryAvailable { get; set; }
        public double ChemistryEfficiencyGain { get; set; }

        public int ConstructionActive { get; set; }
        public int ConstructionAvailable { get; set; }
        public double ConstructionEfficiencyGain { get; set; }

        public int ElectronicsActive { get; set; }
        public int ElectronicsAvailable { get; set; }
        public double ElectronicsEfficiencyGain { get; set; }

        public int FuelRefiningActive { get; set; }
        public int FuelRefiningAvailable { get; set; }
        public double FuelRefiningEfficiencyGain { get; set; }

        public int ManufacturingActive { get; set; }
        public int ManufacturingAvailable { get; set; }
        public double ManufacturingEfficiencyGain { get; set; }

        public int MetallurgyActive { get; set; }
        public int MetallurgyAvailable { get; set; }
        public double MetallurgyEfficiencyGain { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
