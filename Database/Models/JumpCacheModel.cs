﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class JumpCacheModel
    {
        [JsonIgnore]
        public int JumpCacheModelId { get; set; }

        [StringLength(32)]
        public string SourceSystemId { get; set; }
        [StringLength(32)]
        public string SourceSystemName { get; set; }
        [StringLength(8)]
        public string SourceSystemNaturalId { get; set; }

        [StringLength(32)]
        public string DestinationSystemId { get; set; }
        [StringLength(32)]
        public string DestinationSystemName { get; set; }
        [StringLength(8)]
        public string DestinationNaturalId { get; set; }

        public double OverallDistance { get; set; }
        public int JumpCount { get; set; }

        public virtual List<JumpCacheRouteJump> Jumps { get; set; } = new List<JumpCacheRouteJump>();
    }

    public class JumpCacheRouteJump
    {
        [JsonIgnore]
        public int JumpCacheRouteJumpId { get; set; }

        [StringLength(32)]
        public string SourceSystemId { get; set; }
        [StringLength(32)]
        public string SourceSystemName { get; set; }
        [StringLength(8)]
        public string SourceSystemNaturalId { get; set; }

        [StringLength(32)]
        public string DestinationSystemId { get; set; }
        [StringLength(32)]
        public string DestinationSystemName { get; set; }
        [StringLength(8)]
        public string DestinationNaturalId { get; set; }

        public double Distance { get; set; }

        [JsonIgnore]
        public int JumpCacheModelId { get; set; }
        [JsonIgnore]
        public virtual JumpCacheModel JumpCache { get; set; }
    }
}
