﻿using System;
using Microsoft.EntityFrameworkCore;

using FIORest.Database.Models;

namespace FIORest.Database
{
    public class PRUNDataContext : DbContext
    {
        public DbSet<AuthenticationModel> AuthenticationModels { get; set; }
        public DbSet<PermissionAllowance> PermissionAllowances { get; set; }
        public DbSet<FailedLoginAttempt> FailedLoginAttempts { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<APIKey> APIKeys { get; set; }

        public DbSet<GroupModel> GroupModels { get; set; }
        public DbSet<GroupUserEntryModel> GroupUserEntryModels { get; set; }

        public DbSet<BuildingDegradationModel> BuildingDegradationModels { get; set; }
        public DbSet<BuildingDegradationReclaimableMaterialModel> BuildingDegradationReclaimableMaterialModels { get; set; }
        public DbSet<BuildingDegradationRepairMaterialModel> BuildingDegradationRepairMaterialModels { get; set; }

        public DbSet<BUIModel> BUIModels { get; set; }
        public DbSet<BUIBuildingCost> BUIBuildingCosts { get; set; }
        public DbSet<BUIRecipeInput> BUIRecipeInputs { get; set; }
        public DbSet<BUIRecipeOutput> BUIRecipeOutputs { get; set; }
        public DbSet<BUIRecipe> BUIRecipes { get; set; }

        public DbSet<CompanyDataModel> CompanyDataModels { get; set; }
        public DbSet<CompanyDataCurrencyBalance> CompanyDataCurrencyBalances { get; set; }

        public DbSet<CXDataModel> CXDataModels { get; set; }
        public DbSet<CXBuyOrder> CXBuyOrders { get; set; }
        public DbSet<CXSellOrder> CXSellOrders { get; set; }
        public DbSet<CXPCData> CXPCData { get; set; }
        public DbSet<CXPCDataEntry> CXPCDataEntries { get; set; }
        public DbSet<Station> Stations { get; set; }

        public DbSet<ExpertsModel> ExpertModels { get; set; }

        public DbSet<InfrastructureModel> InfrastructureModels { get; set; }
        public DbSet<InfrastructureInfo> InfrastructureInfos { get; set; }
        public DbSet<InfrastructureProjectUpgradeCosts> InfrastructureProjectUpgradeCosts { get; set; }
        public DbSet<InfrastructureProjectUpkeeps> InfrastructureProjectUpkeeps { get; set; }
        public DbSet<InfrastructureProjectContributions> InfrastructureProjectContributions { get; set; }
        public DbSet<InfrastructureReport> InfrastructureModelReports { get; set; }

        public DbSet<LocalMarketModel> LocalMarketModels { get; set; }
        public DbSet<ShippingAd> ShippingAds { get; set; }
        public DbSet<BuyingAd> BuyingAds { get; set; }
        public DbSet<SellingAd> SellingAds { get; set; }

        public DbSet<PRODLinesModel> PRODLinesModels { get; set; }
        public DbSet<ProductionLine> ProductionLines { get; set; }
        public DbSet<ProductionLineOrder> ProductionLineOrders { get; set; }
        public DbSet<ProductionLineInput> ProductionLineInputs { get; set; }
        public DbSet<ProductionLineOutput> ProductionLineOutputs { get; set; }

        public DbSet<SHIPSModel> SHIPSModels { get; set; }
        public DbSet<SHIPSShip> SHIPSShips { get; set; }

        public DbSet<FLIGHTSModel> FLIGHTSModels { get; set; }
        public DbSet<FLIGHTSFlight> FLIGHTSFlights { get; set; }
        public DbSet<FLIGHTSFlightSegment> FLIGHTSFlightSegments { get; set; }
        public DbSet<OriginLine> FLIGHTSOriginLines { get; set; }
        public DbSet<DestinationLine> FLIGHTSDestinationLines { get; set; }

        public DbSet<PlanetDataModel> PlanetDataModels { get; set; }
        public DbSet<PlanetDataResource> PlanetDataResources { get; set; }
        public DbSet<PlanetBuildRequirement> PlanetBuildRequirements { get; set; }
        public DbSet<PlanetProductionFee> PlanetProductionFees { get; set; }
        public DbSet<PlanetCOGCProgram> PlanetCOGCPrograms { get; set; }
        public DbSet<PlanetCOGCVote> PlanetCOGCVotes { get; set; }
        public DbSet<PlanetCOGCUpkeep> PlanetCOGCUpkeep { get; set; }
        public DbSet<PlanetSite> PlanetSites { get; set; }

        public DbSet<SITESModel> SITESModels { get; set; }
        public DbSet<SITESSite> SITESSites { get; set; }
        public DbSet<SITESBuilding> SITESBuildings { get; set; }
        public DbSet<SITESReclaimableMaterial> SITESReclaimableMaterials { get; set; }
        public DbSet<SITESRepairMaterial> SITESRepairMaterials { get; set; }

        public DbSet<WarehouseModel> WarehouseModels { get; set; }

        public DbSet<WorkforceModel> WorkforceModels { get; set; }
        public DbSet<WorkforceDescription> WorkforceDescriptions { get; set; }
        public DbSet<WorkforceNeed> WorkforceNeeds { get; set; }

        public DbSet<MATModel> MATModels { get; set; }

        public DbSet<StorageModel> StorageModels { get; set; }
        public DbSet<StorageItem> StorageItems { get; set; }

        public DbSet<SystemStarsModel> SystemStarsModels { get; set; }
        public DbSet<SystemConnection> SystemConnections { get; set; }
        public DbSet<SystemStar> SystemStars { get; set; }

        public DbSet<WorldSectorsModel> WorldSectorsModels { get; set; }
        public DbSet<SubSector> SubSectors { get; set; }
        public DbSet<SubSectorVertex> SubSectorVertices { get; set; }

        public DbSet<UserDataModel> UserDataModels { get; set; }

        public DbSet<ChatModel> ChatModels { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }

        public DbSet<UserSettingsModel> UserSettingsModels { get; set; }
        public DbSet<UserSettingsBurnRate> UserSettingsBurnRates { get; set; }
        public DbSet<UserSettingsBurnRateExclusion> UserSettingsBurnRateExclusions { get; set; }

        public DbSet<ContractModel> ContractModels { get; set; }
        public DbSet<ContractDependency> ContractDependencies { get; set; }
        public DbSet<ContractCondition> ContractConditions { get; set; }

        public DbSet<CXOSTradeOrdersModel> CXOSTradeOrderModels { get; set; }
        public DbSet<CXOSTradeOrder> CXOSTradeOrders { get; set; }
        public DbSet<CXOSTrade> CXOSTrades { get; set; }

        public DbSet<JumpCacheModel> JumpCacheModels { get; set; }
        public DbSet<JumpCacheRouteJump> JumpCacheRoutes { get; set; }

        public DbSet<ComexExchange> ComexExchanges { get; set; }
        public DbSet<CountryRegistryCountry> CountryRegistryCountries { get; set; }
        public DbSet<SimulationData> SimulationData { get; set; }
        public DbSet<WorkforcePerOneHundred> WorkforcePerOneHundreds { get; set; }
        public DbSet<WorkforcePerOneHundreedNeed> WorkforcePerOneHundredNeeds { get; set; }

        public DbSet<FXDataModel> FXDataModels { get; set; }

        public DbSet<PriceIndexModel> PriceIndexModels { get; set; }

        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.Opts.DebugDatabase)
            {
                options.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            }
        }

        public static PRUNDataContext GetNewContext()
        {
            switch(Globals.Opts.DatabaseType)
            {
                case Options.DBTypes.Postgres:
                    return new PostgresDataContext();
                case Options.DBTypes.Sqlite:
                default:
                    return new SqliteDataContext();
            }
        }
    }
}