﻿using System;

namespace FIORest
{
	public static class Utils
	{
		const double DOUBLE_SMALL_NUMBER = 0.00001;
		const float FLOAT_SMALL_NUMBER = 0.00001f;

		public static bool AboutEquals(this double left, double right)
		{
			return Math.Abs(left - right) < DOUBLE_SMALL_NUMBER;
		}

		public static bool AboutEquals(this float left, float right)
		{
			return Math.Abs(left - right) < FLOAT_SMALL_NUMBER;
		}

		public static double RoundToDecimalPlaces( this double value, int decimalPlaces )
		{
			if (decimalPlaces <= 0 || decimalPlaces > 6 )
			{
				return value;
			}

			double factor = Math.Pow(10.0, decimalPlaces);
			return (double)Math.Round(value * factor) / factor;
		}

		public static double? RoundToDecimalPlaces(this double? value, int decimalPlaces)
		{
			if (value == null || decimalPlaces <= 0 || decimalPlaces > 6)
			{
				return value;
			}

			double factor = Math.Pow(10.0, decimalPlaces);
			return (double)Math.Round((double)value * factor) / factor;
		}

		private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		public static long ToEpochMs(this DateTime dateTime)
		{
			TimeSpan t = dateTime.ToUniversalTime() - epoch;
			return (long)t.TotalMilliseconds;
		}
		
		public static DateTime FromUnixTime(long unixTime)
		{
			return epoch.AddMilliseconds(unixTime);
		}

		public static long GetCurrentEpochMs()
		{
			TimeSpan t = DateTime.UtcNow - epoch;
			return (long)t.TotalMilliseconds;
		}
	}
}