﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class CXRevPart2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ApexDataModelId",
                table: "CXDataModels",
                newName: "CXDataModelId");

            migrationBuilder.CreateTable(
                name: "CXBuyOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    CompanyId = table.Column<string>(type: "text", nullable: true),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    CompanyCode = table.Column<string>(type: "text", nullable: true),
                    ItemCount = table.Column<int>(type: "integer", nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    CXDataModelId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXBuyOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CXSellOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    CompanyId = table.Column<string>(type: "text", nullable: true),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    CompanyCode = table.Column<string>(type: "text", nullable: true),
                    ItemCount = table.Column<int>(type: "integer", nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    CXDataModelId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXSellOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId",
                table: "CXBuyOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId_CXDataModelId",
                table: "CXBuyOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId",
                table: "CXSellOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId_CXDataModelId",
                table: "CXSellOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CXBuyOrders");

            migrationBuilder.DropTable(
                name: "CXSellOrders");

            migrationBuilder.RenameColumn(
                name: "CXDataModelId",
                table: "CXDataModels",
                newName: "ApexDataModelId");
        }
    }
}
