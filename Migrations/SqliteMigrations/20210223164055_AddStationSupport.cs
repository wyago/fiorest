﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class AddStationSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Stations",
                columns: table => new
                {
                    StationId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    SystemId = table.Column<string>(type: "TEXT", nullable: true),
                    SystemNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    SystemName = table.Column<string>(type: "TEXT", nullable: true),
                    CommisionTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ComexId = table.Column<string>(type: "TEXT", nullable: true),
                    ComexName = table.Column<string>(type: "TEXT", nullable: true),
                    ComexCode = table.Column<string>(type: "TEXT", nullable: true),
                    WarehouseId = table.Column<string>(type: "TEXT", nullable: true),
                    CountryId = table.Column<string>(type: "TEXT", nullable: true),
                    CountryCode = table.Column<string>(type: "TEXT", nullable: true),
                    CountryName = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyName = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "INTEGER", nullable: false),
                    GovernorId = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorUserName = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationId = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationName = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationCode = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.StationId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Stations");
        }
    }
}
