﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class AddGlobalWorkforceNeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundreds",
                columns: table => new
                {
                    WorkforcePerOneHundredId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    WorkforceType = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundreds", x => x.WorkforcePerOneHundredId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundredNeeds",
                columns: table => new
                {
                    WorkforcePerOneHundreedNeedId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", nullable: true),
                    Amount = table.Column<double>(type: "REAL", nullable: false),
                    WorkforcePerOneHundredId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundredNeeds", x => x.WorkforcePerOneHundreedNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforcePerOneHundredNeeds_WorkforcePerOneHundreds_WorkforcePerOneHundredId",
                        column: x => x.WorkforcePerOneHundredId,
                        principalTable: "WorkforcePerOneHundreds",
                        principalColumn: "WorkforcePerOneHundredId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkforcePerOneHundredNeeds_WorkforcePerOneHundredId",
                table: "WorkforcePerOneHundredNeeds",
                column: "WorkforcePerOneHundredId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundredNeeds");

            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundreds");
        }
    }
}
